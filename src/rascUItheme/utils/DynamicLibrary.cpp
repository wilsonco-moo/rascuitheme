
#include "DynamicLibrary.h"

#include <cstddef>
#include <iostream>
#ifdef _WIN32
    #include <windows.h>
#else
    #include <dlfcn.h>
#endif

namespace rascUItheme {

    // --------------------------- Constructor/destructor -----------------------------

    DynamicLibrary::DynamicLibrary(const std::string & filename) :
        handle(NULL),
        filename(filename),
        haveSymbolsLoadedSuccessfully(true) {
        openHandle();
    }
    DynamicLibrary::DynamicLibrary(void) :
        handle(NULL),
        filename(""),
        haveSymbolsLoadedSuccessfully(true) {
    }

    DynamicLibrary::~DynamicLibrary(void) {
        closeHandle();
    }

    // ------------------------------- Copy and move -----------------------------------

    DynamicLibrary::DynamicLibrary(const DynamicLibrary & other) :
        handle(NULL),
        filename(other.filename),
        haveSymbolsLoadedSuccessfully(other.haveSymbolsLoadedSuccessfully) {
        openHandle();
    }
    DynamicLibrary & DynamicLibrary::operator = (const DynamicLibrary & other) {
        closeHandle();
        filename = other.filename;
        haveSymbolsLoadedSuccessfully = other.haveSymbolsLoadedSuccessfully;
        openHandle();
        return *this;
    }
    DynamicLibrary::DynamicLibrary(DynamicLibrary && other) :
        handle(other.handle),
        filename(std::move(other.filename)),
        haveSymbolsLoadedSuccessfully(other.haveSymbolsLoadedSuccessfully) {
        other.handle = NULL;
    }
    DynamicLibrary & DynamicLibrary::operator = (DynamicLibrary && other) {
        closeHandle();
        handle = other.handle;
        filename = std::move(other.filename);
        haveSymbolsLoadedSuccessfully = other.haveSymbolsLoadedSuccessfully;
        other.handle = NULL;
        return *this;
    }

    // -------------------------- Opening/closing handle ---------------------------------

    void DynamicLibrary::openHandle(void) {
        #ifdef _WIN32
            handle = (void *)LoadLibrary(filename.c_str());
        #else
            handle = dlopen(filename.c_str(), RTLD_LAZY);
            if (handle == NULL) {
                std::cerr << "Failed to load dynamic library:\n    Filename: " << filename << "\n    Error: " << dlerror() << "\n";
            }
        #endif
    }

    void DynamicLibrary::closeHandle(void) {
        if (handle != NULL) {
            #ifdef _WIN32
                if (FreeLibrary((HMODULE)handle) == 0) {
            #else
                if (dlclose(handle) != 0) {
            #endif
                std::cerr << "Failed to close dynamically loaded library " << filename << ".\n";
            }
            handle = NULL;
        }
    }

    // ----------------------------- Loading symbols ------------------------------------

    void (*DynamicLibrary::loadFunctionInternal(const std::string & name))(void) {
        void * object = loadObject(name);
        void (*func)(void);
        *reinterpret_cast<void **>(&func) = object;
        return func;
    }

    bool DynamicLibrary::loadedSuccessfully(void) const {
        return handle != NULL;
    }

    void * DynamicLibrary::loadObject(const std::string & name) {
        void * object;
        if (DynamicLibrary::loadedSuccessfully()) {
            #ifdef _WIN32
                object = (void *)GetProcAddress((HMODULE)handle, name.c_str());
            #else
                object = dlsym(handle, name.c_str());
            #endif
        } else {
            object = NULL;
        }
        if (object == NULL) {
            haveSymbolsLoadedSuccessfully = false;
        }
        return object;
    }

    bool DynamicLibrary::symbolsLoadedSuccessfully(void) const {
        return haveSymbolsLoadedSuccessfully;
    }
    void DynamicLibrary::resetSymbolsLoaded(void) {
        haveSymbolsLoadedSuccessfully = true;
    }

    const std::string & DynamicLibrary::getFilename(void) const {
        return filename;
    }
}
