
#include "DynamicThemeLoader.h"

#include <cstring>
#include <rascUI/base/Theme.h>

namespace rascUItheme {

    // ------------------ Constructor and destructor -----------------

    DynamicThemeLoader::DynamicThemeLoader(const std::string & filename) :
        DynamicLibrary(filename) {
        libLoadTheme();
    }

    DynamicThemeLoader::DynamicThemeLoader(void) :
        DynamicLibrary(),
        theme(NULL) {
    }

    DynamicThemeLoader::~DynamicThemeLoader(void) {
        libDeleteTheme();
    }

    // ------------------- Copy and move -----------------------------

    DynamicThemeLoader::DynamicThemeLoader(const DynamicThemeLoader & other) :
        DynamicLibrary((const DynamicLibrary &)other) {
        libLoadTheme();
    }
    DynamicThemeLoader & DynamicThemeLoader::operator = (const DynamicThemeLoader & other) {
        libDeleteTheme();
        ((DynamicLibrary &)*this) = (const DynamicLibrary &)other;
        libLoadTheme();
        return *this;
    }
    DynamicThemeLoader::DynamicThemeLoader(DynamicThemeLoader && other) :
        DynamicLibrary((DynamicLibrary &&)other),
        theme(other.theme),
        getApiVersion(other.getApiVersion),
        createTheme(other.createTheme),
        deleteTheme(other.deleteTheme) {
        other.theme = NULL;
    }
    DynamicThemeLoader & DynamicThemeLoader::operator = (DynamicThemeLoader && other) {
        libDeleteTheme();
        ((DynamicLibrary &)*this) = (DynamicLibrary &&)other;
        theme = other.theme;
        getApiVersion = other.getApiVersion;
        createTheme = other.createTheme;
        deleteTheme = other.deleteTheme;
        other.theme = NULL;
        return *this;
    }

    // --------------------- Load and delete theme -------------------

    void DynamicThemeLoader::libLoadTheme(void) {
        getApiVersion = loadFunction<getApiVersionT>("getApiVersion");
        createTheme = loadFunction<createThemeT>("createTheme");
        deleteTheme = loadFunction<deleteThemeT>("deleteTheme");
        if (symbolsLoadedSuccessfully() && strcmp(getApiVersion(), THEME_API_VERSION) == 0) {
            theme = createTheme();
        } else {
            theme = NULL;
        }
    }
    void DynamicThemeLoader::libDeleteTheme(void) {
        if (theme != NULL) {
            deleteTheme(theme);
        }
    }

    bool DynamicThemeLoader::loadedSuccessfully(void) const {
        return DynamicLibrary::loadedSuccessfully() && theme != NULL;
    }
}
