#ifndef RASCUITHEME_UTILS_DYNAMICTHEMELOADER_H_
#define RASCUITHEME_UTILS_DYNAMICTHEMELOADER_H_

#include "DynamicLibrary.h"

namespace rascUI {
    class Theme;
}

namespace rascUItheme {

    class DynamicThemeLoader : public DynamicLibrary {
    private:
        rascUI::Theme * theme;

        using getApiVersionT = const char * (void);
        getApiVersionT * getApiVersion;

        using createThemeT = rascUI::Theme * (void);
        createThemeT * createTheme;

        using deleteThemeT = void (rascUI::Theme *);
        deleteThemeT * deleteTheme;

    public:
        DynamicThemeLoader(const std::string & filename);
        DynamicThemeLoader(void);
        virtual ~DynamicThemeLoader(void);

        // --------- Copy and move ----------
        DynamicThemeLoader(const DynamicThemeLoader & other);
        DynamicThemeLoader & operator = (const DynamicThemeLoader & other);
        DynamicThemeLoader(DynamicThemeLoader && other);
        DynamicThemeLoader & operator = (DynamicThemeLoader && other);
    private:
        void libLoadTheme(void);
        void libDeleteTheme(void);
    public:

        /**
         * This will return NULL if either the library failed to load,
         * any function from the library failed to load, or the theme has
         * the wrong api version.
         */
        inline rascUI::Theme * getTheme(void) const {
            return theme;
        }
        virtual bool loadedSuccessfully(void) const override;
    };

}
#endif
