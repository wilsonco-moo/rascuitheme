/*
 * PathUtils.h
 *
 *  Created on: 30 Sep 2021
 *      Author: wilson
 */

#ifndef RASCUITHEME_UTILS_CONFIG_PATHUTILS_H_
#define RASCUITHEME_UTILS_CONFIG_PATHUTILS_H_

#include <string>
#include <vector>

namespace rascUItheme {

    /**
     * Provides various cross-platform static methods for dealing with files
     * and paths, and getting locations of things like the home directory and
     * config paths.
     * Note: All functions accept strings as c-style strings to avoid unnecessary
     * allocation, but return strings using std::string for convenience.
     * Note: All paths are expected to use forward slashes as separators, since
     * both platforms we're designed for (Windows/Linux) can cope with using
     * forward slash path separators.
     * Note: All functions here are thread-safe. Any thread-unsafe functions used
     * internally are locked.
     */
    class PathUtils {
    public:
        // ================================== File/directory querying and creating ========================
        
        /**
         * Returns true if the specified file exists, i.e: It is any type of file.
         */
        static bool fileExists(const char * filename);

        /**
         * Returns true if the specified file exists, and is either a directory or a symbolic link
         * to a directory.
         */
        static bool isDirectory(const char * filename);

        /**
         * Returns true if the specified file exists, and is either a regular file or a symbolic link
         * to a regular file.
         */
        static bool isRegularFile(const char * filename);
        
        /**
         * Lists the files in a directory. This will return an empty std::vector
         * if the directory is either empty, or we cannot read from it, or it doesn't exist.
         * By default this is sorted in alphabetical order. This can be disabled by setting sort to false.
         * NOTE: THIS WILL NOT RETURN THE FULL PATH, ONLY THE FILENAME.
         * NOTE: On Linux, "" (as working directory) is automatically converted to "." (single dot).
         * NOTE: This automatically removes the special files ".." and "."
         */
        static std::vector<std::string> listFiles(const char * directory, bool sort = true);
        
        /**
         * Ensures that the specified directory exists, (or is a symbolic link). If it does not exist, it is created.
         * Returns: 1 (true)  if after the call the directory exists as a directory.
         *          0 (false) if: Something other than a directory is there, or we failed
         *                        to create a directory there, (in this case an error is printed to the log).
         */
        static bool ensureDirectoryExists(const char * directory);
        
        
        // ================================== Path (string) manipulation ==================================
        
        /**
         * Ensures that the string ends in a single trailing forward slash.
         * This will add a single forward slash if there isn't one, and will
         * remove characters if the provided string has multiple trailing
         * forward slashes.
         * Note:
         * For root directory "/" this returns "/".
         * For empty/current directory "" this returns "".
         */
        static std::string addTrailingForwardSlash(const char * string);

        /**
         * Ensures that the string does NOT have a trailing forward slash,
         * (except in the case of root "/"). This will remove characters
         * from the end of the string, until the string does not end in a
         * forward slash. Removal stops once the string is a single character,
         * otherwise removing trailing slash from "/" would result in "".
         * Note:
         * For root directory "/" this returns "/" (trailing slash is allowed here to preserve meaning as root).
         * For empty/current directory "" this returns "".
         */
        static std::string removeTrailingForwardSlash(const char * string);
        
        /**
         * Removes trailing forward slash, then extracts the filename
         * part of the path.
         * For example: getFilename("/home/bob/image.png")
         * would return "image.png".
         */
        static std::string getFilename(const char * path);
        
        
        // ======================================== USER DIRECTORIES ======================================
        
        /**
         * Returns our current home directory, as an absolute path. This is worked out once and
         * stored for subsequent calls.
         * If home directory cannot be found, working directory (empty) is used as a fallback.
         * The returned path ALWAYS has a trailing forward slash (unless we fall back to working
         * directory, in which case we return empty).
         */
        static const std::string & getHomeDirectory(void);
        
        /**
         * Returns the user's "config directory", as an absolute path. This is a directory which we
         * can write to, to store data for subsequent runs of the application, (typically we should
         * create a subdirectory here for config files). This is worked out once and stored for
         * subsequent calls.
         * On windows, this is: "%appdata%/" (i.e: "C:/Users/user/AppData/Roaming/").
         * On Linux, this is: "~/.config/" (i.e: "/home/user/.config/").
         * 
         * If this cannot be found (or created with the directory reported by getHomeDirectory() in the
         * case of Linux), then working directory (empty) is used as a fallback.
         * The returned path ALWAYS has a trailing forward slash (unless we fall back to working
         * directory, in which case we return empty).
         */
        static const std::string & getConfigDirectory(void);
        
        /**
         * Returns the directory in which we put rascUI specific config files. If it doesn't exist,
         * it is created if possible. This is worked out once and stored for subsequent calls.
         * This is a subdirectory of the directory reported by getConfigDirectory().
         * 
         * If configs directory cannot be found, working directory (empty) is used as a fallback.
         * The returned path ALWAYS has a trailing forward slash (unless we fall back to working
         * directory, in which case we return empty).
         */
        static const std::string & getRascUIConfigDirectory(void);
    };
}

#endif
