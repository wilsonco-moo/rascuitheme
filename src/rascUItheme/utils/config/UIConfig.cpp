/*
 * UIConfig.cpp
 *
 *  Created on: 2 Oct 2021
 *      Author: wilson
 */

#include "UIConfig.h"

#include <iostream>
#include <array>

#include "PathUtils.h"

namespace rascUItheme {
    
    const char * const UIConfig::CONFIG_FILENAME = "rascUI.cfg";
    
    UIConfig::UIConfig() :
        ConfigFile((PathUtils::getRascUIConfigDirectory() + CONFIG_FILENAME).c_str()) {
        
        // If we failed to load, assume the config file doesn't exist
        // yet, so set default config and save.
        if (!loadedSuccessfully()) {
            std::cout << "INFO: UIConfig::UIConfig: Initial config file " << getFilename() << " failed to load, creating default config.\n";
            
            setValue("thing", "potato");
            setValue("cheese", "spam");
            save();
        }
    }
    
    UIConfig::~UIConfig(void) {
    }
}
