/*
 * ConfigFile.h
 *
 *  Created on: 2 Oct 2021
 *      Author: wilson
 */

#ifndef RASCUITHEME_UTILS_CONFIG_CONFIGFILE_H_
#define RASCUITHEME_UTILS_CONFIG_CONFIGFILE_H_

#include <unordered_map>
#include <cstddef>
#include <string>

namespace rascUItheme {

    /**
     * ConfigFile provides a convenient way to load and save very
     * basic config files. These are just text files containing
     * a series of key/value pairs, separated by equals signs.
     * 
     * Applications can use this to persist very basic settings, in
     * cases where XML would be slow and overkill. If more complex
     * data, (or data with actual structure) needs to be stored,
     * consider using an XML library like tinyxml2 instead.
     */
    class ConfigFile {
    private:
        std::string filename;
        std::unordered_map<std::string, std::string> config;
        bool hasLoadedSuccessfully;

    public:
        /**
         * Empty/default constructor: creates an empty ConfigFile
         * without loading a file.
         */
        ConfigFile(void);
        
        /**
         * Creates ConfigFile from a filename. If autoLoadFile is true,
         * we automatically load the contents of the file.
         */
        ConfigFile(const char * filename, bool autoLoadFile = true);
        
        virtual ~ConfigFile(void);
        
        /**
         * Loads (or re-loads) config from current file.
         * Returns true if successful (also see loadedSuccessfully).
         */
        bool load(void);
        
        /**
         * Saves config to current file, overwriting it.
         * Note: The order in which each pair is written to the config
         * file is random (ish). It is whichever order the keys end up
         * in an unordered_map.
         * Returns true if successful.
         */
        bool save(void) const;
        
        /**
         * Gets a config value. This pointer remains valid only until
         * next time the ConfigFile is modified.
         * If the key doesn't exist, an empty string is returned.
         */
        const char * getValue(const char * key) const;
        
        /**
         * Returns true if a config value is assigned to the key.
         */
        bool isSet(const char * key) const;
        
        /**
         * Sets a config value. If the value existed already, it is
         * overwritten.
         */
        void setValue(const char * key, const char * value);
        
        /**
         * Removes the key from the config map.
         */
        void unsetValue(const char * key);
        
        /**
         * Clears the config map of all key/value pairs.
         */
        void clear(void);
        
        
        // --------------------- Accessors -----------------------------
        
        /**
         * Returns true if last time load() was called (or the auto
         * load constructor), we loaded successfully.
         * If load has not yet been run, this will return false.
         */
        inline bool loadedSuccessfully(void) const {
            return hasLoadedSuccessfully;
        }
        
        /**
         * Allows access to our filename.
         */
        inline const std::string & getFilename(void) const {
            return filename;
        }
        
        /**
         * Changes our filename - helpful if you want to save to a
         * different file which was loaded from, or vice versa.
         */
        inline void setFilename(const char * newFilename) {
            filename = newFilename;
        }
    };
}

#endif
