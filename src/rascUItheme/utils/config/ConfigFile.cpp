/*
 * ConfigFile.cpp
 *
 *  Created on: 2 Oct 2021
 *      Author: wilson
 */

#include "ConfigFile.h"

#include <algorithm>
#include <iostream>
#include <fstream>

namespace rascUItheme {

    ConfigFile::ConfigFile(void) :
        filename(),
        config(),
        hasLoadedSuccessfully(false) {
    }

    ConfigFile::ConfigFile(const char * filename, bool autoLoadFile) :
        filename(filename),
        config(),
        hasLoadedSuccessfully(false) {
        // Auto load file only if required.
        if (autoLoadFile) {
            load();
        }
    }
    
    ConfigFile::~ConfigFile(void) {
    }

    bool ConfigFile::load(void) {
        // Clear config, open file. Set hasLoadedSuccessfully to whether we
        // successfully opened the file. Only try to read if successfully opened.
        config.clear();
        std::ifstream file(filename.c_str());
        hasLoadedSuccessfully = file.operator bool();
        if (hasLoadedSuccessfully) {
            
            // Read all lines of the file.
            unsigned long long lineNumber = 0;
            std::string line;
            while(std::getline(file, line)) {
                // Pre increment: line numbers historically start at 1, we also need to count lines we skip.
                lineNumber++;
                
                // Empty or comment line: ignore it.
                if (line.empty() || line[0] == '#') {
                    continue;
                }
                
                // Find first equals character in the part after the first "non space", clear config and give up if the line is invalid.
                const std::string::iterator equalsIter = std::find(line.begin(), line.end(), '=');
                if (equalsIter == line.end()) {
                    std::cerr << "ERROR: ConfigFile::load: " << "Invalid line in config file \"" << filename << "\", at line " << lineNumber << ": \"" << line << "\".\n";
                    config.clear();
                    hasLoadedSuccessfully = false;
                    return false;
                }
                
                // Get key and value from line, now we know where separator is.
                std::string key = line.substr(0, equalsIter - line.begin());
                std::string value = line.substr((equalsIter - line.begin()) + 1, line.end() - equalsIter);
                
                if (key.empty()) {
                    std::cerr << "ERROR: ConfigFile::load: " << "Empty key in config file \"" << filename << "\", at line " << lineNumber << ": \"" << line << "\".\n";
                    config.clear();
                    hasLoadedSuccessfully = false;
                    return false;
                }
                
                if (config.find(key) != config.end()) {
                    std::cerr << "ERROR: ConfigFile::load: " << "Duplicate key in config file \"" << filename << "\", at line " << lineNumber << ": \"" << line << "\".\n";
                    config.clear();
                    hasLoadedSuccessfully = false;
                    return false;
                }
                
                config.emplace(std::move(key), std::move(value));
            }
        } else {
            std::cerr << "WARNING: ConfigFile::load: Failed to load config file \"" << filename << "\".\n";
        }
        return hasLoadedSuccessfully;
    }

    bool ConfigFile::save(void) const {
        // Write each pair to file.
        std::ofstream file(filename);
        for (const std::pair<const std::string, std::string> & pair : config) {
            file << pair.first << '=' << pair.second << '\n';
        }
        // Close file, get success status, complain if not successful. This way we also
        // catch errors which happened during the call to close.
        file.close();
        const bool success = file.operator bool();
        if (!success) {
            std::cerr << "ERROR: ConfigFile::save: Failed to save config to file: \"" << filename << "\".\n";
        }
        return success;
    }

    const char * ConfigFile::getValue(const char * key) const {
        // Find in map, return default config or empty if not found, otherwise return string inside map value.
        // Unfortunately we have to create a temporary std::string for map lookup.
        const std::unordered_map<std::string, std::string>::const_iterator iter = config.find(std::string(key));
        if (iter == config.end()) {
            return "";
        } else {
            return iter->second.c_str();
        }
    }
    
    bool ConfigFile::isSet(const char * key) const {
        return config.find(std::string(key)) != config.end();
    }

    void ConfigFile::setValue(const char * key, const char * value) {
        // Convert key to std::string, find in map.
        std::string keyStr(key);
        const std::unordered_map<std::string, std::string>::iterator iter = config.find(keyStr);
        if (iter == config.end()) {
            // Key doesn't already exist, emplace into map.
            config.emplace(std::piecewise_construct, std::forward_as_tuple(std::move(keyStr)), std::forward_as_tuple(value));
        } else {
            // Key already exists in map: just set value in existing string.
            iter->second = value;
        }
    }

    void ConfigFile::unsetValue(const char * key) {
        config.erase(std::string(key));
    }

    void ConfigFile::clear(void) {
        config.clear();
    }
}
