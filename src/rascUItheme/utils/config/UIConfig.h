/*
 * UIConfig.h
 *
 *  Created on: 2 Oct 2021
 *      Author: wilson
 */

#ifndef RASCUITHEME_UTILS_CONFIG_UICONFIG_H_
#define RASCUITHEME_UTILS_CONFIG_UICONFIG_H_

#include "ConfigFile.h"

namespace rascUItheme {

    /**
     * An extension to ConfigFile, which provides special config for
     * rascUItheme.
     */
    class UIConfig : public ConfigFile {
    public:
        /**
         * Filename of config file, which we read, which resides within
         * the directory reported by PathUtils::getRascUIConfigDirectory.
         */
        static const char * const CONFIG_FILENAME;
        
    private:
        

    public:
        UIConfig(void);
        virtual ~UIConfig(void);
    };
}

#endif
