/*
 * PathUtils.cpp
 *
 *  Created on: 30 Sep 2021
 *      Author: wilson
 */

#include "PathUtils.h"

#include <algorithm>
#include <iostream>
#include <cstring>
#include <atomic>
#include <cctype>
#include <mutex>

#ifdef _WIN32
    #include <windows.h>
    #include <shlobj.h>
#else
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <unistd.h>
    #include <dirent.h>
    #include <errno.h>
    #include <pwd.h>
#endif

// Uncomment to enable logging each time we find directories for the first time.
// #define RASCUITHEME_PATH_UTILS_LOGGING

namespace rascUItheme {

    // Internal static helpers.
    namespace PathUtilHelpers {
        // Mutex to synchronise thread-unsafe operations.
        static std::recursive_mutex mutex;
        
        // Function to work out home directory, atomic bool to store true once we have worked out home directory, and string to store it. The
        // function must be called if hasFoundHomeDirectory is false, after which homeDirectory can be accessed. This is to avoid working it
        // out every time we call getHomeDirectory(). We make the assumption that the home directory will not change while the program is running.
        static void findHomeDirectory(void);
        static std::atomic<bool> hasFoundHomeDirectory(false);
        static std::string homeDirectory;
        
        // Function to work out config directory, atomic bool to store true once we have worked out config directory, and string to store it. The
        // function must be called if hasFoundConfigDirectory is false, after which configDirectory can be accessed. This is to avoid working it
        // out every time we call getConfigDirectory(). We make the assumption that the config directory will not change while the program is running.
        static void findConfigDirectory(void);
        static std::atomic<bool> hasFoundConfigDirectory(false);
        static std::string configDirectory;
        
        // Function to work out rascUI config directory, atomic bool to store true once we have worked out rascUI config directory, and string to store it. The
        // function must be called if hasFoundRascUIConfigDirectory is false, after which rascUIConfigDirectory can be accessed. This is to avoid working it
        // out every time we call getRascUIConfigDirectory(). We make the assumption that the config directory will not change while the program is running.
        static void findRascUIConfigDirectory(void);
        static std::atomic<bool> hasFoundRascUIConfigDirectory(false);
        static std::string rascUIConfigDirectory;
    }
    
    bool PathUtils::fileExists(const char * unformattedFile) {
        const std::string file = removeTrailingForwardSlash(unformattedFile);
        #ifdef _WIN32
            // Get info using GetFileAttributesA.
            DWORD info = GetFileAttributesA(file.c_str());

            // If that call succeeded, the file exists.
            return info != INVALID_FILE_ATTRIBUTES;
        #else
            // Get info using stat.
            struct stat info;
            int err = stat(file.c_str(), &info);

            // If that call succeeded, the file exists.
            return err == 0;
        #endif
    }

    bool PathUtils::isDirectory(const char * unformattedDir) {
        const std::string directory = removeTrailingForwardSlash(unformattedDir);
        #ifdef _WIN32
            // Get info using GetFileAttributesA.
            DWORD info = GetFileAttributesA(directory.c_str());

            // If that call succeeded, and the directory bit is set, return true.
            return info != INVALID_FILE_ATTRIBUTES && (info & FILE_ATTRIBUTE_DIRECTORY) != 0;
        #else
            // Get info using stat.
            struct stat info;
            int err = stat(directory.c_str(), &info);

            // If that call succeeded, and the file is a directory, return true. If the file is a symbolic link
            // to a directory, stat will actually return information about the directory it links to, so will work correctly.
            return err == 0 && S_ISDIR(info.st_mode);
        #endif
    }

    bool PathUtils::isRegularFile(const char * unformattedFile) {
        const std::string file = removeTrailingForwardSlash(unformattedFile);
        #ifdef _WIN32
            // Get info using GetFileAttributesA.
            DWORD info = GetFileAttributesA(file.c_str());

            // For lack of any better way to do this, just check that the file is NOT a directory.
            return info != INVALID_FILE_ATTRIBUTES && (info & FILE_ATTRIBUTE_DIRECTORY) == 0;
        #else
            // Get info using stat.
            struct stat info;
            int err = stat(file.c_str(), &info);

            // If that call succeeded, and the file is a regular file, return true. If the file is a symbolic link
            // to a regular file, stat will actually return information about the file it links to, so will work correctly.
            return err == 0 && S_ISREG(info.st_mode);
        #endif
    }

    std::vector<std::string> PathUtils::listFiles(const char * directoryIn, bool sort) {
        const std::string directory = removeTrailingForwardSlash(directoryIn);
        std::vector<std::string> files;

        // These directory functions aren't thread safe, so lock to static mutex.
        std::lock_guard<std::recursive_mutex> lock(PathUtilHelpers::mutex);
        
        #ifdef _WIN32
            std::string pattern = directory + "/*.*";
            WIN32_FIND_DATA data;
            HANDLE hFind;
            if ((hFind = FindFirstFile(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
                do {
                    std::string file(data.cFileName);
                    if (file != "." && file != "..") files.push_back(file);
                } while (FindNextFile(hFind, &data) != 0);
                FindClose(hFind);
            }
        #else
            // Note: If directory is empty (current working directory), replace with dot.
            DIR * dir = opendir(directory.empty() ? "." : directory.c_str());
            if (dir != NULL) {
                struct dirent * entry;
                while ((entry = readdir(dir)) != NULL) {
                    std::string file(entry->d_name);
                    if (file != "." && file != "..") files.push_back(file);
                }
                closedir(dir);
            }
        #endif
        if (sort) std::sort(files.begin(), files.end());
        return files;
    }
    
    bool PathUtils::ensureDirectoryExists(const char * unformattedDir) {
        const std::string directory = rascUItheme::PathUtils::removeTrailingForwardSlash(unformattedDir);
        #ifdef _WIN32
            // Get info using GetFileAttributesA.
            DWORD info = GetFileAttributesA(directory.c_str());

            if (info == INVALID_FILE_ATTRIBUTES) {
                // If the directory does not exist, create it.
                BOOL status = CreateDirectoryA(directory.c_str(), NULL);

                // If we failed to create it...
                if (status == 0) {
                    std::cerr << "WARNING: PathUtils::ensureDirectoryExists: " << "Failed to create directory: \"" << directory << "\".\n";
                    return false;
                }
            } else {
                // If it does exist, but is not a directory, show a warning and return false.
                if ((info & FILE_ATTRIBUTE_DIRECTORY) == 0) {
                    std::cerr << "WARNING: PathUtils::ensureDirectoryExists: " << "Cannot use directory: \"" << directory << "\", as there is already something else there.";
                    return false;
                }
            }
            return true;

        #else
            // Get info using stat.
            struct stat info;
            int err = stat(directory.c_str(), &info);

            if (err == -1) {
                // If the directory does not exist, create it.
                err = mkdir(directory.c_str(), 0777);

                // If we failed to create it...
                if (err == -1) {
                    std::cerr << "WARNING: PathUtils::ensureDirectoryExists: " << "Failed to create directory: \"" << directory << "\".\n";
                    return false;
                }
            } else {
                // If it does exist, but it is not a directory, show a warning and return false. Symbolic links are
                // handled correctly here because if stat encounters a symbolic link, it will return info about the file
                // it links to, not the link itself.
                if (!S_ISDIR(info.st_mode)) {
                    std::cerr << "WARNING: PathUtils::ensureDirectoryExists: " << "Cannot use directory: \"" << directory << "\", as there is already something else there.";
                    return false;
                }
            }
            return true;
        #endif
    }
    
    std::string PathUtils::addTrailingForwardSlash(const char * string) {
        std::string output;
        // Do nothing for empty strings (leave empty).
        if (*string != '\0') {
            size_t length = std::strlen(string);
            // String ends in a slash: reduce length until there is only one slash at end, then reserve and assign.
            if (string[length - 1] == '/') {
                while(length > 1 && string[length - 2] == '/') {
                    length--;
                }
                output.reserve(length);
                output.assign(string, length);
            // String doesn't end in a slash: reserve and assign, then add slash.
            } else {
                output.reserve(length + 1);
                output.assign(string, length);
                output += '/';
            }
        }
        return output;
    }

    std::string PathUtils::removeTrailingForwardSlash(const char * string) {
        std::string output;
        // Do nothing for empty strings (leave empty).
        if (*string != '\0') {
            // Get string length, decrease length until the last character isn't a forward slash, but don't go below a
            // length of 1, (otherwise removing trailing slash from "/" would result in ""), then reserve/assign to output.
            size_t length = std::strlen(string);
            while(length > 1 && string[length - 1] == '/') {
                length--;
            }
            output.reserve(length);
            output.assign(string, length);
        }
        return output;
    }
    
    std::string PathUtils::getFilename(const char * rawPath) {
        std::string path = removeTrailingForwardSlash(rawPath);
        // Do nothing for empty (leave empty). If string has one character, it is either root "/"
        // (so should be left unchanged) or has no "/" symbols, so should also be left unchanged.
        if (path.length() > 1) {
            // If we find a slash, return substring after slash.
            const size_t slashPos = path.find_last_of('/');
            if (slashPos != std::string::npos) {
                return path.substr(slashPos + 1);
            }
        }
        return path;
    }
    
    const std::string & PathUtils::getHomeDirectory(void) {
        // First find home directory if not already found, then return it.
        if (!PathUtilHelpers::hasFoundHomeDirectory) {
            PathUtilHelpers::findHomeDirectory();
        }
        return PathUtilHelpers::homeDirectory;
    }
    
    static void PathUtilHelpers::findHomeDirectory(void) {
        // Lock to mutex, return if already found as another thread may do it while we wait for the lock.
        std::lock_guard<std::recursive_mutex> lock(mutex);
        if (hasFoundHomeDirectory) return;
        
        #ifdef _WIN32

            char path[MAX_PATH + 1];
            if (SUCCEEDED(SHGetFolderPathA(NULL, CSIDL_PROFILE, NULL, 0, path))) {
                // If we successully found the home directory, use it.
                // Replace all backslashes with forward slashes, for consistency.
                homeDirectory = path;
                std::replace(homeDirectory.begin(), homeDirectory.end(), '\\', '/');
                homeDirectory = rascUItheme::PathUtils::addTrailingForwardSlash(homeDirectory.c_str());
                goto foundHomeDirectory;
            }

            // Finding home directory failed. We don't need to do anything here: homeDirectory is already initialised to an empty string.
            std::cerr << "WARNING: PathUtils::getHomeDirectory: " << "Failed to find home directory, falling back to using working directory for configuration files.\n";

        #else

            // Try to find the home directory using getpwuid.
            {
                struct passwd * userInfo = getpwuid(getuid());
                if (userInfo != NULL) {
                    const char * homeDir = userInfo->pw_dir;
                    if (homeDir != NULL) {
                        homeDirectory = rascUItheme::PathUtils::addTrailingForwardSlash(homeDir);
                        goto foundHomeDirectory;
                    }
                }
            }

            // If that fails, print a warning, and try to get the home directory using getenv.
            std::cerr << "WARNING: PathUtils::getHomeDirectory: " << "Failed to find home directory using getpwuid(getuid()), falling back to getenv(\"home\").\n";
            {
                const char * homeDir = getenv("HOME");
                if (homeDir != NULL) {
                    homeDirectory = rascUItheme::PathUtils::addTrailingForwardSlash(homeDir);
                    goto foundHomeDirectory;
                }
            }

            // If that fails, print another warning and give up and use an empty string (our working directory).
            // Note: We don't need to do anything here: homeDirectory is already initialised to an empty string.
            std::cerr << "WARNING: PathUtils::getHomeDirectory: " << "Failed to find home directory using getenv(\"home\"), falling back to using working directory as home directory.\n";

        #endif

        foundHomeDirectory:;
        #ifdef RASCUITHEME_PATH_UTILS_LOGGING
            std::cout << "Using home directory: [" << homeDirectory << "]\n";
        #endif
        // Set to true as the last thing we do, for correct memory order.
        hasFoundHomeDirectory = true;
    }
    
    const std::string & PathUtils::getConfigDirectory(void) {
        // First find config directory if not already found, then return it.
        if (!PathUtilHelpers::hasFoundConfigDirectory) {
            PathUtilHelpers::findConfigDirectory();
        }
        return PathUtilHelpers::configDirectory;
    }
    
    static void PathUtilHelpers::findConfigDirectory(void) {
        // Lock to mutex, return if already found as another thread may do it while we wait for the lock.
        std::lock_guard<std::recursive_mutex> lock(mutex);
        if (hasFoundConfigDirectory) return;
        
        #ifdef _WIN32

            // Try to find the directory "%appdata%/", so first get appdata directory.
            std::string confDir;
            char path[MAX_PATH + 1];
            if (!SUCCEEDED(SHGetFolderPathA(NULL, CSIDL_APPDATA, NULL, 0, path))) {
                std::cerr << "WARNING: PathUtils::getConfigDirectory: " << "Failed to locate appdata directory, will fall back to using current working directory.\n";
                goto foundConfigDirectory;
            }
            
            // Process the path to remove backslashes, ensure there is a trailing forward slash.
            confDir = path;
            std::replace(confDir.begin(), confDir.end(), '\\', '/');
            confDir = rascUItheme::PathUtils::addTrailingForwardSlash(confDir.c_str());
            
            // Make sure the directory which we've been given actually exists.
            if (!PathUtils::isDirectory(confDir.c_str())) {
                std::cerr << "WARNING: PathUtils::getConfigDirectory: " << "Reported config directory: \" << confDir << \" doesn't seem to exist, will fall back to using current working directory.\n";
                goto foundConfigDirectory;
            }
            
            // Everything seems successful, write output.
            configDirectory = confDir;

        #else

            // Try to use the directory "~/.config/", so first get home directory.
            std::string confDir = rascUItheme::PathUtils::getHomeDirectory();
            
            // Don't bother trying to create subdirectories if the home directory returned empty (failed to find so fell back to working directory).
            if (confDir.empty()) {
                std::cerr << "WARNING: PathUtils::getConfigDirectory: " << "Failed to find home directory for config filepath, falling back to using working directory.\n";
                goto foundConfigDirectory;
            }
            
            // Ensure the ".config" subdirectory exists.
            confDir += ".config/";
            if (!rascUItheme::PathUtils::ensureDirectoryExists(confDir.c_str())) {
                std::cerr << "WARNING: PathUtils::getConfigDirectory: " << "Failed to find/create \".config\" subdirectory within home directory, falling back to using working directory.\n";
                goto foundConfigDirectory;
            }
            
            // Everything seems successful, write output.
            configDirectory = confDir;
            
        #endif

        foundConfigDirectory:;
        #ifdef RASCUITHEME_PATH_UTILS_LOGGING
            std::cout << "Using config directory: [" << configDirectory << "]\n";
        #endif
        // Set to true as the last thing we do, for correct memory order.
        hasFoundConfigDirectory = true;
    }
    
    const std::string & PathUtils::getRascUIConfigDirectory(void) {
        // First find rascUI config directory if not already found, then return it.
        if (!PathUtilHelpers::hasFoundRascUIConfigDirectory) {
            PathUtilHelpers::findRascUIConfigDirectory();
        }
        return PathUtilHelpers::rascUIConfigDirectory;
    }
    
    static void PathUtilHelpers::findRascUIConfigDirectory(void) {
        // Lock to mutex, return if already found as another thread may do it while we wait for the lock.
        std::lock_guard<std::recursive_mutex> lock(mutex);
        if (hasFoundRascUIConfigDirectory) return;
        
        // Try to use the directory "config/rascUI", so first get config directory.
        std::string confDir = rascUItheme::PathUtils::getConfigDirectory();
        
        // Don't bother trying to create subdirectories if the config directory returned empty (failed to find so fell back to working directory).
        if (confDir.empty()) {
            std::cerr << "WARNING: PathUtils::getRascUIConfigDirectory: " << "Failed to find config directory for rascUI config filepath, falling back to using working directory.\n";
            goto foundRascUIConfigDirectory;
        }
        
        // Ensure the "rascUI" subdirectory exists.
        confDir += "rascUI/";
        if (!rascUItheme::PathUtils::ensureDirectoryExists(confDir.c_str())) {
            std::cerr << "WARNING: PathUtils::getRascUIConfigDirectory: " << "Failed to find/create \".config\" subdirectory within home directory, falling back to using working directory.\n";
            goto foundRascUIConfigDirectory;
        }
        
        // Everything seems successful, write output.
        rascUIConfigDirectory = confDir;
        
        foundRascUIConfigDirectory:;
        #ifdef RASCUITHEME_PATH_UTILS_LOGGING
            std::cout << "Using rascUI config directory: [" << rascUIConfigDirectory << "]\n";
        #endif
        // Set to true as the last thing we do, for correct memory order.
        hasFoundRascUIConfigDirectory = true;
    }
}
