
#ifndef RASCUITHEME_UTILS_DYNAMICLIBRARY_H_
#define RASCUITHEME_UTILS_DYNAMICLIBRARY_H_

#include <string>

namespace rascUItheme {

    /**
     * DynamicLibrary provides a cross-platform system for loading dynamic libraries.
     * To create a DynamicLibrary instance, a filename for the library to load is required. Then use
     * the function loadedSuccessfully to check if the library was loaded successfully.
     * Next, objects and functions respectively can be loaded with the loadObject and loadFunction methods.
     * When loading functions, DynamicLibrary will do all the *not quite standard compliant* casting from
     * a void pointer to a function pointer. It is convenient to use the haveSymbolsLoadedSuccessfully
     * method if you are loading a lot of functions, as then you only need to check for success once.
     *
     * Here is an example of using the DynamicLibrary class to load a function, (the function takes a string
     * as a parameter), dynamically from a library:
     *
     *   // Load library, check success.
     *   rascUItheme::DynamicLibrary library("lib.so");
     *   if (!library.loadedSuccessfully()) {
     *       std::cerr << "Failed to load dynamic library.\n";
     *       exit(EXIT_FAILURE);
     *   }
     *   // Load function, check success.
     *   using funcType = void(const std::string &);
     *   funcType * function = library.loadFunction<funcType>("doSomethingElse");
     *   if (function == NULL) {
     *       std::cerr << "Failed to load function from dynamic library.\n";
     *       exit(EXIT_FAILURE);
     *   }
     *   // Run the function.
     *   function("Potato");
     *
     * NOTE: DynamicLibrary instances can be safely copied and moved.
     */
    class DynamicLibrary {
    private:
        // The handle to the dynamic library, used internally.
        void * handle;
        // The filename of the library.
        std::string filename;
        // Stores the return value of symbolsLoadedSuccessfully.
        bool haveSymbolsLoadedSuccessfully;

    public:
        /**
         * The main constructor. Here we require the filename of the library to load.
         * The user can find out whether loading the library was successful, by calling
         * our loadedSuccessfully method.
         */
        DynamicLibrary(const std::string & filename);

        /**
         * The default constructor. This loads nothing, and internally stores a NULL handle.
         */
        DynamicLibrary(void);

        virtual ~DynamicLibrary(void);

        // --------- Copy and move ----------
        DynamicLibrary(const DynamicLibrary & other);
        DynamicLibrary & operator = (const DynamicLibrary & other);
        DynamicLibrary(DynamicLibrary && other);
        DynamicLibrary & operator = (DynamicLibrary && other);
    private:
        void openHandle(void);
        void closeHandle(void);
        void (*loadFunctionInternal(const std::string & name))(void);
    public:

        /**
         * If the library was loaded successfully, this returns true, otherwise this returns false.
         * NOTE: If the library itself loaded successfully, but a symbol (in loadObject or loadFunction)
         *       failed to load, this will still return true. To check that all symbols loaded
         *       successfully also, use the symbolsLoadedSuccessfully method.
         * NOTE: Any custom subclass which loads more stuff should override this method, for consistency.
         *       BUT: Our internal calls to this method, (i.e: within loadObject), will NOT use the
         *       overridden version, as otherwise this causes a silly problem where we refuse to load objects
         *       because a subclass thinks we have failed to load since we don't have those objects yet.
         */
        virtual bool loadedSuccessfully(void) const;

        /**
         * Loads an object, as a void pointer, from the library. If you are loading a function,
         * prefer the loadFunction method instead of casting the result of this.
         * If the library failed to load, i.e: If loadedSucessfully is false, this will return NULL.
         */
        void * loadObject(const std::string & name);

        /**
         * Loads a function pointer, of specified type, from the dynamic library. This internally
         * does all the *not quite standard compliant* conversion from a void pointer to a function pointer.
         * If the library failed to load, i.e: If loadedSucessfully is false, this will return NULL.
         * Examples of use, (assuming we are loading the function: void someFunction(int))
         * Using c style function pointer syntax:
         *     void (*someFunction)(int) = library.loadFunction<void(int)>("someFunction");
         * Alternatively, the C++ "using" keyword can be used. This avoids repeating
         * the function type, and avoids c style function pointer syntax:
         *     using funcType = void(int);
         *     funcType * someFunction = library.loadFunction<funcType>("someFunction");
         * If required, std::function can be used instead of a plain function pointer:
         *     using funcType = void(int);
         *     std::function<funcType> someFunction = library.loadFunction<funcType>("someFunction");
         */
        template <class T>
        inline T * loadFunction(const std::string & name) {
            return (T *)loadFunctionInternal(name);
        }

        /**
         * The method symbolsLoadedSuccessfully will return true initially, (EVEN IF THE LIBRARY FAILED TO LOAD). If the user
         * requests, via loadFunction or loadObject any symbol which fails to load, (i.e: loading returns NULL because
         * the symbol does not exist, or the library failed to load), then this method will return false, until
         * resetSymbolsLoaded is called.
         *
         * This is convenient if the user wants to load a series of functions, so that they don't have to check
         * the return value of every single one. If symbolsLoadedSuccessfully returns true, then all returned symbols are non-NULL.
         * This can potentially make calling loadedSuccessfully redundant, although the user cannot distinguish between
         * the library itself failing to load, and just one function failing to load, (ASSUMING THE FIRST TRIED TO LOAD
         * AT LEAST ONE FUNCTION).
         *
         * I.e:
         *   Returns true   if no symbols have failed to load, since resetSymbolsLoaded was last called.
         *   Returns false  if any symbol failed to load, since resetSymbolsLoaded was last called.
         *
         * For example:
         *
         * DynamicLibrary library("someLibrary.so");
         * funcType1 * someFunction1 = library.loadFunction<funcType1>("someFunction1");  // Loading at least one function
         * funcType2 * someFunction2 = library.loadFunction<funcType2>("someFunction2");  // must be attempted BEFORE
         * funcType3 * someFunction3 = library.loadFunction<funcType3>("someFunction3");  // calling symbolsLoadedSuccessfully.
         * funcType4 * someFunction4 = library.loadFunction<funcType4>("someFunction4");
         * funcType5 * someFunction5 = library.loadFunction<funcType5>("someFunction5");
         * if (!library.symbolsLoadedSuccessfully()) {
         *     std::cerr << "At least one function failed to load. Either the function did not exist in the library, or the library failed to load.\n";
         * }
         */
        bool symbolsLoadedSuccessfully(void) const;

        /**
         * Resets the return value of symbolsLoadedSuccessfully. See the documentation
         * for symbolsLoadedSuccessfully for more information.
         */
        void resetSymbolsLoaded(void);

        /**
         * Gets the filename associated with this dynamically loaded library, i.e the one
         * provided in our constructor.
         */
        const std::string & getFilename(void) const;
    };
}

#endif
