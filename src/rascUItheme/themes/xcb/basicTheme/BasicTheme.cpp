/*
 * BasicTheme.cpp
 *
 *  Created on: 20 Jun 2019
 *      Author: wilson
 */

// Force enable the theme if requested.
#ifdef FORCE_ENABLE_XCB_BASIC_THEME
    #ifndef ENABLE_RASCUI_THEME
        #define ENABLE_RASCUI_THEME
    #endif
#endif

// Only compile if this theme is enabled.
#ifdef ENABLE_RASCUI_THEME

#include "BasicTheme.h"

#include <rascUIxcb/platform/Context.h>
#include <rascUIxcb/platform/Window.h>
#include <rascUI/util/Location.h>
#include <rascUIxcb/util/Misc.h>
#include <X11/cursorfont.h>
#include <xcb/xproto.h>
#include <sys/types.h>
#include <algorithm>
#include <cstddef>
#include <limits>
#include <string>

#define GET_XCB_RECT(oldRect) \
    xcb_rectangle_t rect = rascUIxcb::Misc::rascUIToXcbRect(oldRect);

#define DRAW_RECT(rectangle, fillColour, saveFillColour)                                                          \
    if (saveFillColour) {                                                                                         \
        lastFillColour = (fillColour)->get();                                                                     \
        xcb_change_gc(getConnection(), getGraphics(), XCB_GC_FOREGROUND, &lastFillColour);                        \
    } else {                                                                                                      \
        (fillColour)->setForeground(getGraphics());                                                               \
    }                                                                                                             \
    xcb_poly_fill_rectangle(getConnection(), getDrawable(), getGraphics(), 1, &(rectangle));

#define DRAW_OUTLINED_RECT(rectangle, fillColour, saveFillColour)                                                 \
    DRAW_RECT(rectangle, fillColour, saveFillColour)                                                              \
    colourOutline.setForeground(getGraphics());                                                                   \
    {                                                                                                             \
        xcb_rectangle_t DRAW_OUTLINED_RECT_outlineRect = {                                                        \
            (rectangle).x,                                                                                        \
            (rectangle).y,                                                                                        \
            (uint16_t)((rectangle).width - 1),                                                                    \
            (uint16_t)((rectangle).height - 1)                                                                    \
        };                                                                                                        \
                                                                                                                  \
        xcb_poly_rectangle(getConnection(), getDrawable(), getGraphics(), 1, &DRAW_OUTLINED_RECT_outlineRect);    \
    }

#define TEXT_CHAR_WIDTH 7




namespace xcbBasicTheme {

    BasicTheme::BasicTheme(void) :
        rascUIxcb::XcbThemeBase(),
        
        // Initialise all colours with RGB values. Note that each channel is 16 bit.
        colourOutline               (0x3300, 0x3300, 0x3300),
        colourBack                  (0xdd00, 0xdd00, 0xdd00),
        colourFront                 (0xc500, 0xcc00, 0xc500), //(0xbb00, 0xbb00, 0xbb00),
        colourText                  (0x0000, 0x0000, 0x0000),
        colourTextInactive          (0x6a00, 0x6a00, 0x6a00),
        colourDefaultBg             (0x6300, 0xce00, 0xed00),
        colourTextEntry             (0xff00, 0xff00, 0xff00), // (0xee00, 0xee00, 0xee00),
        colourScrollBackground      (0xdd00, 0xdd00, 0xdd00),

        colourButtonNormal          (0xb300, 0xb000, 0xb300), // (0x9900, 0x9900, 0x9900),
        colourButtonMouseOver       (0x8b00, 0x8b00, 0xb200), // (0x7700, 0x7700, 0x9900),
        colourButtonMousePress      (0x6c00, 0x6c00, 0x9100), // (0x5900, 0x5900, 0x7700),
        colourButtonSelected        (0x7900, 0x7900, 0xa100), // (0x6600, 0x6600, 0x8800),
        colourButtonInactive        (0xd000, 0xd000, 0xd000),
        colourWindowDecorationBorder(0x7f00, 0x7f00, 0x7f00),
        colourWindowDecorationFront (0x5b00, 0x9800, 0xde00),
        colourWindowDecorationText  (0xff00, 0xff00, 0xff00),

        font(0),
        fontCursor(0),
        
        lastFillColour(0) {
            
        normalComponentWidth = 18.0f;    // Normal component width   (universal button width, as of 0.42 only for scrollbar puck)
        normalComponentHeight = 24.0f;   // Normal component height  (universal button height value)
        uiBorder = 4.0f;                 // UI border
        customTextCharWidth = 7.0f;      // This should match the X font we are using.
        customTitleTextCharWidth = 7.0f; // Currently we don't implement title text anyway.
        
        windowDecorationNormalComponentWidth = normalComponentHeight; // Same width as height, make decoration buttons square!
        windowDecorationNormalComponentHeight = normalComponentHeight;
        windowDecorationUiBorder = uiBorder + 1;
        
        // Set custom drawing colours.
        customColourMainText        = &colourText;
        customColourSecondaryText   = &colourText;
        customColourInactiveText    = &colourTextInactive;
        customColourBackplaneMain   = &colourFront;
        customColourBackplaneLight  = &colourFront;
        customColourBackplaneDark   = &colourBack;
    }

    BasicTheme::~BasicTheme(void) {
        // Make sure destroy gets called, if init has been called, but
        // destroy has not.
        callDestroy();
    }

    rascUIxcb::Colour * BasicTheme::getButtonColour(rascUI::State state) {
        switch(state) {
            case rascUI::State::normal:     return &colourButtonNormal;
            case rascUI::State::mouseOver:  return &colourButtonMouseOver;
            case rascUI::State::mousePress: return &colourButtonMousePress;
            case rascUI::State::selected:   return &colourButtonSelected;
            default:                        return &colourButtonInactive;
        }
    }
    
    rascUIxcb::Colour * BasicTheme::getTextColour(rascUI::State state) {
        switch(state) {
            case rascUI::State::inactive: return &colourTextInactive;
            default:                      return &colourText;
        }
    }
    
    void BasicTheme::drawTextInternal(const rascUI::Location & location, const rascUI::Rectangle & rectangle, const std::string & string, rascUIxcb::Colour & colour) const {
        xcb_change_gc(getConnection(), getGraphics(), XCB_GC_BACKGROUND, &lastFillColour);
        colour.setForeground(getGraphics());
        int xOff, yOff;
        rascUI::State state = location.getState();
        if (state == rascUI::State::mousePress || state == rascUI::State::selected) {
            xOff = 5; yOff = 17;
        } else {
            xOff = 4; yOff = 16;
        }
        const unsigned int maxChars = std::max<int>(0, location.cache.width - xOff) / getFontCharWidth();
        const uint8_t len = std::min<unsigned int>(std::min<size_t>(string.length(), maxChars), std::numeric_limits<uint8_t>::max());
        
        xcb_image_text_8(getConnection(), len, getDrawable(), getGraphics(), rectangle.x + xOff, rectangle.y + yOff, string.c_str());
    }
    
    xcb_cursor_t BasicTheme::createCursor(const uint16_t character) const {
        xcb_cursor_t id = xcb_generate_id(getConnection());
        
        // See: 
        // https://xcb.freedesktop.org/tutorial/mousecursors/
        // http://web.archive.org/web/20240703172705/https://xcb.freedesktop.org/tutorial/mousecursors/
        xcb_create_glyph_cursor(getConnection(), // connection
            id, // cursor id
            fontCursor, // source font
            fontCursor, // mask font
            character, // source character
            character + 1, // mask character, for cursors this is +1, see https://tronche.com/gui/x/xlib/appendix/b/  (http://web.archive.org/web/20240208200819/https://tronche.com/gui/x/xlib/appendix/b/)
            0x0000, 0x0000, 0x0000, // foreground rgb
            0xff00, 0xff00, 0xff00); // background rgb
        return id;
    }
    
    unsigned int BasicTheme::getFontCharWidth(void) {
        return 7;
    }
    
    void BasicTheme::init(void * userData) {
        XcbThemeBase::init(userData);
        
        // Set the context for each of our colours.
        rascUIxcb::Context * context = getContext();
        colourOutline.setContext(context);
        colourBack.setContext(context);
        colourFront.setContext(context);
        colourText.setContext(context);
        colourTextInactive.setContext(context);
        colourDefaultBg.setContext(context);
        colourTextEntry.setContext(context);
        colourScrollBackground.setContext(context);
        colourButtonNormal.setContext(context);
        colourButtonMouseOver.setContext(context);
        colourButtonMousePress.setContext(context);
        colourButtonSelected.setContext(context);
        colourButtonInactive.setContext(context);
        colourWindowDecorationBorder.setContext(context);
        colourWindowDecorationFront.setContext(context);
        colourWindowDecorationText.setContext(context);

        // Generate an ID and set up our text font.
        font = xcb_generate_id(getConnection());
        xcb_open_font(getConnection(), font, 4, "7x13"); // name length, name.
        
        // Generate an ID and set up our cursor font.
        fontCursor = xcb_generate_id(getConnection());
        xcb_open_font(getConnection(), fontCursor, 6, "cursor"); // name length, name.
        
        // Create cursors.
        cursorArrow = createCursor(XC_left_ptr);
        cursorHand = createCursor(XC_hand2);
        cursorText = createCursor(XC_xterm);
        cursorMove = createCursor(XC_fleur);
        cursorMoveHorizontally = createCursor(XC_sb_h_double_arrow);
        cursorMoveVertically = createCursor(XC_sb_v_double_arrow);
        cursorMoveRight = createCursor(XC_right_side);
        cursorMoveUpRight = createCursor(XC_top_right_corner);
        cursorMoveUp = createCursor(XC_top_side);
        cursorMoveUpLeft = createCursor(XC_top_left_corner);
        cursorMoveLeft = createCursor(XC_left_side);
        cursorMoveDownLeft = createCursor(XC_bottom_left_corner);
        cursorMoveDown = createCursor(XC_bottom_side);
        cursorMoveDownRight = createCursor(XC_bottom_right_corner);
        
        // Set arrow cursor by default.
        context->setRootCursor(cursorArrow);
    }
    
    void BasicTheme::beforeDraw(void * userData) {
        XcbThemeBase::beforeDraw(userData);
        
        // Set the window's graphics context to use our font.
        uint32_t mask = XCB_GC_FONT;
        xcb_change_gc(getConnection(), getGraphics(), mask, &font);
        
        // Find the relevant cursor, according to rascUI's current cursor type.
        rascUIxcb::Window & window = *getWindow();
        xcb_cursor_t newCursor;
        switch(window.getHighestPriorityCursorType()) {
            case rascUI::CursorType::arrow: newCursor = cursorArrow; break;
            case rascUI::CursorType::hand: newCursor = cursorHand; break;
            case rascUI::CursorType::text: newCursor = cursorText; break;
            case rascUI::CursorType::move: newCursor = cursorMove; break;
            case rascUI::CursorType::moveHorizontally: newCursor = cursorMoveHorizontally; break;
            case rascUI::CursorType::moveVertically: newCursor = cursorMoveVertically; break;
            case rascUI::CursorType::moveRight: newCursor = cursorMoveRight; break;
            case rascUI::CursorType::moveUpRight: newCursor = cursorMoveUpRight; break;
            case rascUI::CursorType::moveUp: newCursor = cursorMoveUp; break;
            case rascUI::CursorType::moveUpLeft: newCursor = cursorMoveUpLeft; break;
            case rascUI::CursorType::moveLeft: newCursor = cursorMoveLeft; break;
            case rascUI::CursorType::moveDownLeft: newCursor = cursorMoveDownLeft; break;
            case rascUI::CursorType::moveDown: newCursor = cursorMoveDown; break;
            case rascUI::CursorType::moveDownRight: newCursor = cursorMoveDownRight; break;
            default: newCursor = XCB_CURSOR_NONE; break;
        }
        
        // Update cursor if needed.
        if (window.xcbGetLastCursor() != newCursor) {
            xcb_change_window_attributes(getConnection(), window.xcbGetWindow(), XCB_CW_CURSOR, &newCursor);
            window.xcbGetLastCursor() = newCursor;
        }
    }
    
    void BasicTheme::destroy(void) {
        rascUIxcb::Context * context = getContext();
        
        // Clear the default cursor.
        context->setRootCursor(XCB_CURSOR_NONE);
        
        // Free the cursors.
        xcb_free_cursor(getConnection(), cursorArrow);
        xcb_free_cursor(getConnection(), cursorHand);
        xcb_free_cursor(getConnection(), cursorText);
        xcb_free_cursor(getConnection(), cursorMove);
        xcb_free_cursor(getConnection(), cursorMoveHorizontally);
        xcb_free_cursor(getConnection(), cursorMoveVertically);
        xcb_free_cursor(getConnection(), cursorMoveRight);
        xcb_free_cursor(getConnection(), cursorMoveUpRight);
        xcb_free_cursor(getConnection(), cursorMoveUp);
        xcb_free_cursor(getConnection(), cursorMoveUpLeft);
        xcb_free_cursor(getConnection(), cursorMoveLeft);
        xcb_free_cursor(getConnection(), cursorMoveDownLeft);
        xcb_free_cursor(getConnection(), cursorMoveDown);
        xcb_free_cursor(getConnection(), cursorMoveDownRight);
        
        // Free the fonts.
        xcb_close_font(getConnection(), fontCursor);
        xcb_close_font(getConnection(), font);
        
        // Set the context, for each of our colours, to NULL (no Context).
        colourOutline.setContext(NULL);
        colourBack.setContext(NULL);
        colourFront.setContext(NULL);
        colourText.setContext(NULL);
        colourTextInactive.setContext(NULL);
        colourDefaultBg.setContext(NULL);
        colourTextEntry.setContext(NULL);
        colourScrollBackground.setContext(NULL);
        colourButtonNormal.setContext(NULL);
        colourButtonMouseOver.setContext(NULL);
        colourButtonMousePress.setContext(NULL);
        colourButtonSelected.setContext(NULL);
        colourButtonInactive.setContext(NULL);
        colourWindowDecorationBorder.setContext(NULL);
        colourWindowDecorationFront.setContext(NULL);
        colourWindowDecorationText.setContext(NULL);
        
        XcbThemeBase::destroy();
    }
    
    void BasicTheme::onChangeViewArea(const rascUI::Rectangle & view) {
        XcbThemeBase::onChangeViewArea(view);
        
        // Only draw window background if context's config hint says we should.
        if (getContext()->xcbShouldDrawWindowBackgrounds()) {
            // If the window (context) is not double buffered, set the window's back
            // pixel to the background colour. This ensures that this colour is used
            // each time the window resizes, or each time the pixmap is reset because
            // of an expose event.
            uint32_t backgroundColour = colourDefaultBg.get();
            if (!getContext()->xcbIsDoubleBufferEnabled()) {
                xcb_change_window_attributes(getConnection(), getWindow()->xcbGetWindow(), XCB_CW_BACK_PIXEL, &backgroundColour);
            }
            
            // ALWAYS then fill the drawable with a rectangle of the background colour.
            // If double buffering is disabled, this will provide the INITIAL background colour.
            // Otherwise, this will ensure that the newly allocated off-screen buffer contains
            // the right colour.
            GET_XCB_RECT(view)
            xcb_change_gc(getConnection(), getGraphics(), XCB_GC_FOREGROUND, &backgroundColour);
            xcb_poly_fill_rectangle(getConnection(), getDrawable(), getGraphics(), 1, &rect);
        }
    }

    void BasicTheme::drawBackPanel(const rascUI::Location & location) {
        GET_XCB_RECT(location.cache)
        DRAW_RECT(rect, &colourBack, true)
    }

    void BasicTheme::drawFrontPanel(const rascUI::Location & location) {
        GET_XCB_RECT(location.cache)
        DRAW_OUTLINED_RECT(rect, &colourFront, true)
    }

    void BasicTheme::drawText(const rascUI::Location & location, const std::string & string) {
        drawTextInternal(location, location.cache, string, *getTextColour(location.getState()));
    }

    void BasicTheme::drawButton(const rascUI::Location & location) {
        GET_XCB_RECT(location.cache)
        DRAW_OUTLINED_RECT(rect, getButtonColour(location.getState()), true)
    }

    void BasicTheme::drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) {
        GET_XCB_RECT(location.cache)
        rascUIxcb::Colour * colour = getButtonColour(location.getState());
        DRAW_OUTLINED_RECT(rect, colour, false)

        // Nothing to draw if rectangle is too small.
        if (rect.width < 9 || rect.height < 9) return;

        rect.x += 4; rect.y += 4; rect.width -= 8; rect.height -= 8;
        colour = (location.getState() == rascUI::State::inactive ? colour : &colourTextEntry);
        DRAW_OUTLINED_RECT(rect, colour, false)

        colour->setBackground(getGraphics());
        getTextColour(location.getState())->setForeground(getGraphics());

        const unsigned int maxChars = rect.width / getFontCharWidth();
        const uint8_t len = std::min<unsigned int>(std::min<size_t>(string.length(), maxChars), std::numeric_limits<uint8_t>::max());
        
        int xOff, yOff;
        rascUI::State state = location.getState();
        if (state == rascUI::State::mousePress || state == rascUI::State::selected) {
            xOff = 4; yOff = 13;
        } else {
            xOff = 3; yOff = 12;
        }
        xcb_image_text_8(getConnection(), len, getDrawable(), getGraphics(), rect.x + xOff, rect.y + yOff, string.c_str());

        if (drawCursor && cursorPos < maxChars) {
            rect.x += TEXT_CHAR_WIDTH * cursorPos + 3;
            rect.y += 2; rect.height -= 4;
            rect.width = 2;
            xcb_poly_fill_rectangle(getConnection(), getDrawable(), getGraphics(), 1, &rect);
        }
    }

    void BasicTheme::drawScrollBarBackground(const rascUI::Location & location) {
        GET_XCB_RECT(location.cache)
        DRAW_OUTLINED_RECT(rect, &colourScrollBackground, true)
    }

    void BasicTheme::drawScrollContentsBackground(const rascUI::Location & location) {
        GET_XCB_RECT(location.cache)
        DRAW_RECT(rect, &colourScrollBackground, true)
    }

    void BasicTheme::drawScrollPuck(const rascUI::Location & location, bool vertical) {
        drawButton(location);
    }

    void BasicTheme::drawScrollUpButton(const rascUI::Location & location, bool vertical) {
        drawButton(location);
    }

    void BasicTheme::drawScrollDownButton(const rascUI::Location & location, bool vertical) {
        drawButton(location);
    }

    void BasicTheme::drawFadePanel(const rascUI::Location & location) {
    }
    
    void BasicTheme::drawCheckboxButton(const rascUI::Location & location) {
    }
    
    void BasicTheme::drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) {
    }
    
    void BasicTheme::drawTooltipBackground(const rascUI::Location & location) {
    }
    
    void BasicTheme::drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) {
    }
    
    void BasicTheme::drawWindowDecorationBackPanel(const rascUI::Location & location) {
        GET_XCB_RECT(location.cache)
        DRAW_OUTLINED_RECT(rect, &colourWindowDecorationBorder, true)
        
        xcb_rectangle_t innerOutline;
        innerOutline.x = rect.x - 1 + (windowDecorationUiBorder * location.xScale);
        innerOutline.y = rect.y - 1 + ((windowDecorationUiBorder + windowDecorationNormalComponentHeight + uiBorder) * location.yScale);
        innerOutline.width = rect.width + 1 - (2.0f * windowDecorationUiBorder * location.xScale);
        innerOutline.height = rect.height + 1 - (((2.0f * windowDecorationUiBorder) + windowDecorationNormalComponentHeight + uiBorder) * location.yScale);

        xcb_poly_rectangle(getConnection(), getDrawable(), getGraphics(), 1, &innerOutline);
    }
    
    void BasicTheme::drawWindowDecorationFrontPanel(const rascUI::Location & location) {
        GET_XCB_RECT(location.cache)
        DRAW_OUTLINED_RECT(rect, &colourWindowDecorationFront, true)
    }
    
    void BasicTheme::drawWindowDecorationText(const rascUI::Location & location, const std::string & string) {
        drawTextInternal(location, location.cache, string, colourWindowDecorationText);
    }
    
    void BasicTheme::drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) {
        GET_XCB_RECT(location.cache)
        DRAW_OUTLINED_RECT(rect, getButtonColour(location.getState()), true)
        // Offset text by a bit to centre things.
        rascUI::Rectangle offsetRectangle = location.cache;
        offsetRectangle.x += (location.xScale * 5.0f);
        offsetRectangle.y -= location.yScale;
        drawTextInternal(location, offsetRectangle, "x", *getTextColour(location.getState()));
    }
    
    void BasicTheme::getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) {
        if (state == rascUI::State::mousePress || state == rascUI::State::selected) {
            offX = 5; offY = 17;
        } else {
            offX = 4; offY = 16;
        }
    }
}

// --------------------------------------------------------------------

const char * getApiVersion(void) {
    return "theme-rascUI-0.7";
}

rascUI::Theme * createTheme(void) {
    return new xcbBasicTheme::BasicTheme();
}

void deleteTheme(rascUI::Theme * theme) {
    delete theme;
}

#endif
