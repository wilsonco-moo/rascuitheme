/*
 * BasicTheme.h
 *
 *  Created on: 20 Jun 2019
 *      Author: wilson
 */

#ifndef RASCUITHEME_THEMES_XCB_BASICTHEME_BASICTHEME_H_
#define RASCUITHEME_THEMES_XCB_BASICTHEME_BASICTHEME_H_

#include <rascUIxcb/theme/XcbThemeBase.h>
#include <rascUIxcb/util/Colour.h>
#include <rascUI/util/State.h>
#include <cstdint>

namespace xcbBasicTheme {

    /**
     * BasicTheme is intended as the simplest possible Theme implementation for
     * xcb. It uses only X core fonts, and very simple drawing consisting
     * of rectangles.
     * 
     * As a result, BasicTheme will run (and launch) extremely quickly, even
     * on very antiquated hardware, and even through a remote ssh connection.
     * Note that unlike ScalableTheme, BasicTheme does NOT leak any memory.
     * If running using valgrind for debugging purposes, BasicTheme should
     * be used in preference.
     * Note however that BasicTheme only really supports a UI scale of 1.
     * UI scales other than that *will* work, but (for simplicity) text and
     * positioning does not scale. If scaling is required, use ScalableTheme.
     */
    class BasicTheme : public rascUIxcb::XcbThemeBase {
    private:
        // Colours
        rascUIxcb::Colour colourOutline, colourBack, colourFront, colourText, colourTextInactive, colourDefaultBg, colourTextEntry,
                          colourScrollBackground, colourButtonNormal, colourButtonMouseOver, colourButtonMousePress, colourButtonSelected,
                          colourButtonInactive, colourWindowDecorationBorder, colourWindowDecorationFront, colourWindowDecorationText;

        // Fonts
        xcb_font_t font, fontCursor;
        
        // Cursors
        xcb_cursor_t cursorArrow, cursorHand, cursorText, cursorMove, cursorMoveHorizontally, cursorMoveVertically, cursorMoveRight,
            cursorMoveUpRight, cursorMoveUp, cursorMoveUpLeft, cursorMoveLeft, cursorMoveDownLeft, cursorMoveDown, cursorMoveDownRight;
        
        // Each time we draw something, we keep a record of it's fill colour.
        // This is used as the background colour for text drawn over the top of it.
        uint32_t lastFillColour;

    public:
        BasicTheme(void);
        virtual ~BasicTheme(void);

    private:
        rascUIxcb::Colour * getButtonColour(rascUI::State state);
        rascUIxcb::Colour * getTextColour(rascUI::State state);
        void drawTextInternal(const rascUI::Location & location, const rascUI::Rectangle & rectangle, const std::string & string, rascUIxcb::Colour & colour) const;
        xcb_cursor_t createCursor(const uint16_t character) const;
        // Gets current font character width, for now just returns a constant.
        // Todo: Choose font based on scale!
        static unsigned int getFontCharWidth(void);
        
    protected:
        virtual void init(void * userData) override;
    public:
        virtual void beforeDraw(void * userData) override;
    protected:
        virtual void destroy(void) override;
    public:
        virtual void onChangeViewArea(const rascUI::Rectangle & view) override;
        virtual void drawBackPanel(const rascUI::Location & location) override;
        virtual void drawFrontPanel(const rascUI::Location & location) override;
        virtual void drawText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawButton(const rascUI::Location & location) override;
        virtual void drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) override;
        virtual void drawScrollBarBackground(const rascUI::Location & location) override;
        virtual void drawScrollContentsBackground(const rascUI::Location & location) override;
        virtual void drawScrollPuck(const rascUI::Location & location, bool vertical) override;
        virtual void drawScrollUpButton(const rascUI::Location & location, bool vertical) override;
        virtual void drawScrollDownButton(const rascUI::Location & location, bool vertical) override;
        virtual void drawFadePanel(const rascUI::Location & location) override;
        virtual void drawCheckboxButton(const rascUI::Location & location) override;
        virtual void drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) override;
        virtual void drawTooltipBackground(const rascUI::Location & location) override;
        virtual void drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) override;
        virtual void drawWindowDecorationBackPanel(const rascUI::Location & location) override;
        virtual void drawWindowDecorationFrontPanel(const rascUI::Location & location) override;
        virtual void drawWindowDecorationText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) override;
        virtual void getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) override;
    };
}

// --------------------------------------------------------------------

#ifdef _WIN32
    #define APIFUNC extern "C" __declspec(dllexport)
#else
    #define APIFUNC extern "C"
#endif

APIFUNC const char * getApiVersion(void);

APIFUNC rascUI::Theme * createTheme(void);

APIFUNC void deleteTheme(rascUI::Theme * theme);

#endif
