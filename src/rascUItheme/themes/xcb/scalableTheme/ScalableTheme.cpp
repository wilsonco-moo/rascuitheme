/*
 * ScalableTheme.cpp
 *
 *  Created on: 31 May 2020
 *      Author: wilson
 */

// Force enable the theme if requested.
#ifdef FORCE_ENABLE_XCB_SCALABLE_THEME
    #ifndef ENABLE_RASCUI_THEME
        #define ENABLE_RASCUI_THEME
    #endif
#endif

// Only compile if this theme is enabled.
#ifdef ENABLE_RASCUI_THEME

#include "ScalableTheme.h"

#include <rascUIxcb/platform/Context.h>
#include <rascUIxcb/platform/Window.h>
#include <rascUI/util/Location.h>
#include <X11/Xlib.h>
#include <algorithm>
#include <cstddef>
#include <string>

// Creates an xcb xcb_rectangle_t (called rect), from the specified rascUI
// rectangle.
#define GET_XCB_RECT(oldRect)          \
    xcb_rectangle_t rect = {           \
        (int16_t)(oldRect).x,          \
        (int16_t)(oldRect).y,          \
        (uint16_t)(oldRect).width,     \
        (uint16_t)(oldRect).height     \
    };

namespace xcbScalableTheme {

    ScalableTheme::ScalableTheme(void) :
        rascUIxcb::XcbThemeBase(),
        
        // Initialise all colours with RGB values. Note that each channel is 16 bit.
        #define X(name, code) name((code >> 8) & 0xff00, code & 0xff00, (code << 8) & 0xff00),
        SCALABLE_THEME_COLOURS
        #undef X
        
        xftFont(NULL),
        xftDraw(NULL),
        lastFontScale(0.0f),
        lastFillColour(0) {
        
        normalComponentWidth = 18.0f;    // Normal component width   (universal button width, as of 0.42 only for scrollbar puck)
        normalComponentHeight = 24.0f;   // Normal component height  (universal button height value)
        uiBorder = 4.0f;                 // UI border
        customTextCharWidth = 7.0f;      // This should match the X font we are using.
        customTitleTextCharWidth = 7.0f; // Currently we don't implement title text anyway.
        
        windowDecorationNormalComponentWidth = normalComponentHeight;
        windowDecorationNormalComponentHeight = normalComponentHeight;
        windowDecorationUiBorder = 5.0f;
        
        // Set custom drawing colours.
        customColourMainText        = &colourText;
        customColourSecondaryText   = &colourText;
        customColourInactiveText    = &colourButtonMousePress;
        customColourBackplaneMain   = &colourFront;
        customColourBackplaneLight  = &colourFront;
        customColourBackplaneDark   = &colourBack;
    }

    ScalableTheme::~ScalableTheme(void) {
        // Make sure destroy gets called, if init has been called, but
        // destroy has not.
        callDestroy();
    }
    
    void ScalableTheme::deleteXftFont(void) {
        // Do nothing if we don't already have a font anyway.
        if (xftFont == NULL) return;
        
        // Close the font and set it to NULL, so we know that we don't have a font any more.
        XftFontClose(getContext()->xcbGetXlibDisplay(), xftFont);
        xftFont = NULL;
    }
    
    void ScalableTheme::updateXftFont(GLfloat uiScale) {
        // Do nothing if the scale hasn't changed, and there is already a font.
        if (uiScale == lastFontScale && xftFont != NULL) return;
        lastFontScale = uiScale;
        
        // Delete the old font.
        deleteXftFont();
        
        // Allocate the new font.
        Display * display = getContext()->xcbGetXlibDisplay();
        xftFont = XftFontOpen(display, DefaultScreen(display),
                              XFT_FAMILY, XftTypeString, "sans",
                              XFT_SIZE, XftTypeDouble, 10.5f * uiScale,
                              NULL);
    }
    
    void ScalableTheme::getButtonColour(ButtonCol * col, rascUI::State state) {
        switch(state) {
        case rascUI::State::normal:
            *col = {colourButtonOutlineTop.get(),
                    colourButtonNormal.get(),
                    colourButtonOutlineBottom.get(),
                    false};
            break;
        case rascUI::State::mouseOver:
            *col = {colourButtonSelectedOutlineTop.get(),
                    colourButtonMouseOver.get(),
                    colourButtonSelectedOutlineBottom.get(),
                    false};
            break;
        case rascUI::State::mousePress:
            *col = {colourButtonSelectedOutlineBottom.get(),
                    colourButtonMousePress.get(),
                    colourButtonSelectedOutlineTop.get(),
                    true};
            break;
        case rascUI::State::selected:
            *col = {colourButtonSelectedOutlineBottom.get(),
                    colourButtonSelected.get(),
                    colourButtonSelectedOutlineTop.get(),
                    true};
            break;
        default: // Inactive
            *col = {colourButtonOutlineTop.get(),
                    colourButtonInactive.get(),
                    colourButtonOutlineBottom.get(),
                    false};
            break;
        }
    }
    
    void ScalableTheme::drawBorderedRect(const xcb_rectangle_t & rectangle, const ButtonCol & buttonCol, GLfloat uiScale) {
        int topBorder, bottomBorder;
        if (buttonCol.inverted) {
            topBorder = (int)(uiScale * 2.0f);
            bottomBorder = (int)(uiScale * 1.0f);
        } else {
            topBorder = (int)(uiScale * 1.0f);
            bottomBorder = (int)(uiScale * 2.0f);
        }
        xcb_rectangle_t drawRectangles[2];
        int widthMinusBottomBord = rectangle.width - bottomBorder,
            widthMinusTopBord = rectangle.width - topBorder,
            heightMinusBottomBord = rectangle.height - bottomBorder,
            heightMinusTopBord = rectangle.height - topBorder;
        
        drawRectangles[0] = {rectangle.x, rectangle.y, (uint16_t)widthMinusBottomBord, (uint16_t)topBorder};
        drawRectangles[1] = {rectangle.x, (int16_t)(rectangle.y + topBorder), (uint16_t)topBorder, (uint16_t)heightMinusTopBord};
        xcb_change_gc(getConnection(), getGraphics(), XCB_GC_FOREGROUND, &buttonCol.topCol);
        xcb_poly_fill_rectangle(getConnection(), getDrawable(), getGraphics(), 2, drawRectangles);
        
        drawRectangles[0] = {(int16_t)(rectangle.x + topBorder), (int16_t)(rectangle.y + rectangle.height - bottomBorder), (uint16_t)widthMinusTopBord, (uint16_t)bottomBorder};
        drawRectangles[1] = {(int16_t)(rectangle.x + rectangle.width - bottomBorder), rectangle.y, (uint16_t)bottomBorder, (uint16_t)heightMinusBottomBord};
        xcb_change_gc(getConnection(), getGraphics(), XCB_GC_FOREGROUND, &buttonCol.bottomCol);
        xcb_poly_fill_rectangle(getConnection(), getDrawable(), getGraphics(), 2, drawRectangles);
        
        drawRectangles[0] = {(int16_t)(rectangle.x + topBorder), (int16_t)(rectangle.y + topBorder), (uint16_t)(widthMinusTopBord - bottomBorder), (uint16_t)(heightMinusTopBord - bottomBorder)};
        xcb_change_gc(getConnection(), getGraphics(), XCB_GC_FOREGROUND, &buttonCol.middleCol);
        xcb_poly_fill_rectangle(getConnection(), getDrawable(), getGraphics(), 1, drawRectangles);
    }
    
    void ScalableTheme::getTextOff(int * x, int * y, rascUI::State state, GLfloat uiScale) {
        if (state == rascUI::State::mousePress || state == rascUI::State::selected) {
            *x = (int)(5.0f * uiScale);
            *y = (int)(17.0f * uiScale);
        } else {
            *x = (int)(4.0f * uiScale);
            *y = (int)(16.0f * uiScale);
        }
    }
    
    void ScalableTheme::drawTextPosState(int x, int y, rascUI::State state, const std::string & text) {
        drawTextPosState(x, y, state, text.c_str(), text.length());
    }
    
    void ScalableTheme::drawTextPosState(int x, int y, rascUI::State state, const char * text, size_t textLength) {
        rascUIxcb::Colour * colour;
        if (state == rascUI::State::inactive) {
            colour = &colourTextInactive;
        } else {
            colour = &colourText;
        }
        
        // Build an XftColor struct from the xcb text colour.
        XftColor xftColour;
        xftColour.pixel       = colour->get();
        xftColour.color.red   = colour->getRed();
        xftColour.color.green = colour->getGreen();
        xftColour.color.blue  = colour->getBlue();
        xftColour.color.alpha = (unsigned short)-1;

        // Draw the text using xft.
        XftDrawStringUtf8(xftDraw, &xftColour, xftFont, x, y, (const XftChar8 *)text, textLength);
    }
    
    void ScalableTheme::drawTextPosColour(int x, int y, rascUIxcb::Colour & colour, const std::string & text) const {
        // Build an XftColor struct from the xcb text colour.
        XftColor xftColour;
        xftColour.pixel       = colour.get();
        xftColour.color.red   = colour.getRed();
        xftColour.color.green = colour.getGreen();
        xftColour.color.blue  = colour.getBlue();
        xftColour.color.alpha = (unsigned short)-1;

        // Draw the text using xft.
        XftDrawStringUtf8(xftDraw, &xftColour, xftFont, x, y, (const XftChar8 *)text.c_str(), text.length());
    }
    
    int ScalableTheme::getTextTrueWidth(const std::string & text, size_t length) {
        // Use the letter M as reference, since it is typically quite wide.
        // Get extents of text with reference (M) appended to it.
        std::string baseStr = text.substr(0, length);
        baseStr.append(1, 'M');
        XGlyphInfo extents;
        XftTextExtentsUtf8(getContext()->xcbGetXlibDisplay(), xftFont, (const XftChar8 *)baseStr.c_str(), baseStr.length(), &extents);
        int width = extents.width;
        
        // Get width of letter M, and subtract it from previous extents.
        XftTextExtentsUtf8(getContext()->xcbGetXlibDisplay(), xftFont, (const XftChar8 *)"M", 1, &extents);
        return width - extents.width;
    }
    
    void ScalableTheme::init(void * userData) {
        XcbThemeBase::init(userData);
        
        // Set the context for each of our colours.
        rascUIxcb::Context * context = getContext();
        #define X(name, code) name.setContext(context);
        SCALABLE_THEME_COLOURS
        #undef X
        
        // If the context has an x scale, update the font on initialisation.
        if (context->defaultXScalePtr != NULL) {
            updateXftFont(*context->defaultXScalePtr);
        }
        
        // Create the cursor context.
        cursorContext = NULL;
        xcb_cursor_context_new(getConnection(), context->xcbGetScreen(), &cursorContext);
        if (cursorContext != NULL) {
            // If cursor context succeeded, create all cursors, store in array.
            #define X(name, cursorName) \
                cursors[CursorIndices::name] = xcb_cursor_load_cursor(cursorContext, cursorName);
            SCALABLE_THEME_CURSORS
            #undef X
            
            // Set arrow cursor by default.
            context->setRootCursor(cursors[CursorIndices::arrow]);
        } else {
            // If cursor context failed, just set all cursors to none.
            cursors.fill(XCB_CURSOR_NONE);
        }
    }
    
    void ScalableTheme::beforeDraw(void * userData) {
        XcbThemeBase::beforeDraw(userData);
        
        // Create ourselves a draw for our font, at the start of drawing.
        Display * display = getContext()->xcbGetXlibDisplay();
        int screen = DefaultScreen(display);
        xftDraw = XftDrawCreate(display, getDrawable(), DefaultVisual(display, screen), DefaultColormap(display, screen));
        
        // Find the relevant cursor, according to rascUI's current cursor type.
        // Use the X macro to map from rascUI cursor enum to our internal array
        // of cursors.
        rascUIxcb::Window & window = *getWindow();
        xcb_cursor_t newCursor;
        switch(window.getHighestPriorityCursorType()) {
            #define X(name, ...) \
                case rascUI::CursorType::name: \
                    newCursor = cursors[CursorIndices::name]; \
                    break;
            SCALABLE_THEME_CURSORS
            #undef X
            default:
                newCursor = XCB_CURSOR_NONE;
                break;
        }

        // Update cursor if needed.
        if (window.xcbGetLastCursor() != newCursor) {
            xcb_change_window_attributes(getConnection(), window.xcbGetWindow(), XCB_CW_CURSOR, &newCursor);
            window.xcbGetLastCursor() = newCursor;
        }
    }
    
    void ScalableTheme::afterDraw(const rascUI::Rectangle & drawnArea) {
        // After we have finished drawing, delete our font's draw.
        // This must be done, as we can no longer rely on the existence of our drawable,
        // after this point.
        XftDrawDestroy(xftDraw);
        
        XcbThemeBase::afterDraw(drawnArea);
    }
    
    void ScalableTheme::destroy(void) {
        if (cursorContext != NULL) {
            // Clear the default cursor.
            getContext()->setRootCursor(XCB_CURSOR_NONE);
            
            // Free the cursors.
            for (const xcb_cursor_t cursor : cursors) {
                xcb_free_cursor(getConnection(), cursor);
            }
            
            // Free the cursor context.
            xcb_cursor_context_free(cursorContext);
        }
        
        // Free the font.
        deleteXftFont();
    
        // Set the context, for each of our colours, to NULL (no Context).
        #define X(name, code) name.setContext(NULL);
        SCALABLE_THEME_COLOURS
        #undef X
        
        XcbThemeBase::destroy();
    }
    
    void ScalableTheme::onChangeViewArea(const rascUI::Rectangle & view) {
        XcbThemeBase::onChangeViewArea(view);
        
        // Only draw window background if context's config hint says we should.
        if (getContext()->xcbShouldDrawWindowBackgrounds()) {
            // If the window (context) is not double buffered, set the window's back
            // pixel to the background colour. This ensures that this colour is used
            // each time the window resizes, or each time the pixmap is reset because
            // of an expose event.
            uint32_t backgroundColour = colourDefaultBg.get();
            if (!getContext()->xcbIsDoubleBufferEnabled()) {
                xcb_change_window_attributes(getConnection(), getWindow()->xcbGetWindow(), XCB_CW_BACK_PIXEL, &backgroundColour);
            }
            
            // ALWAYS then fill the drawable with a rectangle of the background colour.
            // If double buffering is disabled, this will provide the INITIAL background colour.
            // Otherwise, this will ensure that the newly allocated off-screen buffer contains
            // the right colour.
            GET_XCB_RECT(view)
            xcb_change_gc(getConnection(), getGraphics(), XCB_GC_FOREGROUND, &backgroundColour);
            xcb_poly_fill_rectangle(getConnection(), getDrawable(), getGraphics(), 1, &rect);
        }
        
        // Update the drawable within our font's draw. It will likely have changed
        // since beforeDraw, due to double buffering.
        XftDrawChange(xftDraw, getDrawable());
        
        // Update the xft font using the window's UI scale.
        // Do this here since we get an onChangeViewArea call each time the ui scale changes,
        // and UI scale is not updated until this call.
        updateXftFont(getWindow()->getXScale());
    }

    void ScalableTheme::drawBackPanel(const rascUI::Location & location) {
        GET_XCB_RECT(location.cache)
        colourBack.setForeground(getGraphics());
        xcb_poly_fill_rectangle(getConnection(), getDrawable(), getGraphics(), 1, &rect);
    }

    void ScalableTheme::drawFrontPanel(const rascUI::Location & location) {
        GET_XCB_RECT(location.cache)
        ButtonCol buttonCol = { colourFrontOutlineTop.get(), colourFront.get(), colourFrontOutlineBottom.get(), false };
        drawBorderedRect(rect, buttonCol, location.xScale);
    }

    void ScalableTheme::drawText(const rascUI::Location & location, const std::string & string) {
        rascUI::State state = location.getState();
        
        // Figure out the x and y off.
        int xOff, yOff;
        getTextOff(&xOff, &yOff, state, location.xScale);
        
        // Draw the text.
        drawTextPosState(location.cache.x + xOff, location.cache.y + yOff, state, string);
    }

    void ScalableTheme::drawButton(const rascUI::Location & location) {
        GET_XCB_RECT(location.cache)
        ButtonCol buttonCol;
        getButtonColour(&buttonCol, location.getState());
        drawBorderedRect(rect, buttonCol, location.xScale);
    }

    void ScalableTheme::drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) {
        GET_XCB_RECT(location.cache)
        ButtonCol buttonCol;
        rascUI::State state = location.getState();
        int offset3 = (int)(3.0f * location.xScale),
            offset2 = (int)(2.0f * location.xScale);
        
        // Draw outer rectangle.
        getButtonColour(&buttonCol, state);
        drawBorderedRect(rect, buttonCol, location.xScale);
        
        // Set new middle colour to draw inner rectangle.
        switch(state) {
        case rascUI::State::normal:
            buttonCol.middleCol = colourTextEntryNormal.get();
            break;
        case rascUI::State::mouseOver:
            buttonCol.middleCol = colourTextEntryMouseOver.get();
            break;
        case rascUI::State::mousePress:
            buttonCol.middleCol = colourTextEntryMousePress.get();
            break;
        case rascUI::State::selected:
            buttonCol.middleCol = colourTextEntrySelected.get();
            break;
        default: // Inactive
            buttonCol.middleCol = colourTextEntryInactive.get();
            break;
        }
        
        // Shrink rectangle and draw inner rectangle.
        if (buttonCol.inverted) {
            rect.x += offset3; rect.y += offset3; 
        } else {
            rect.x += offset2; rect.y += offset2;
        }
        rect.width -= (offset2 + offset3); rect.height -= (offset2 + offset3);
        std::swap(buttonCol.topCol, buttonCol.bottomCol);
        buttonCol.inverted = !buttonCol.inverted;
        drawBorderedRect(rect, buttonCol, location.xScale);
        
        // Figure out offsets, add a small x offset, and draw text.
        int xOff, yOff;
        getTextOff(&xOff, &yOff, state, location.xScale);
        xOff += (int)(location.xScale * 2.0f);
        drawTextPosState(location.cache.x + xOff, location.cache.y + yOff, state, string);
        
        if (drawCursor) {
            // Get the width of the text.
            int textWidth = getTextTrueWidth(string, std::min(string.length(), cursorPos));
            
            // Offset the rectangle by the width, and make it cursor shaped.
            rect.x += (textWidth + offset2);
            rect.y += offset3; rect.height -= offset3 * 2;
            rect.width = offset2;
            
            // Draw a rectangle for the cursor.
            colourText.setForeground(getGraphics());
            xcb_poly_fill_rectangle(getConnection(), getDrawable(), getGraphics(), 1, &rect);
        }
    }

    void ScalableTheme::drawScrollBarBackground(const rascUI::Location & location) {
        // TODO stipple
        drawFrontPanel(location);
    }

    void ScalableTheme::drawScrollContentsBackground(const rascUI::Location & location) {
        //GET_XCB_RECT(location.cache)
        //DRAW_OUTLINED_RECT(rect, &colourScrollBackground, true)
        drawBackPanel(location);
    }

    void ScalableTheme::drawScrollPuck(const rascUI::Location & location, bool vertical) {
        drawButton(location);
    }

    void ScalableTheme::drawScrollUpButton(const rascUI::Location & location, bool vertical) {
        drawButton(location);
    }

    void ScalableTheme::drawScrollDownButton(const rascUI::Location & location, bool vertical) {
        drawButton(location);
    }

    void ScalableTheme::drawFadePanel(const rascUI::Location & location) {
    }
    
    void ScalableTheme::drawCheckboxButton(const rascUI::Location & location) {
    }
    
    void ScalableTheme::drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) {
    }
    
    void ScalableTheme::drawTooltipBackground(const rascUI::Location & location) {
    }
    
    void ScalableTheme::drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) {
    }
    
    void ScalableTheme::drawWindowDecorationBackPanel(const rascUI::Location & location) {
        xcb_rectangle_t drawRectangles[4];
        const unsigned int singleBorder = location.xScale * 1.0f;
        const unsigned int doubleBorder = location.xScale * 2.0f;
        const unsigned int exteriorBorder = location.xScale * windowDecorationUiBorder;
        const unsigned int topBorder = location.xScale * (uiBorder + windowDecorationNormalComponentHeight + windowDecorationUiBorder);
        const unsigned int width = location.cache.width;
        const unsigned int height = location.cache.height;
        
        drawRectangles[0] = { 0, 0, (uint16_t)(width - doubleBorder), (uint16_t)singleBorder };
        drawRectangles[1] = { 0, (int16_t)singleBorder, (uint16_t)singleBorder, (uint16_t)(height - singleBorder) };
        drawRectangles[2] = { (int16_t)exteriorBorder, (int16_t)(height - exteriorBorder), (uint16_t)(width - (2u * exteriorBorder) + singleBorder), (uint16_t)singleBorder };
        drawRectangles[3] = { (int16_t)(width - exteriorBorder), (int16_t)(topBorder - doubleBorder), (uint16_t)(singleBorder), (uint16_t)(height - topBorder - exteriorBorder + doubleBorder) };
        colourWindowDecorationTop.setForeground(getGraphics());
        xcb_poly_fill_rectangle(getConnection(), getDrawable(), getGraphics(), 4, drawRectangles);
        
        drawRectangles[0] = { (int16_t)singleBorder, (int16_t)(height - doubleBorder), (uint16_t)(width - singleBorder), (uint16_t)doubleBorder };
        drawRectangles[1] = { (int16_t)(width - doubleBorder), 0, (uint16_t)doubleBorder, (uint16_t)(height - doubleBorder) };
        drawRectangles[2] = { (int16_t)(exteriorBorder - doubleBorder), (int16_t)(topBorder - doubleBorder), (uint16_t)(width - (2u * exteriorBorder) + doubleBorder), (uint16_t)doubleBorder };
        drawRectangles[3] = { (int16_t)(exteriorBorder - doubleBorder), (int16_t)topBorder, (uint16_t)doubleBorder, (uint16_t)(height - topBorder - exteriorBorder + singleBorder) };
        colourWindowDecorationBottom.setForeground(getGraphics());
        xcb_poly_fill_rectangle(getConnection(), getDrawable(), getGraphics(), 4, drawRectangles);
        
        drawRectangles[0] = { (int16_t)singleBorder, (int16_t)singleBorder, (uint16_t)(width - singleBorder - doubleBorder), (uint16_t)(topBorder - singleBorder - doubleBorder) };
        drawRectangles[1] = { (int16_t)singleBorder, (int16_t)(topBorder - doubleBorder), (uint16_t)(exteriorBorder - singleBorder - doubleBorder), (uint16_t)(height - topBorder) };
        drawRectangles[2] = { (int16_t)(width - exteriorBorder + singleBorder), (int16_t)(topBorder - doubleBorder), (uint16_t)(exteriorBorder - singleBorder - doubleBorder), (uint16_t)(height - topBorder) };
        drawRectangles[3] = { (int16_t)singleBorder, (int16_t)(height - exteriorBorder + singleBorder), (uint16_t)(width - singleBorder - doubleBorder), (uint16_t)doubleBorder };
        colourWindowDecorationFill.setForeground(getGraphics());
        xcb_poly_fill_rectangle(getConnection(), getDrawable(), getGraphics(), 4, drawRectangles);
    }
    
    void ScalableTheme::drawWindowDecorationFrontPanel(const rascUI::Location & location) {
        // Offset location slightly for better visual alignment.
        // Ok since front panel won't repaint itself on its own (won't respond
        // to keyboard).
        rascUI::Rectangle offsetRect = location.cache;
        offsetRect.x -= location.xScale;
        offsetRect.width += location.xScale;
        GET_XCB_RECT(offsetRect)
        
        ButtonCol buttonCol = { colourWindowDecorationTop.get(), colourWindowDecorationFrontPanelFill.get(), colourWindowDecorationBottom.get(), false };
        drawBorderedRect(rect, buttonCol, location.xScale);
    }
    
    void ScalableTheme::drawWindowDecorationText(const rascUI::Location & location, const std::string & string) {
        // Figure out the x and y off.
        int xOff, yOff;
        getTextOff(&xOff, &yOff, location.getState(), location.xScale);
        
        // Offset location slightly for better visual alignment.
        // Ok since front panel won't repaint itself on its own (won't respond
        // to keyboard).
        const int x = location.cache.x - location.xScale + xOff;
        const int y = location.cache.y + yOff;
        
        drawTextPosColour(x, y, colourWindowDecorationText, string);
    }
    
    void ScalableTheme::drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) {
        // Draw background, (i.e: Normal button).
        drawButton(location);
        
        rascUI::State state = location.getState();
        
        // Figure out the x and y off.
        int xOff, yOff;
        getTextOff(&xOff, &yOff, state, location.xScale);
        
        // Draw the text (offset it a bit).
        drawTextPosState(location.cache.x + xOff + (4.0f * location.xScale), location.cache.y + yOff, state, "X", 1);
    }
    
    void ScalableTheme::getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) {
        int intXOff, intYOff;
        getTextOff(&intXOff, &intYOff, state, 1.0f);
        offX = (GLfloat)intXOff;
        offY = (GLfloat)intYOff;
    }

}

// --------------------------------------------------------------------

const char * getApiVersion(void) {
    return "theme-rascUI-0.7";
}

rascUI::Theme * createTheme(void) {
    return new xcbScalableTheme::ScalableTheme();
}

void deleteTheme(rascUI::Theme * theme) {
    delete theme;
}

#endif
