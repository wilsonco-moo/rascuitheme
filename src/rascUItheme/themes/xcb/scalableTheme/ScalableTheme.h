/*
 * ScalableTheme.h
 *
 *  Created on: 31 May 2020
 *      Author: wilson
 */

#ifndef RASCUITHEME_THEMES_XCB_SCALABLETHEME_SCALABLETHEME_H_
#define RASCUITHEME_THEMES_XCB_SCALABLETHEME_SCALABLETHEME_H_

#include <rascUIxcb/theme/XcbThemeBase.h>
#include <rascUIxcb/util/Colour.h>
#include <rascUI/util/State.h>
#include <xcb/xcb_cursor.h>
#include <X11/Xft/Xft.h>
#include <xcb/xproto.h>
#include <cstdint>
#include <array>

namespace xcbScalableTheme {

    /**
     * ScalableTheme (for xcb) is intended as a nicer and more complete theme
     * (compared to BasicTheme). ScalableTheme uses vector fonts, rather than
     * the X core fonts that BasicTheme uses. This means it can scale nicely to
     * any UI scale. ScalableTheme also uses more complex (and nicer) drawing
     * for buttons and panels, making it much nicer to look at.
     * 
     * Note however that ScalableTheme leaves allocated memory at the end - this
     * is evident when running in valgrind. Unfortunately, this does not seem
     * possible to solve, as the memory is leaked by libraries which
     * ScalableTheme uses: Xft and fontconfig. It seems that Xft does not tidy
     * up after itself properly, leaving allocated font cache stuff in the
     * fontconfig library, even after freeing all Xft fonts. Also, neither Xft
     * nor fontconfig seem to provide a functional way to clean this up.
     * 
     * tldr: If running in valgrind (for debug purposes), use BasicTheme
     *       instead: this doesn't leak memory.
     * 
     * To compile ScalableTheme, libxft-dev and libfreetype6-dev is required.
     * 
     * Note that this requires linking to Xft, AND requires an
     * additional include directory: "/usr/include/freetype2".
     * This can be accomplished with the following compile flag:
     * "-I/usr/include/freetype2".
     * See: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=735764
     *      http://web.archive.org/web/20200601201047/https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=735764
     *
     * Since there is so little information about xft, and I have not managed
     * to find any decent or complete API documentation for it, this theme
     * was assembled using examples from the following:
     * 
     * Fairly good (but incomplete) documentation:
     *          https://keithp.com/~keithp/talks/xtc2001/paper/xft.html
     * Archive: http://web.archive.org/web/20170302151845/https://keithp.com/~keithp/talks/xtc2001/paper/xft.html
     * 
     * A faily complete usage example:
     *          https://github.com/awaw/status/blob/master/status.c
     * Archive: http://web.archive.org/web/20200531170058/https://github.com/awaw/status/blob/master/status.c
     * 
     * Warning: When using this theme, valgrind will report leaked memory. This
     *          comes from the fontconfig library, and is a result of Xft
     *          not cleaning up after itself properly. If you want to use
     *          valgrind, don't use this theme: use the basic theme.
     */
    class ScalableTheme : public rascUIxcb::XcbThemeBase {
    private:
        // Stores the three colours required to draw a button. Also stores the
        // field "inverted", which controls which way around thick/thin borders
        // are drawn.
        class ButtonCol {
        public:
            uint32_t topCol, middleCol, bottomCol;
            bool inverted;
        };
    
        // Colours
        #define SCALABLE_THEME_COLOURS                        \
            X(colourButtonOutlineTop,               0x636363) \
            X(colourButtonOutlineBottom,            0x333333) \
            X(colourButtonSelectedOutlineTop,       0x817373) \
            X(colourButtonSelectedOutlineBottom,    0x262626) \
            X(colourBack,                           0xdddddd) \
            X(colourFrontOutlineTop,                0x8d8d8d) \
            X(colourFrontOutlineBottom,             0x6b6b6b) \
            X(colourFront,                          0xc7c7c7) \
            X(colourText,                           0x000000) \
            X(colourTextInactive,                   0x666666) \
            X(colourDefaultBg,                      0x63ceed) \
            X(colourTextEntryNormal,                0xefefef) \
            X(colourTextEntryMouseOver,             0xeaeaff) \
            X(colourTextEntryMousePress,            0xf3f3ff) \
            X(colourTextEntrySelected,              0xffffff) \
            X(colourTextEntryInactive,              0xc7c7c7) \
            X(colourButtonNormal,                   0x999999) \
            X(colourButtonMouseOver,                0x8585a5) \
            X(colourButtonMousePress,               0x737399) \
            X(colourButtonSelected,                 0x8585a6) \
            X(colourButtonInactive,                 0xc7c7c7) \
            X(colourWindowDecorationTop,            0x5d648d) \
            X(colourWindowDecorationBottom,         0x3b426a) \
            X(colourWindowDecorationFill,           0x99a5ff) \
            X(colourWindowDecorationFrontPanelFill, 0x6179ff) \
            X(colourWindowDecorationText,           0xffffff)
        
        // Color instances.
        #define X(name, code) rascUIxcb::Colour name;
        SCALABLE_THEME_COLOURS
        #undef X
        
        // Fonts
        XftFont * xftFont;
        XftDraw * xftDraw;
        GLfloat lastFontScale;
        
        // Cursor context.
        xcb_cursor_context_t * cursorContext;
        
        // Cursors, format: rascUI enum name, xcb_cursor.h name.
        #define SCALABLE_THEME_CURSORS \
            X(arrow, "left_ptr") \
            X(hand, "hand2") \
            X(text, "xterm") \
            X(move, "fleur") \
            X(moveHorizontally, "sb_h_double_arrow") \
            X(moveVertically, "sb_v_double_arrow") \
            X(moveRight, "right_side") \
            X(moveUpRight, "top_right_corner") \
            X(moveUp, "top_side") \
            X(moveUpLeft, "top_left_corner") \
            X(moveLeft, "left_side") \
            X(moveDownLeft, "bottom_left_corner") \
            X(moveDown, "bottom_side") \
            X(moveDownRight, "bottom_right_corner")
        
        // Build an internal enum of cursors, so we can count them and map
        // from rascUI enum to our cursor array index.
        #define X(name, ...) name,
        class CursorIndices { public: enum { SCALABLE_THEME_CURSORS COUNT }; };
        #undef X
        
        // Array of xcb cursor ids.
        std::array<xcb_cursor_t, CursorIndices::COUNT> cursors;
        
        // Each time we draw something, we keep a record of it's fill colour.
        // This is used as the background colour for text drawn over the top of it.
        uint32_t lastFillColour;

    public:
        ScalableTheme(void);
        virtual ~ScalableTheme(void);

    private:
        // Deletes the xft font - this should be called during destroy.
        void deleteXftFont(void);
        // Re-creates the xft font using a new UI scale, if required.
        void updateXftFont(GLfloat uiScale);
    
        // Gets the top, middle and bottom button colour appropriate for the state.
        void getButtonColour(ButtonCol * col, rascUI::State state);
        
        // Draws a rectangle with borders, at the specified position.
        void drawBorderedRect(const xcb_rectangle_t & rectangle, const ButtonCol & buttonCol, GLfloat uiScale);
        
        // Gets the text offset with the specified UI scale.
        void getTextOff(int * x, int * y, rascUI::State state, GLfloat uiScale);
        
        // Draws text at the specified coordinates, with a colour appropriate for the
        // specified state.
        void drawTextPosState(int x, int y, rascUI::State state, const std::string & text);
        void drawTextPosState(int x, int y, rascUI::State state, const char * text, size_t textLength);
        
        // Draws text at the specified coordinates, with specified colour.
        void drawTextPosColour(int x, int y, rascUIxcb::Colour & colour, const std::string & text) const;
        
        // Gets the true width of the text, (trimmed to the specified length).
        // This is done by getting its extents, with an additional character added, 
        // and subtracting the width of that character. This is necessary because the extents
        // function returns smaller values when the last character is thinner (like a space).
        // This behaviour is not useful for drawing cursors.
        int getTextTrueWidth(const std::string & text, size_t length);
        
    protected:
        virtual void init(void * userData) override;
    public:
        virtual void beforeDraw(void * userData) override;
        virtual void afterDraw(const rascUI::Rectangle & drawnArea) override;
    protected:
        virtual void destroy(void) override;
    public:
        virtual void onChangeViewArea(const rascUI::Rectangle & view) override;
        virtual void drawBackPanel(const rascUI::Location & location) override;
        virtual void drawFrontPanel(const rascUI::Location & location) override;
        virtual void drawText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawButton(const rascUI::Location & location) override;
        virtual void drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) override;
        virtual void drawScrollBarBackground(const rascUI::Location & location) override;
        virtual void drawScrollContentsBackground(const rascUI::Location & location) override;
        virtual void drawScrollPuck(const rascUI::Location & location, bool vertical) override;
        virtual void drawScrollUpButton(const rascUI::Location & location, bool vertical) override;
        virtual void drawScrollDownButton(const rascUI::Location & location, bool vertical) override;
        virtual void drawFadePanel(const rascUI::Location & location) override;
        virtual void drawCheckboxButton(const rascUI::Location & location) override;
        virtual void drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) override;
        virtual void drawTooltipBackground(const rascUI::Location & location) override;
        virtual void drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) override;
        virtual void drawWindowDecorationBackPanel(const rascUI::Location & location) override;
        virtual void drawWindowDecorationFrontPanel(const rascUI::Location & location) override;
        virtual void drawWindowDecorationText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) override;
        virtual void getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) override;
    };
}

// --------------------------------------------------------------------

#ifdef _WIN32
    #define APIFUNC extern "C" __declspec(dllexport)
#else
    #define APIFUNC extern "C"
#endif

APIFUNC const char * getApiVersion(void);

APIFUNC rascUI::Theme * createTheme(void);

APIFUNC void deleteTheme(rascUI::Theme * theme);

#endif
