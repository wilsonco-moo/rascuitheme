/*
 * ScalableTheme.h
 *
 *  Created on: 31 May 2020
 *      Author: wilson
 */

#ifndef RASCUITHEME_THEMES_WIN_SCALABLETHEME_SCALABLETHEME_H_
#define RASCUITHEME_THEMES_WIN_SCALABLETHEME_SCALABLETHEME_H_

#include <rascUIwin/theme/WinThemeBase.h>
#include <rascUI/util/State.h>
#include <cstdint>

namespace winScalableTheme {

    /**
     * ScalableTheme (for Windows API) is intended as a nicer and more complete
     * theme (compared to BasicTheme). ScalableTheme uses vector fonts, rather
     * than the basic raster fonts that BasicTheme uses. This means it can scale
     * nicely to any UI scale. ScalableTheme also uses more complex (and nicer)
     * drawing for buttons and panels, making it much nicer to look at.
     */
    class ScalableTheme : public rascUIwin::WinThemeBase {
    private:
        /**
         * A simple class storing a pen and a brush. This is used internally,
         * within the theme, for storing colours.
         * The contents of the pen and brush fields are only defined between
         * a call to create and a call to destroy.
         */
        class PenAndBrush {
        public:
            // Pen (outline colour).
            HPEN pen;
            // Brush (fill colour).
            HBRUSH brush;
            
            // Creates our pen and brush, using the provided colour. This
            // should be called for all colours, during the theme's init method.
            void create(COLORREF colour);
            
            // Destroys our pen and brush. This should be called for all
            // colours, during the theme's destroy method.
            void destroy(void);
        };
        
        /**
         * Stores the three colours required to draw a button. Also stores the
         * field "inverted", which controls which way around thick/thin borders
         * are drawn.
         */
        class ButtonCol {
        public:
            PenAndBrush * topCol,
                        * middleCol,
                        * bottomCol;
            bool inverted;
        };
        
        // ---------------------------------------------------------------------
        
        // Define the colours whose codes are needed alone.
        #define COLOURCODE_TEXT          0x000000
        #define COLOURCODE_TEXT_INACTIVE 0x666666
        
        // Define colours. A PenAndBrush is created from each of these.
        #define SCALABLE_THEME_COLOURS                                        \
            X(colourButtonOutlineTop,               0x636363                ) \
            X(colourButtonOutlineBottom,            0x333333                ) \
            X(colourButtonSelectedOutlineTop,       0x817373                ) \
            X(colourButtonSelectedOutlineBottom,    0x262626                ) \
            X(colourBack,                           0xdddddd                ) \
            X(colourFrontOutlineTop,                0x8d8d8d                ) \
            X(colourFrontOutlineBottom,             0x6b6b6b                ) \
            X(colourFront,                          0xc7c7c7                ) \
            X(colourText,                           COLOURCODE_TEXT         ) \
            X(colourTextInactive,                   COLOURCODE_TEXT_INACTIVE) \
            X(colourDefaultBg,                      0x63ceed                ) \
            X(colourTextEntryNormal,                0xefefef                ) \
            X(colourTextEntryMouseOver,             0xeaeaff                ) \
            X(colourTextEntryMousePress,            0xf3f3ff                ) \
            X(colourTextEntrySelected,              0xffffff                ) \
            X(colourTextEntryInactive,              0xc7c7c7                ) \
            X(colourButtonNormal,                   0x999999                ) \
            X(colourButtonMouseOver,                0x8585a5                ) \
            X(colourButtonMousePress,               0x737399                ) \
            X(colourButtonSelected,                 0x8585a6                ) \
            X(colourButtonInactive,                 0xc7c7c7                )
        
        // Declare colours.
        #define X(name, code) PenAndBrush name;
        SCALABLE_THEME_COLOURS
        #undef X
        
        // The font which we are using.
        HFONT windowsFont;
        GLfloat lastFontScale;
        
    public:
        ScalableTheme(void);
        virtual ~ScalableTheme(void);
        
    private:
        // Deletes the windows font - this should be called during destroy.
        void deleteWindowsFont(void);
        
        // Re-creates the windows font using a new UI scale, if required.
        void updateWindowsFont(GLfloat uiScale);
    
        // Sets our current display context to use our current font, IF our
        // font has been created. Also sets the appropriate text colour and
        // background. This should be called each time we potentially get a
        // new display context - such as during beforeDraw and onChangeViewArea.
        void setTextProperties(void);
        
        // Gets the top, middle and bottom button colour appropriate for the state.
        void getButtonColour(ButtonCol * col, rascUI::State state);
        
        // Draws a rectangle with borders, at the specified position.
        void drawBorderedRect(const RECT & rectangle, const ButtonCol & buttonCol, GLfloat uiScale);
        
        // Gets the text offset with the specified UI scale.
        void getTextOff(int * x, int * y, rascUI::State state, GLfloat uiScale);
        
        // Draws text at the specified coordinates, with a colour appropriate for the
        // specified state.
        void drawTextPosState(int x, int y, rascUI::State state, const std::string & text);
        
        // Gets the width of the text, trimmed to the specified length.
        int getTextTrueWidth(const std::string & text, size_t length);
        
    protected:
        virtual void init(void * userData) override;
    public:
        virtual void beforeDraw(void * userData) override;
    protected:
        virtual void destroy(void) override;
    public:
        virtual void onChangeViewArea(const rascUI::Rectangle & view) override;
        virtual void drawBackPanel(const rascUI::Location & location) override;
        virtual void drawFrontPanel(const rascUI::Location & location) override;
        virtual void drawText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawButton(const rascUI::Location & location) override;
        virtual void drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) override;
        virtual void drawScrollBarBackground(const rascUI::Location & location) override;
        virtual void drawScrollContentsBackground(const rascUI::Location & location) override;
        virtual void drawScrollPuck(const rascUI::Location & location, bool vertical) override;
        virtual void drawScrollUpButton(const rascUI::Location & location, bool vertical) override;
        virtual void drawScrollDownButton(const rascUI::Location & location, bool vertical) override;
        virtual void drawFadePanel(const rascUI::Location & location) override;
        virtual void drawCheckboxButton(const rascUI::Location & location) override;
        virtual void drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) override;
        virtual void drawTooltipBackground(const rascUI::Location & location) override;
        virtual void drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) override;
        virtual void drawWindowDecorationBackPanel(const rascUI::Location & location) override;
        virtual void drawWindowDecorationFrontPanel(const rascUI::Location & location) override;
        virtual void drawWindowDecorationText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) override;
        virtual void getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) override;
    };
}

// --------------------------------------------------------------------

#ifdef _WIN32
    #define APIFUNC extern "C" __declspec(dllexport)
#else
    #define APIFUNC extern "C"
#endif

APIFUNC const char * getApiVersion(void);

APIFUNC rascUI::Theme * createTheme(void);

APIFUNC void deleteTheme(rascUI::Theme * theme);

#endif
