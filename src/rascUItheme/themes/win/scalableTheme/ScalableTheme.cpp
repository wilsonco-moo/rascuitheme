/*
 * ScalableTheme.cpp
 *
 *  Created on: 31 May 2020
 *      Author: wilson
 */

// Force enable the theme if requested.
#ifdef FORCE_ENABLE_WIN_SCALABLE_THEME
    #ifndef ENABLE_RASCUI_THEME
        #define ENABLE_RASCUI_THEME
    #endif
#endif

// Only compile if this theme is enabled.
#ifdef ENABLE_RASCUI_THEME

#include "ScalableTheme.h"

#include <rascUIwin/platform/Context.h>
#include <rascUIwin/platform/Window.h>
#include <rascUI/util/Location.h>
#include <windows.h>
#include <cstddef>
#include <string>

// Draws a rectangle which fills the location, using the specified outline pen
// and fill brush.
#define DRAW_RASCUI_RECT(outlinePen, fillBrush, rect)       \
    SelectObject(getDisplayContext(), (outlinePen));        \
    SelectObject(getDisplayContext(), (fillBrush));         \
    Rectangle(getDisplayContext(),                          \
              (int)(rect).x,                                \
              (int)(rect).y,                                \
              (int)((rect).x + (rect).width),               \
              (int)((rect).y + (rect).height));

// Creates a windows RECT (called rect), from the specified rascUI rectangle.
#define GET_WINDOWS_RECT(oldRect)               \
    RECT rect = {                               \
        (LONG)(oldRect).x,                      \
        (LONG)(oldRect).y,                      \
        (LONG)((oldRect).x + (oldRect).width),  \
        (LONG)((oldRect).y + (oldRect).height)  \
    };
    
// Converts a colour code (in the format 0xrrggbb) to a windows COLORREF, by
// passing it through the windows RGB macro.
#define COLCODE_TO_REF(code) \
    RGB((((code) >> 16) & 0xff), (((code) >> 8) & 0xff), ((code) & 0xff))


namespace winScalableTheme {
    
    void ScalableTheme::PenAndBrush::create(COLORREF colour) {
        pen = CreatePen(PS_SOLID, 0, colour);
        brush = CreateSolidBrush(colour);
    }
    void ScalableTheme::PenAndBrush::destroy(void) {
        DeleteObject(pen);
        DeleteObject(brush);
    }
    
    // ---------------------------------------------------------------------
    
    ScalableTheme::ScalableTheme(void) :
        rascUIwin::WinThemeBase(),
        windowsFont(NULL),
        lastFontScale(0.0f) {
            
        normalComponentWidth = 18.0f;    // Normal component width   (universal button width, as of 0.42 only for scrollbar puck)
        normalComponentHeight = 24.0f;   // Normal component height  (universal button height value)
        uiBorder = 4.0f;                 // UI border
        customTextCharWidth = 7.0f;      // This should match the font we are using.
        customTitleTextCharWidth = 7.0f; // Currently we don't implement title text anyway.
        
        windowDecorationNormalComponentWidth = normalComponentWidth;
        windowDecorationNormalComponentHeight = normalComponentHeight;
        windowDecorationUiBorder = uiBorder;
        
        // Set custom drawing colours.
        customColourMainText        = NULL;
        customColourSecondaryText   = NULL;
        customColourInactiveText    = NULL;
        customColourBackplaneMain   = NULL;
        customColourBackplaneLight  = NULL;
        customColourBackplaneDark   = NULL;
    }

    ScalableTheme::~ScalableTheme(void) {
        // Make sure destroy gets called, if init has been called, but
        // destroy has not.
        callDestroy();
    }
    
    void ScalableTheme::deleteWindowsFont(void) {
        // Do nothing if we don't already have a font anyway.
        if (windowsFont == NULL) return;
        
        // Destroy the font and set it to NULL, so we know that we don't have a font any more.
        DeleteObject(windowsFont);
        windowsFont = NULL;
    }
    
    void ScalableTheme::updateWindowsFont(GLfloat uiScale) {
        // Do nothing if the scale hasn't changed, and there is already a font.
        if (uiScale == lastFontScale && windowsFont != NULL) return;
        lastFontScale = uiScale;
        
        // Delete the old font.
        deleteWindowsFont();
        
        // Allocate the new font.
        windowsFont = CreateFont(
            (int)(17.0f * uiScale),    // Character height (font size in pixels). Make sure this scales along with the UI scale.
            0,                         // Average font character width (zero means pick closest)
            0,                         // Angle, in tenths of degrees, between "escapement vector" and x axis: Part of font's rotation (zero means don't rotate).
            0,                         // Angle, in tenths of degrees, between "baseline" and x axis: Part of font's rotation (zero means don't rotate).
            FW_DONTCARE,               // Font weight (0-1000). Sensible values are FW_DONTCARE (default) or FW_NORMAL.
            FALSE,                     // Whether the font should be italic (FALSE is not italic).
            FALSE,                     // Whether the font should be underlined (FALSE is no underline).
            FALSE,                     // Whether to enable strikeout (FALSE is no strikeout).
            DEFAULT_CHARSET,           // The character set for the font - use default here.
            OUT_OUTLINE_PRECIS,        // The output precision: specifies how closely the font should match this description. OUT_OUTLINE_PRECIS specifies that it must pick a "TrueType or other outline-based font".
            CLIP_DEFAULT_PRECIS,       // Behaviour of clipping when characters are partially outside clipping region. CLIP_DEFAULT_PRECIS is default clipping behaviour.
            DEFAULT_QUALITY,           // Output quality (antialiasing behaviour). DEFAULT_QUALITY means use system default.
            VARIABLE_PITCH | FF_SWISS, // Font pitch and font family. These are used to find a closest font, if the exact one cannot be found.
            "Arial"                    // Font name. On windows the Arial font pretty much always exists. If this is not found, a similar font will be used instead.
        );
    }
    
    void ScalableTheme::setTextProperties(void) {
        // Do nothing if we have no font (yet).
        if (windowsFont == NULL) return;
        // Set the text background to transparent, set the text alignment and
        // tell the display context to use the font.
        SetBkMode(getDisplayContext(), TRANSPARENT);
        SetTextAlign(getDisplayContext(), TA_TOP);
        SelectObject(getDisplayContext(), windowsFont);
    }
    
    void ScalableTheme::getButtonColour(ButtonCol * col, rascUI::State state) {
        switch(state) {
        case rascUI::State::normal:
            *col = {&colourButtonOutlineTop,
                    &colourButtonNormal,
                    &colourButtonOutlineBottom,
                    false};
            break;
        case rascUI::State::mouseOver:
            *col = {&colourButtonSelectedOutlineTop,
                    &colourButtonMouseOver,
                    &colourButtonSelectedOutlineBottom,
                    false};
            break;
        case rascUI::State::mousePress:
            *col = {&colourButtonSelectedOutlineBottom,
                    &colourButtonMousePress,
                    &colourButtonSelectedOutlineTop,
                    true};
            break;
        case rascUI::State::selected:
            *col = {&colourButtonSelectedOutlineBottom,
                    &colourButtonSelected,
                    &colourButtonSelectedOutlineTop,
                    true};
            break;
        default: // Inactive
            *col = {&colourButtonOutlineTop,
                    &colourButtonInactive,
                    &colourButtonOutlineBottom,
                    false};
            break;
        }
    }
    
    void ScalableTheme::drawBorderedRect(const RECT & rectangle, const ButtonCol & buttonCol, GLfloat uiScale) {
        int topBorder, bottomBorder;
        if (buttonCol.inverted) {
            topBorder = (int)(uiScale * 2.0f);
            bottomBorder = (int)(uiScale * 1.0f);
        } else {
            topBorder = (int)(uiScale * 1.0f);
            bottomBorder = (int)(uiScale * 2.0f);
        }
        int leftPlusBord    = rectangle.left   + topBorder,
            rightMinusBord  = rectangle.right  - bottomBorder,
            topPlusBord     = rectangle.top    + topBorder,
            bottomMinusBord = rectangle.bottom - bottomBorder;
        
        SelectObject(getDisplayContext(), buttonCol.topCol->pen);
        SelectObject(getDisplayContext(), buttonCol.topCol->brush);
        Rectangle(getDisplayContext(), rectangle.left, rectangle.top, rightMinusBord, topPlusBord);
        Rectangle(getDisplayContext(), rectangle.left, topPlusBord, leftPlusBord, rectangle.bottom);
        
        SelectObject(getDisplayContext(), buttonCol.bottomCol->pen);
        SelectObject(getDisplayContext(), buttonCol.bottomCol->brush);
        Rectangle(getDisplayContext(), leftPlusBord, bottomMinusBord, rectangle.right, rectangle.bottom);
        Rectangle(getDisplayContext(), rightMinusBord, rectangle.top, rectangle.right, bottomMinusBord);
        
        SelectObject(getDisplayContext(), buttonCol.middleCol->pen);
        SelectObject(getDisplayContext(), buttonCol.middleCol->brush);
        Rectangle(getDisplayContext(), leftPlusBord, topPlusBord, rightMinusBord, bottomMinusBord);
    }
    
    void ScalableTheme::getTextOff(int * x, int * y, rascUI::State state, GLfloat uiScale) {
        if (state == rascUI::State::mousePress || state == rascUI::State::selected) {
            *x = (int)(5.0f * uiScale);
            *y = (int)(3.0f * uiScale);
        } else {
            *x = (int)(4.0f * uiScale);
            *y = (int)(2.0f * uiScale);
        }
    }
    
    void ScalableTheme::drawTextPosState(int x, int y, rascUI::State state, const std::string & text) {
        // Set text colour depending on state.
        if (state == rascUI::State::inactive) {
            SetTextColor(getDisplayContext(), COLCODE_TO_REF(COLOURCODE_TEXT_INACTIVE));
        } else {
            SetTextColor(getDisplayContext(), COLCODE_TO_REF(COLOURCODE_TEXT));
        }
        
        // Draw the text.
        TextOut(getDisplayContext(), x, y, (text).c_str(), (text).size());
    }
    
    int ScalableTheme::getTextTrueWidth(const std::string & text, size_t length) {
        SIZE size;
        GetTextExtentPoint32A(getDisplayContext(), text.c_str(), length, &size);
        return size.cx;
    }

    void ScalableTheme::init(void * userData) {
        WinThemeBase::init(userData);
        
        // Initialise colours.
        #define X(name, code) name.create(COLCODE_TO_REF(code));
        SCALABLE_THEME_COLOURS
        #undef X
        
        // Set the context's background pen and brush.
        rascUIwin::Context * context = getContext();
        context->winThemeSetBackground(colourDefaultBg.pen, colourDefaultBg.brush);
        
        // If the context has an x scale, update the font on initialisation.
        if (context->defaultXScalePtr != NULL) {
            updateWindowsFont(*context->defaultXScalePtr);
        }
    }
    
    void ScalableTheme::beforeDraw(void * userData) {
        WinThemeBase::beforeDraw(userData);
        
        // Set text properties here, since we get a new
        // display context each time we begin drawing.
        setTextProperties();
    }
    
    void ScalableTheme::destroy(void) {
        // Reset the context's background pen and brush.
        getContext()->winThemeSetBackground(NULL, NULL);
        
        // Free the font.
        deleteWindowsFont();
        
        // Delete colours.
        #define X(name, code) name.destroy();
        SCALABLE_THEME_COLOURS
        #undef X
        
        WinThemeBase::destroy();
    }
    
    void ScalableTheme::onChangeViewArea(const rascUI::Rectangle & view) {
        WinThemeBase::onChangeViewArea(view);
        
        // Update the windows font using the window's UI scale.
        // Do this here since we get an onChangeViewArea call each time the ui scale changes,
        // and UI scale is not updated until this call.
        updateWindowsFont(getWindow()->getXScale());
        
        // Set the text properties, since the display context will likely have changed.
        setTextProperties();
        
        // If double buffering is enabled, after changes in view area we have a new display context
        // AND a new bitmap. So fill the new bitmap with the background colour.
        if (getWindow()->winIsDoubleBufferEnabled()) {
            DRAW_RASCUI_RECT(colourDefaultBg.pen, colourDefaultBg.brush, view)
        }
    }

    void ScalableTheme::drawBackPanel(const rascUI::Location & location) {
        DRAW_RASCUI_RECT(colourBack.pen, colourBack.brush, location.cache)
    }

    void ScalableTheme::drawFrontPanel(const rascUI::Location & location) {
        GET_WINDOWS_RECT(location.cache)
        ButtonCol buttonCol = { &colourFrontOutlineTop, &colourFront, &colourFrontOutlineBottom, false };
        drawBorderedRect(rect, buttonCol, location.xScale);
    }

    void ScalableTheme::drawText(const rascUI::Location & location, const std::string & string) {
        rascUI::State state = location.getState();
        
        // Figure out the x and y off.
        int xOff, yOff;
        getTextOff(&xOff, &yOff, state, location.xScale);
        
        // Draw the text.
        drawTextPosState(location.cache.x + xOff, location.cache.y + yOff, state, string);
    }

    void ScalableTheme::drawButton(const rascUI::Location & location) {
        GET_WINDOWS_RECT(location.cache)
        ButtonCol buttonCol;
        getButtonColour(&buttonCol, location.getState());
        drawBorderedRect(rect, buttonCol, location.xScale);
    }

    void ScalableTheme::drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) {
        GET_WINDOWS_RECT(location.cache)
        ButtonCol buttonCol;
        rascUI::State state = location.getState();
        int offset3 = (int)(3.0f * location.xScale),
            offset2 = (int)(2.0f * location.xScale);
        
        // Draw outer rectangle.
        getButtonColour(&buttonCol, state);
        drawBorderedRect(rect, buttonCol, location.xScale);
        
        // Set new middle colour to draw inner rectangle.
        switch(state) {
        case rascUI::State::normal:
            buttonCol.middleCol = &colourTextEntryNormal;
            break;
        case rascUI::State::mouseOver:
            buttonCol.middleCol = &colourTextEntryMouseOver;
            break;
        case rascUI::State::mousePress:
            buttonCol.middleCol = &colourTextEntryMousePress;
            break;
        case rascUI::State::selected:
            buttonCol.middleCol = &colourTextEntrySelected;
            break;
        default: // Inactive
            buttonCol.middleCol = &colourTextEntryInactive;
            break;
        }
        
        // Shrink rectangle and draw inner rectangle.
        if (buttonCol.inverted) {
            rect.left += offset3; rect.top += offset3; 
            rect.right -= offset2; rect.bottom -= offset2;
        } else {
            rect.left += offset2; rect.top += offset2;
            rect.right -= offset3; rect.bottom -= offset3;
        }
        std::swap(buttonCol.topCol, buttonCol.bottomCol);
        buttonCol.inverted = !buttonCol.inverted;
        drawBorderedRect(rect, buttonCol, location.xScale);
        
        // Figure out offsets, add a small x offset, and draw text.
        int xOff, yOff;
        getTextOff(&xOff, &yOff, state, location.xScale);
        xOff += (int)(location.xScale * 2.0f);
        drawTextPosState(location.cache.x + xOff, location.cache.y + yOff, state, string);
        
        if (drawCursor) {
            // Get the width of the text.
            int textWidth = getTextTrueWidth(string, std::min(string.length(), cursorPos));
            
            // Offset the rectangle by the width, and make it cursor shaped.
            rect.left += (textWidth + offset3);
            rect.top += offset3;
            rect.right = rect.left + offset2;
            rect.bottom -= offset3;
            
            // Draw a rectangle for the cursor.
            SelectObject(getDisplayContext(), colourText.pen);
            SelectObject(getDisplayContext(), colourText.brush);
            Rectangle(getDisplayContext(), rect.left, rect.top, rect.right, rect.bottom);
        }
    }

    void ScalableTheme::drawScrollBarBackground(const rascUI::Location & location) {
        // TODO stipple
        drawFrontPanel(location);
    }

    void ScalableTheme::drawScrollContentsBackground(const rascUI::Location & location) {
        drawBackPanel(location);
    }

    void ScalableTheme::drawScrollPuck(const rascUI::Location & location, bool vertical) {
        drawButton(location);
    }

    void ScalableTheme::drawScrollUpButton(const rascUI::Location & location, bool vertical) {
        drawButton(location);
    }

    void ScalableTheme::drawScrollDownButton(const rascUI::Location & location, bool vertical) {
        drawButton(location);
    }

    void ScalableTheme::drawFadePanel(const rascUI::Location & location) {
    }
    
    void ScalableTheme::drawCheckboxButton(const rascUI::Location & location) {
    }
    
    void ScalableTheme::drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) {
    }
    
    void ScalableTheme::drawTooltipBackground(const rascUI::Location & location) {
    }
    
    void ScalableTheme::drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) {
    }
    
    void ScalableTheme::drawWindowDecorationBackPanel(const rascUI::Location & location) {
        drawBackPanel(location);
    }
    
    void ScalableTheme::drawWindowDecorationFrontPanel(const rascUI::Location & location) {
        drawFrontPanel(location);
    }
    
    void ScalableTheme::drawWindowDecorationText(const rascUI::Location & location, const std::string & string) {
        drawText(location, string);
    }
    
    void ScalableTheme::drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) {
        drawButton(location);
    }
    
    void ScalableTheme::getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) {
        int intOffX, intOffY;
        getTextOff(&intOffX, &intOffY, state, 1.0f);
        offX = (GLfloat)intOffX;
        offY = (GLfloat)intOffY;
    }
}

// --------------------------------------------------------------------

const char * getApiVersion(void) {
    return "theme-rascUI-0.7";
}

rascUI::Theme * createTheme(void) {
    return new winScalableTheme::ScalableTheme();
}

void deleteTheme(rascUI::Theme * theme) {
    delete theme;
}

#endif
