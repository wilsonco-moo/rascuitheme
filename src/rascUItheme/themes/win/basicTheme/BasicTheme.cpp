/*
 * BasicTheme.cpp
 *
 *  Created on: 19 May 2020
 *      Author: wilson
 */

// Force enable the theme if requested.
#ifdef FORCE_ENABLE_WIN_BASIC_THEME
    #ifndef ENABLE_RASCUI_THEME
        #define ENABLE_RASCUI_THEME
    #endif
#endif

// Only compile if this theme is enabled.
#ifdef ENABLE_RASCUI_THEME

#include "BasicTheme.h"

#include <rascUIwin/platform/Context.h>
#include <rascUIwin/platform/Window.h>
#include <rascUI/util/Location.h>
#include <windows.h>
#include <cstddef>
#include <string>

// Draws a rectangle which fills the location, using the specified outline pen
// and fill brush.
#define DRAW_RASCUI_RECT(outlinePen, fillBrush, rect)       \
    SelectObject(getDisplayContext(), (outlinePen));        \
    SelectObject(getDisplayContext(), (fillBrush));         \
    Rectangle(getDisplayContext(),                          \
              (int)(rect).x,                                \
              (int)(rect).y,                                \
              (int)((rect).x + (rect).width),               \
              (int)((rect).y + (rect).height));

// Draws text relative to the location, using appropriate xOff, yOff and colour.
#define DRAW_TEXT_OFF(str, loc) {                             \
    int xOff, yOff;                                           \
    getTextOff(&xOff, &yOff, (loc).getState());               \
    TextOut(getDisplayContext(),                              \
            (int)((loc).cache.x + (GLfloat)xOff),             \
            (int)((loc).cache.y + (GLfloat)yOff),             \
            (str).c_str(), (str).size());                     \
}

// The colour to draw all text.
#define TEXT_COLOUR_RGB RGB(0, 0, 0)

namespace winBasicTheme {

    BasicTheme::BasicTheme(void) :
        rascUIwin::WinThemeBase(),
        fontCharWidth(0) {
            
        normalComponentWidth = 18.0f;    // Normal component width   (universal button width, as of 0.42 only for scrollbar puck)
        normalComponentHeight = 24.0f;   // Normal component height  (universal button height value)
        uiBorder = 4.0f;                 // UI border
        customTextCharWidth = 7.0f;      // This should match the font we are using.
        customTitleTextCharWidth = 7.0f; // Currently we don't implement title text anyway.
        
        windowDecorationNormalComponentWidth = normalComponentWidth;
        windowDecorationNormalComponentHeight = normalComponentHeight;
        windowDecorationUiBorder = uiBorder;
        
        // Set custom drawing colours.
        customColourMainText        = NULL;
        customColourSecondaryText   = NULL;
        customColourInactiveText    = NULL;
        customColourBackplaneMain   = NULL;
        customColourBackplaneLight  = NULL;
        customColourBackplaneDark   = NULL;
    }

    BasicTheme::~BasicTheme(void) {
        // Make sure destroy gets called, if init has been called, but
        // destroy has not.
        callDestroy();
    }
    
    HBRUSH BasicTheme::getButtonFillColour(rascUI::State state) {
        switch(state) {
            case rascUI::State::normal:     return colourButtonNormal;
            case rascUI::State::mouseOver:  return colourButtonMouseOver;
            case rascUI::State::mousePress: return colourButtonMousePress;
            case rascUI::State::selected:   return colourButtonSelected;
            default:                        return colourButtonInactive;
        }
    }
    
    void BasicTheme::getTextOff(int * xOff, int * yOff, rascUI::State state) {
        if (state == rascUI::State::mousePress || state == rascUI::State::selected) {
            *xOff = 5; *yOff = 7;
        } else {
            *xOff = 4; *yOff = 6;
        }
    }

    void BasicTheme::setTextProperties(void) {
        // Set the text properties, and get the character width. This will save us from doing this later.
        SetBkMode(getDisplayContext(), TRANSPARENT);
        SetTextColor(getDisplayContext(), TEXT_COLOUR_RGB);
        SetTextAlign(getDisplayContext(), TA_TOP);
        SelectObject(getDisplayContext(), GetStockObject(SYSTEM_FIXED_FONT));
        TEXTMETRIC textMetrics;
        GetTextMetrics(getDisplayContext(), &textMetrics);
        fontCharWidth = textMetrics.tmMaxCharWidth;
    }

    void BasicTheme::init(void * userData) {
        WinThemeBase::init(userData);
        
        // Initialise pens (outline colours).
        outlineDefault   = CreatePen(PS_SOLID, 0, RGB(0x33, 0x33, 0x33));
        outlineDefaultBg = CreatePen(PS_SOLID, 0, RGB(0x63, 0xce, 0xed));
        outlineText      = CreatePen(PS_SOLID, 0, TEXT_COLOUR_RGB);
        
        // Initialise brushes (fill colours).
        colourBack             = CreateSolidBrush(RGB(0xdd, 0xdd, 0xdd));
        colourFront            = CreateSolidBrush(RGB(0xbb, 0xbb, 0xbb));
        colourText             = CreateSolidBrush(TEXT_COLOUR_RGB);
        colourDefaultBg        = CreateSolidBrush(RGB(0x63, 0xce, 0xed));
        colourTextEntry        = CreateSolidBrush(RGB(0xee, 0xee, 0xee));
        colourScrollBackground = CreateSolidBrush(RGB(0x62, 0x68, 0x06));

        colourButtonNormal     = CreateSolidBrush(RGB(0x99, 0x99, 0x99));
        colourButtonMouseOver  = CreateSolidBrush(RGB(0x77, 0x77, 0x99));
        colourButtonMousePress = CreateSolidBrush(RGB(0x59, 0x59, 0x77));
        colourButtonSelected   = CreateSolidBrush(RGB(0x66, 0x66, 0x88));
        colourButtonInactive   = CreateSolidBrush(RGB(0xff, 0xff, 0xff));
        
        // Set the context's background pen and brush.
        getContext()->winThemeSetBackground(outlineDefaultBg, colourDefaultBg);
    }
    
    void BasicTheme::beforeDraw(void * userData) {
        WinThemeBase::beforeDraw(userData);
        
        // Set text properties here, since we get a new display context each time we begin drawing.
        setTextProperties();
    }
    
    void BasicTheme::destroy(void) {
        // Reset the context's background pen and brush.
        getContext()->winThemeSetBackground(NULL, NULL);
        
        // Delete pens (outline colours).
        DeleteObject(outlineDefault);
        DeleteObject(outlineDefaultBg);
        DeleteObject(outlineText);
        
        // Delete brushes (fill colours).
        DeleteObject(colourBack);
        DeleteObject(colourFront);
        DeleteObject(colourText);
        DeleteObject(colourDefaultBg);
        DeleteObject(colourTextEntry);
        DeleteObject(colourScrollBackground);

        DeleteObject(colourButtonNormal);
        DeleteObject(colourButtonMouseOver);
        DeleteObject(colourButtonMousePress);
        DeleteObject(colourButtonSelected);
        DeleteObject(colourButtonInactive);
        
        WinThemeBase::destroy();
    }
    
    void BasicTheme::onChangeViewArea(const rascUI::Rectangle & view) {
        WinThemeBase::onChangeViewArea(view);
        
        // If double buffering is enabled, after changes in view area we have a new display context
        // AND a new bitmap. So set text properties, and fill the new bitmap with the new background colour.
        if (getWindow()->winIsDoubleBufferEnabled()) {
            setTextProperties();
            DRAW_RASCUI_RECT(outlineDefaultBg, colourDefaultBg, view)
        }
    }

    void BasicTheme::drawBackPanel(const rascUI::Location & location) {
        DRAW_RASCUI_RECT(outlineDefault, colourBack, location.cache)
    }

    void BasicTheme::drawFrontPanel(const rascUI::Location & location) {
        DRAW_RASCUI_RECT(outlineDefault, colourFront, location.cache)
    }

    void BasicTheme::drawText(const rascUI::Location & location, const std::string & string) {
        DRAW_TEXT_OFF(string, location)
    }

    void BasicTheme::drawButton(const rascUI::Location & location) {
        DRAW_RASCUI_RECT(outlineDefault, getButtonFillColour(location.getState()), location.cache)
    }

    void BasicTheme::drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) {
        // Get coordinates.
        int x1 = (int)location.cache.x,
            y1 = (int)location.cache.y,
            x2 = (int)(location.cache.x + location.cache.width),
            y2 = (int)(location.cache.y + location.cache.height);
        // Draw backing rectangle.
        SelectObject(getDisplayContext(), outlineDefault);
        SelectObject(getDisplayContext(), getButtonFillColour(location.getState()));
        Rectangle(getDisplayContext(), x1, y1, x2, y2);
        // Draw front rectangle.
        x1 += 4; y1 += 4; x2 -= 4; y2 -= 4;
        SelectObject(getDisplayContext(), colourTextEntry);
        Rectangle(getDisplayContext(), x1, y1, x2, y2);
        // Draw text.
        int xOff, yOff;
        getTextOff(&xOff, &yOff, location.getState());
        TextOut(getDisplayContext(),
                (int)(location.cache.x + (GLfloat)xOff) + 2,
                (int)(location.cache.y + (GLfloat)yOff),
                string.c_str(), string.size());
        // Draw cursor.
        if (drawCursor) {
            x1 += fontCharWidth * cursorPos + 3;
            y1 += 2; y2 -= 2;
            x2 = x1 + 2;
            SelectObject(getDisplayContext(), outlineText);
            SelectObject(getDisplayContext(), colourText);
            Rectangle(getDisplayContext(), x1, y1, x2, y2);
        }
    }

    void BasicTheme::drawScrollBarBackground(const rascUI::Location & location) {
        drawBackPanel(location);
    }

    void BasicTheme::drawScrollContentsBackground(const rascUI::Location & location) {
        DRAW_RASCUI_RECT(outlineDefault, colourScrollBackground, location.cache)
    }

    void BasicTheme::drawScrollPuck(const rascUI::Location & location, bool vertical) {
        drawButton(location);
    }

    void BasicTheme::drawScrollUpButton(const rascUI::Location & location, bool vertical) {
        drawButton(location);
    }

    void BasicTheme::drawScrollDownButton(const rascUI::Location & location, bool vertical) {
        drawButton(location);
    }

    void BasicTheme::drawFadePanel(const rascUI::Location & location) {
    }
    
    void BasicTheme::drawCheckboxButton(const rascUI::Location & location) {
    }
    
    void BasicTheme::drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) {
    }
    
    void BasicTheme::drawTooltipBackground(const rascUI::Location & location) {
    }
    
    void BasicTheme::drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) {
    }
    
    void BasicTheme::drawWindowDecorationBackPanel(const rascUI::Location & location) {
        drawBackPanel(location);
    }
    
    void BasicTheme::drawWindowDecorationFrontPanel(const rascUI::Location & location) {
        drawFrontPanel(location);
    }
    
    void BasicTheme::drawWindowDecorationText(const rascUI::Location & location, const std::string & string) {
        drawText(location, string);
    }
    
    void BasicTheme::drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) {
        drawButton(location);
    }
    
    void BasicTheme::getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) {
        int intOffX, intOffY;
        getTextOff(&intOffX, &intOffY, state);
        offX = (GLfloat)intOffX;
        offY = (GLfloat)intOffY;
    }
}

// --------------------------------------------------------------------

const char * getApiVersion(void) {
    return "theme-rascUI-0.7";
}

rascUI::Theme * createTheme(void) {
    return new winBasicTheme::BasicTheme();
}

void deleteTheme(rascUI::Theme * theme) {
    delete theme;
}

#endif
