/*
 * BasicTheme.h
 *
 *  Created on: 19 May 2020
 *      Author: wilson
 */

#ifndef RASCUITHEME_THEMES_WIN_BASICTHEME_BASICTHEME_H_
#define RASCUITHEME_THEMES_WIN_BASICTHEME_BASICTHEME_H_

#include <rascUIwin/theme/WinThemeBase.h>
#include <rascUI/util/State.h>
#include <cstdint>

namespace winBasicTheme {

    /**
     * This is a simple implementation of WinThemeBase.
     */
    class BasicTheme : public rascUIwin::WinThemeBase {
    private:
        // Pens (outline colours).
        HPEN outlineDefault, outlineDefaultBg, outlineText;
        
        // Brushes (fill colours).
        HBRUSH colourBack, colourFront, colourText, colourDefaultBg, colourTextEntry, colourScrollBackground,
               colourButtonNormal, colourButtonMouseOver, colourButtonMousePress, colourButtonSelected, colourButtonInactive;
               
        // The current font character width. This is set in beforeDraw, and used for drawing the cursor for text entry boxes.
        // This assumes that all character have the same width, i.e: that we are using a monospaced font.
        // Who needs proportionally spaced fonts anyway!?
        int fontCharWidth;
    public:
        BasicTheme(void);
        virtual ~BasicTheme(void);
        
    private:
        HBRUSH getButtonFillColour(rascUI::State state);
        void getTextOff(int * xOff, int * yOff, rascUI::State state);
        void setTextProperties(void);
        
    protected:
        virtual void init(void * userData) override;
    public:
        virtual void beforeDraw(void * userData) override;
    protected:
        virtual void destroy(void) override;
    public:
        virtual void onChangeViewArea(const rascUI::Rectangle & view) override;
        virtual void drawBackPanel(const rascUI::Location & location) override;
        virtual void drawFrontPanel(const rascUI::Location & location) override;
        virtual void drawText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawButton(const rascUI::Location & location) override;
        virtual void drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) override;
        virtual void drawScrollBarBackground(const rascUI::Location & location) override;
        virtual void drawScrollContentsBackground(const rascUI::Location & location) override;
        virtual void drawScrollPuck(const rascUI::Location & location, bool vertical) override;
        virtual void drawScrollUpButton(const rascUI::Location & location, bool vertical) override;
        virtual void drawScrollDownButton(const rascUI::Location & location, bool vertical) override;
        virtual void drawFadePanel(const rascUI::Location & location) override;
        virtual void drawCheckboxButton(const rascUI::Location & location) override;
        virtual void drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) override;
        virtual void drawTooltipBackground(const rascUI::Location & location) override;
        virtual void drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) override;
        virtual void drawWindowDecorationBackPanel(const rascUI::Location & location) override;
        virtual void drawWindowDecorationFrontPanel(const rascUI::Location & location) override;
        virtual void drawWindowDecorationText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) override;
        virtual void getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) override;
    };
}

// --------------------------------------------------------------------

#ifdef _WIN32
    #define APIFUNC extern "C" __declspec(dllexport)
#else
    #define APIFUNC extern "C"
#endif

APIFUNC const char * getApiVersion(void);

APIFUNC rascUI::Theme * createTheme(void);

APIFUNC void deleteTheme(rascUI::Theme * theme);

#endif
