/*
 * DefaultTheme.cpp
 *
 *  Created on: 5 Oct 2018
 *      Author: wilson
 */

// Force enable the theme if requested.
#ifdef FORCE_ENABLE_OPENGL_DEFAULT_THEME
    #ifndef ENABLE_RASCUI_THEME
        #define ENABLE_RASCUI_THEME
    #endif
#endif

// Only compile if this theme is enabled.
#ifdef ENABLE_RASCUI_THEME

#include "DefaultTheme.h"

#include <GL/gl.h>
#include <stddef.h>
#include <string>

#include <rascUI/util/Location.h>
#include <wool/font/Font.h>
#include <wool/misc/RGBA.h>

#define COLOUR_INT(r, g, b) glColor3i((r) * 8388608, (g) * 8388608, (b) * 8388608)

#define GET_ORIGIN                                                                                              \
    GLfloat xOrigin = location.cache.x, yOrigin = location.cache.y;                                             \
    if (location.getState() == rascUI::State::mousePress || location.getState() == rascUI::State::selected) {   \
        xOrigin += TEXT_XOFF;                                                                                   \
        yOrigin += TEXT_YOFF;                                                                                   \
    }

#define GET_COORDINATES                                 \
    GLfloat x1 = location.cache.x,                      \
            y1 = location.cache.y,                      \
            x2 = x1 + location.cache.width,             \
            y2 = y1 + location.cache.height;

#define GET_6_GRID(x1Pos, x2Pos, x3Pos, y1Pos, y2Pos)   \
    GLfloat x1 = xOrigin + (x1Pos) * location.xScale,   \
            x2 = xOrigin + (x2Pos) * location.xScale,   \
            x3 = xOrigin + (x3Pos) * location.xScale,   \
            y1 = yOrigin + (y1Pos) * location.yScale,   \
            y2 = yOrigin + (y2Pos) * location.yScale;

// The x and y position that the text is drawn relative to the top left of the component.
#define TEXT_X     3.0f
#define TEXT_Y     7.0f

// How much the text is shifted by when the user presses down on the mouse.
#define TEXT_XOFF  1.5f
#define TEXT_YOFF  1.0f

//horizontal offset for flashing text cursor in textinput UI bits.
#define CURSOR_POS 5.0f

namespace rasc {

    DefaultTheme::DefaultTheme(void) :
        rascUI::Theme(),

        // All the custom colours.
        local_colourMainText      (wool::RGB::WHITE),
        local_colourSecondaryText (wool::RGB::GREY),
        local_colourInactiveText  (70, 70, 70),           // Defined by 3 [0-255] values.
        local_colourBackplaneMain (wool::RGB::DARK_GREY), // Defined by already existing colour.
        local_colourBackplaneLight(0.3f, 0.3f, 0.3f),     // Defined by 3 [0-1] values.
        local_colourBackplaneDark (0.2f, 0.2f, 0.2f) {

        customColourMainText       = &local_colourMainText;
        customColourSecondaryText  = &local_colourSecondaryText;
        customColourInactiveText   = &local_colourInactiveText;
        customColourBackplaneMain  = &local_colourBackplaneMain;
        customColourBackplaneLight = &local_colourBackplaneLight;
        customColourBackplaneDark  = &local_colourBackplaneDark;

        customTextCharWidth = wool::Font::wideSpacedFont.getCharWidth();
        customTitleTextCharWidth = wool::Font::font.getCharWidth();

        normalComponentWidth = 18.0f;
        normalComponentHeight = 24.0f;
        uiBorder = 2.0f;
        
        windowDecorationNormalComponentWidth = normalComponentWidth;
        windowDecorationNormalComponentHeight = normalComponentHeight;
        windowDecorationUiBorder = uiBorder;
    }

    DefaultTheme::~DefaultTheme(void) {
    }

    void DefaultTheme::init(void * userData) {
    }
    void DefaultTheme::beforeDraw(void * userData) {
    }
    void DefaultTheme::afterDraw(const rascUI::Rectangle & drawnArea) {
    }
    void DefaultTheme::redrawFromBuffer(void) {
    }
    void * DefaultTheme::getDrawBuffer(void) {
        return NULL;
    }
    void DefaultTheme::destroy(void) {
    }

    void DefaultTheme::onChangeViewArea(const rascUI::Rectangle & view) {
    }

    void DefaultTheme::drawBackPanel(const rascUI::Location & location) {

        GET_COORDINATES

        COLOUR_INT(160, 160, 160);

        glVertex2f(x1, y1);
        glVertex2f(x2, y1);

        glVertex2f(x2, y2);
        glVertex2f(x1, y2);
    }


    void DefaultTheme::drawFrontPanel(const rascUI::Location & location) {

        GET_COORDINATES

        if (location.getState() == rascUI::State::inactive) {
            COLOUR_INT(20, 20, 20);
        } else {
            COLOUR_INT(60, 60, 60);
        }

        glVertex2f(x1, y1);
        glVertex2f(x2, y1);

        if (location.getState() == rascUI::State::inactive) {
            COLOUR_INT(20, 20, 20);
        } else {
            COLOUR_INT(10, 10, 10);
        }

        glVertex2f(x2, y2);
        glVertex2f(x1, y2);
    }



    void DefaultTheme::drawText(const rascUI::Location & location, const std::string & string) {

        // For choosing text colour based on the state of button
        if (location.getState() == rascUI::State::inactive) {
            wool_setColourRGB(local_colourInactiveText);
        } else {
            wool_setColourRGB(local_colourMainText);
        }

        if (location.getState() == rascUI::State::mousePress || location.getState() == rascUI::State::selected) {
            wool::Font::wideSpacedFont.drawFloorStringScale(string, location.cache.x + (TEXT_X + TEXT_XOFF) * location.xScale, location.cache.y + (TEXT_Y + TEXT_YOFF) * location.yScale, location.xScale, location.yScale);
        } else {
            wool::Font::wideSpacedFont.drawFloorStringScale(string, location.cache.x + TEXT_X * location.xScale, location.cache.y + TEXT_Y * location.yScale, location.xScale, location.yScale);
        }
    }




    void DefaultTheme::drawButton(const rascUI::Location & location) {

        GET_COORDINATES
        //top of button colour
        switch(location.getState()) {
            case rascUI::State::normal:     COLOUR_INT(85, 85, 85);  break;
            case rascUI::State::mouseOver:  COLOUR_INT(40, 40, 40);  break;
            case rascUI::State::mousePress: COLOUR_INT( 0,  0,  0);  break;
            case rascUI::State::selected:   COLOUR_INT(20, 20, 20);  break;
            case rascUI::State::inactive:   COLOUR_INT(20, 20, 20);  break;
            default: break;
        }

        glVertex2f(x1, y1);
        glVertex2f(x2, y1);
        //bottom of button colour
        switch(location.getState()) {
            case rascUI::State::normal:     COLOUR_INT(20, 20, 20);  break;
            case rascUI::State::mouseOver:  COLOUR_INT(0 ,  0,  0);  break;
            case rascUI::State::mousePress: COLOUR_INT(40, 40, 40);  break;
            case rascUI::State::selected:   COLOUR_INT(85, 85, 85);  break;
            case rascUI::State::inactive:   COLOUR_INT(20, 20, 20);  break;
            default: break;
        }

        glVertex2f(x2, y2);
        glVertex2f(x1, y2);
    }




    void DefaultTheme::drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) {

        GET_COORDINATES

        switch(location.getState()) {
            case rascUI::State::normal:     COLOUR_INT(40, 40, 40); break;
            case rascUI::State::mouseOver:  COLOUR_INT(200, 200, 200); break;
            case rascUI::State::mousePress: COLOUR_INT(255, 255, 255); break;
            case rascUI::State::selected:   COLOUR_INT(120, 120, 120); break;
            default: break;
        }

        glVertex2f(x1, y1);
        glVertex2f(x2, y1);

        switch(location.getState()) {
            case rascUI::State::normal:     COLOUR_INT(60, 60, 60); break;
            case rascUI::State::mouseOver:  COLOUR_INT(220, 220, 220); break;
            case rascUI::State::mousePress: COLOUR_INT(255, 255, 255); break;
            case rascUI::State::selected:   COLOUR_INT(160, 160, 160); break;
            default: break;
        }

        glVertex2f(x2, y2);
        glVertex2f(x1, y2);
        // above is outliner box, below is inner text input field
        {
            GLfloat border = 2.0f * location.xScale;
            x1 += border, y1 += border, x2 -= border, y2 -= border;
        }

        switch(location.getState()) {
            case rascUI::State::normal:     COLOUR_INT(140, 140, 140); break;
            case rascUI::State::mouseOver:  COLOUR_INT(120, 120, 120); break;
            case rascUI::State::mousePress: COLOUR_INT(200, 200, 200); break;
            case rascUI::State::selected:   COLOUR_INT(180, 180, 180); break;
            default: break;
        }

        glVertex2f(x1, y1);
        glVertex2f(x2, y1);

        switch(location.getState()) {
            case rascUI::State::normal:     COLOUR_INT(200, 200, 200); break;
            case rascUI::State::mouseOver:  COLOUR_INT(200, 200, 200); break;
            case rascUI::State::mousePress: COLOUR_INT(255, 255, 255); break;
            case rascUI::State::selected:   COLOUR_INT(255, 255, 255); break;
            default: break;
        }

        glVertex2f(x2, y2);
        glVertex2f(x1, y2);

        wool_setColourRGBA(wool::RGBA::BLACK);
        if (location.getState() == rascUI::State::mousePress || location.getState() == rascUI::State::selected) {
            wool::Font::wideSpacedFont.drawFloorStringScale(string, location.cache.x + (TEXT_X + TEXT_XOFF) * location.xScale, location.cache.y + (TEXT_Y + TEXT_YOFF) * location.yScale, location.xScale, location.yScale);
        } else {
            wool::Font::wideSpacedFont.drawFloorStringScale(string, location.cache.x + TEXT_X * location.xScale, location.cache.y + TEXT_Y * location.yScale, location.xScale, location.yScale);
        }

        if (drawCursor) {
            GLfloat cursorWidth = 2.0f * location.xScale;

            GLfloat cX1 = x1 + cursorPos * location.xScale * 10.0f + CURSOR_POS * location.xScale,
                    cX2 = cX1 + cursorWidth;

            y1 += cursorWidth;
            y2 -= cursorWidth;

            glVertex2f(cX1, y1);
            glVertex2f(cX2, y1);
            glVertex2f(cX2, y2);
            glVertex2f(cX1, y2);
        }
    }


    void DefaultTheme::drawScrollBarBackground(const rascUI::Location & location) {
        drawBackPanel(location);
    }

    void DefaultTheme::drawScrollContentsBackground(const rascUI::Location & location) {
        // drawBackPanel(location);
        GET_COORDINATES

        COLOUR_INT(160, 160, 160);

        glVertex2f(x1, y1);
        glVertex2f(x2, y1);

        glVertex2f(x2, y2);
        glVertex2f(x1, y2);
    }

    void DefaultTheme::drawScrollPuck(const rascUI::Location & location, bool vertical) {
        drawButton(location);
    }

    void DefaultTheme::drawScrollUpButton(const rascUI::Location & location, bool vertical) {
        // Draw the button background.
        drawButton(location);

        // Set the colour to white.
        wool_setColourRGBA(wool::RGBA::WHITE);

        // Generate coordinates.
        GET_ORIGIN
        GET_6_GRID(1, 9, 17, 8, 16)

        // Draw the arrow shape.
        glVertex2f(x2, y2);
        glVertex2f(x3, y2);
        glVertex2f(x2, y1);
        glVertex2f(x1, y2);
    }

    void DefaultTheme::drawScrollDownButton(const rascUI::Location & location, bool vertical) {
        // Draw the button background.
        drawButton(location);

        // Set the colour to white.
        wool_setColourRGBA(wool::RGBA::WHITE);

        // Generate coordinates.
        GET_ORIGIN
        GET_6_GRID(1, 9, 17, 8, 16)

        // Draw the arrow shape.
        glVertex2f(x2, y1);
        glVertex2f(x1, y1);
        glVertex2f(x2, y2);
        glVertex2f(x3, y1);
    }

    void DefaultTheme::drawFadePanel(const rascUI::Location & location) {
        GET_COORDINATES
        glColor4f(0.0f, 0.0f, 0.0f, 0.6f);
        glVertex2f(x1, y1);
        glVertex2f(x2, y1);
        glVertex2f(x2, y2);
        glVertex2f(x1, y2);
    }

    void DefaultTheme::drawCheckboxButton(const rascUI::Location & location) {
    }
    void DefaultTheme::drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) {
    }
    void DefaultTheme::drawTooltipBackground(const rascUI::Location & location) {
    }
    void DefaultTheme::drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) {
    }

    void DefaultTheme::drawWindowDecorationBackPanel(const rascUI::Location & location) {
        drawBackPanel(location);
    }
    
    void DefaultTheme::drawWindowDecorationFrontPanel(const rascUI::Location & location) {
        drawFrontPanel(location);
    }
    
    void DefaultTheme::drawWindowDecorationText(const rascUI::Location & location, const std::string & string) {
        drawText(location, string);
    }
    
    void DefaultTheme::drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) {
        drawButton(location);
    }

    void DefaultTheme::customSetDrawColour(void * colour) {
        wool_setColourRGB(*((wool::RGB *)colour));
    }
    void * DefaultTheme::customGetTextColour(const rascUI::Location & location) {
        // For choosing text colour based on the state of button
        if (location.getState() == rascUI::State::inactive) {
            return customColourInactiveText;
        } else {
            return customColourMainText;
        }
    }
    void DefaultTheme::customDrawTextMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {
        if (location.getState() == rascUI::State::mousePress || location.getState() == rascUI::State::selected) {
            wool::Font::wideSpacedFont.drawFloorStringScale(string, location.cache.x + (offsetX + TEXT_X + TEXT_XOFF) * location.xScale, location.cache.y + (offsetY + TEXT_Y + TEXT_YOFF) * location.yScale, location.xScale * scaleX, location.yScale * scaleY);
        } else {
            wool::Font::wideSpacedFont.drawFloorStringScale(string, location.cache.x + (offsetX + TEXT_X) * location.xScale, location.cache.y + (offsetY + TEXT_Y) * location.yScale, location.xScale * scaleX, location.yScale * scaleY);
        }
    }
    void DefaultTheme::customDrawTextNoMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {
        wool::Font::wideSpacedFont.drawFloorStringScale(string, location.cache.x + (offsetX + TEXT_X) * location.xScale, location.cache.y + (offsetY + TEXT_Y) * location.yScale, location.xScale * scaleX, location.yScale * scaleY);
    }
    void DefaultTheme::customDrawTextTitle(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {
        if (location.getState() == rascUI::State::mousePress || location.getState() == rascUI::State::selected) {
            wool::Font::font.drawFloorStringScale(string, location.cache.x + (offsetX + TEXT_X + TEXT_XOFF) * location.xScale, location.cache.y + (offsetY + TEXT_Y + TEXT_YOFF) * location.yScale, location.xScale * scaleX, location.yScale * scaleY);
        } else {
            wool::Font::font.drawFloorStringScale(string, location.cache.x + (offsetX + TEXT_X) * location.xScale, location.cache.y + (offsetY + TEXT_Y) * location.yScale, location.xScale * scaleX, location.yScale * scaleY);
        }
    }

    void DefaultTheme::getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) {
        if (state == rascUI::State::mousePress || state == rascUI::State::selected) {
            offX = TEXT_X + TEXT_XOFF;
            offY = TEXT_Y + TEXT_YOFF;
        } else {
            offX = TEXT_X;
            offY = TEXT_Y;
        }
    }
}

// --------------------------------------------------------------------

const char * getApiVersion(void) {
    return "theme-rascUI-0.7";
}

rascUI::Theme * createTheme(void) {
    return new rasc::DefaultTheme();
}

void deleteTheme(rascUI::Theme * theme) {
    delete theme;
}

#endif
