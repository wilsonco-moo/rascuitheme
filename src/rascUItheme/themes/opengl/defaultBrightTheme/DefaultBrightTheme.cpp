/*
 * DefaultBrightTheme.cpp
 *
 *  Created on: 7 Jul 2019
 *      Author: wilson
 */

// Force enable the theme if requested.
#ifdef FORCE_ENABLE_OPENGL_DEFAULT_BRIGHT_THEME
    #ifndef ENABLE_RASCUI_THEME
        #define ENABLE_RASCUI_THEME
    #endif
#endif

// Only compile if this theme is enabled.
#ifdef ENABLE_RASCUI_THEME

#include "DefaultBrightTheme.h"

#include <GL/gl.h>
#include <stddef.h>
#include <string>

#include <rascUI/util/Location.h>
#include <wool/font/Font.h>
#include <wool/misc/RGBA.h>

/**
 * This macro sets the colour, based on 3 0-255 values.
 */
#define COLOUR_INT(r, g, b) glColor3i((r) * 8388608, (g) * 8388608, (b) * 8388608);
// This allows a COLOUR_INT colour to be used with a predefined colour macro.
#define COLOUR_INT3(col) COLOUR_INT(col)

/**
 * Gets the origin position of the location, in two variables:
 * xOrigin and yOrigin.
 * This then adds the text x offset and y offset onto it.
 */
#define GET_ORIGIN                                                                                              \
    GLfloat xOrigin = location.cache.x, yOrigin = location.cache.y;                                             \
    if (location.getState() == rascUI::State::mousePress || location.getState() == rascUI::State::selected) {   \
        xOrigin += TEXT_XOFF;                                                                                   \
        yOrigin += TEXT_YOFF;                                                                                   \
    }

/**
 * From the defined variables xOrigin and yOrigin, this generates a 6 coordinate grid.
 */
#define GET_6_GRID(x1Pos, x2Pos, x3Pos, y1Pos, y2Pos)   \
    GLfloat x1 = xOrigin + (x1Pos) * location.xScale,   \
            x2 = xOrigin + (x2Pos) * location.xScale,   \
            x3 = xOrigin + (x3Pos) * location.xScale,   \
            y1 = yOrigin + (y1Pos) * location.yScale,   \
            y2 = yOrigin + (y2Pos) * location.yScale;





// The x and y position that the text is drawn relative to the top left of the component.
#define TEXT_X     5.0f
#define TEXT_Y     7.0f

// How much the text is shifted by when the user presses down on the mouse.
#define TEXT_XOFF  2.0f
#define TEXT_YOFF  1.0f

//horizontal offset for flashing text cursor in textinput UI bits.
#define CURSOR_POS 4.0f

#define CORNER_SIZE 3.0f

// The colour for drawing borders around components. This is defined as a macro, since this
// colour is only used internally within the theme. This should be used with the COLOUR_INT3 macro.
#define BORDER_COLOUR 70, 70, 70



namespace rasc {

    // --------------- Coords class ---------------------

    Coords::Coords(const rascUI::Location & location) :
        location(&location),
        x1(location.cache.x),
        y1(location.cache.y),
        x2(x1 + location.cache.width),
        y2(y1 + location.cache.height) {
    }
    void Coords::drawTop(void) {
        glVertex2f(x1, y1);
        glVertex2f(x2, y1);
    }
    void Coords::drawBottom(void) {
        glVertex2f(x2, y2);
        glVertex2f(x1, y2);
    }
    void Coords::drawAll(void) {
        glVertex2f(x1, y1); glVertex2f(x2, y1);
        glVertex2f(x2, y2); glVertex2f(x1, y2);
    }

    void Coords::drawOctoTop(GLfloat corner) {
        GLfloat sc = corner * location->xScale,
              y1sc = y1 + sc;
        glVertex2f(x1 + sc, y1); glVertex2f(x2 - sc, y1); glVertex2f(x2, y1sc); glVertex2f(x1, y1sc);
        glVertex2f(x1, y1sc);    glVertex2f(x2, y1sc);
    }

    void Coords::drawOctoBottom(GLfloat corner) {
        GLfloat sc = corner * location->xScale,
              y2sc = y2 - sc;
        glVertex2f(x2, y2sc); glVertex2f(x1, y2sc);
        glVertex2f(x1, y2sc); glVertex2f(x2, y2sc); glVertex2f(x2 - sc, y2); glVertex2f(x1 + sc, y2);
    }

    void Coords::drawOcto(GLfloat corner) {
        GLfloat sc = corner * location->xScale,
              y1sc = y1 + sc,
              y2sc = y2 - sc;
        glVertex2f(x1 + sc, y1); glVertex2f(x2 - sc, y1); glVertex2f(x2, y1sc);    glVertex2f(x1, y1sc);
        glVertex2f(x1, y1sc);    glVertex2f(x2, y1sc);    glVertex2f(x2, y2sc);    glVertex2f(x1, y2sc);
        glVertex2f(x1, y2sc);    glVertex2f(x2, y2sc);    glVertex2f(x2 - sc, y2); glVertex2f(x1 + sc, y2);
    }

    void Coords::subBorder(GLfloat border) {
        border *= location->xScale;
        x1 += border; y1 += border;
        x2 -= border; y2 -= border;
    }

    // --------------------------------------------------

    DefaultBrightTheme::DefaultBrightTheme(void) :
        rascUI::Theme(),

        // All the custom colours.
        local_colourMainText      (wool::RGB::BLACK),
        local_colourSecondaryText (wool::RGB::GREY),
        local_colourInactiveText  (120, 120, 120),         // Defined by 3 [0-255] values.
        local_colourBackplaneMain (wool::RGB::LIGHT_GREY), // Defined by already existing colour.
        local_colourBackplaneLight(0.7f, 0.7f, 0.7f),      // Defined by 3 [0-1] values.
        local_colourBackplaneDark (0.8f, 0.8f, 0.8f) {

        customColourMainText       = &local_colourMainText;
        customColourSecondaryText  = &local_colourSecondaryText;
        customColourInactiveText   = &local_colourInactiveText;
        customColourBackplaneMain  = &local_colourBackplaneMain;
        customColourBackplaneLight = &local_colourBackplaneLight;
        customColourBackplaneDark  = &local_colourBackplaneDark;

        customTextCharWidth = wool::Font::wideSpacedFont.getCharWidth();
        customTitleTextCharWidth = wool::Font::font.getCharWidth();

        normalComponentWidth = 19.0f;
        normalComponentHeight = 24.0f;
        uiBorder = 4.0f;
        
        windowDecorationNormalComponentWidth = normalComponentWidth;
        windowDecorationNormalComponentHeight = normalComponentHeight;
        windowDecorationUiBorder = uiBorder;
    }

    DefaultBrightTheme::~DefaultBrightTheme(void) {
    }

    void DefaultBrightTheme::init(void * userData) {
    }
    void DefaultBrightTheme::beforeDraw(void * userData) {
    }
    void DefaultBrightTheme::afterDraw(const rascUI::Rectangle & drawnArea) {
    }
    void DefaultBrightTheme::redrawFromBuffer(void) {
    }
    void * DefaultBrightTheme::getDrawBuffer(void) {
        return NULL;
    }
    void DefaultBrightTheme::destroy(void) {
    }

    void DefaultBrightTheme::onChangeViewArea(const rascUI::Rectangle & view) {
    }

    void DefaultBrightTheme::drawBackPanel(const rascUI::Location & location) {
        COLOUR_INT(237, 236, 235)
        Coords coords(location);
        coords.drawAll();
    }

    void DefaultBrightTheme::drawFrontPanel(const rascUI::Location & location) {
        COLOUR_INT3(BORDER_COLOUR);
        Coords coords(location);
        coords.drawOcto(CORNER_SIZE);
        coords.subBorder(1);
        if (location.getState() == rascUI::State::inactive) {
            COLOUR_INT(235, 235, 235);
        } else {
            COLOUR_INT(222, 221, 220);
        }
        coords.drawOctoTop(CORNER_SIZE);
        if (location.getState() == rascUI::State::inactive) {
            COLOUR_INT(235, 235, 235);
        } else {
            COLOUR_INT(202, 201, 200);
        }
        coords.drawOctoBottom(CORNER_SIZE);
    }



    void DefaultBrightTheme::drawText(const rascUI::Location & location, const std::string & string) {

        // For choosing text colour based on the state of button
        if (location.getState() == rascUI::State::inactive) {
            wool_setColourRGB(local_colourInactiveText);
        } else {
            wool_setColourRGB(local_colourMainText);
        }

        if (location.getState() == rascUI::State::mousePress || location.getState() == rascUI::State::selected) {
            wool::Font::wideSpacedFont.drawFloorStringScale(string, location.cache.x + (TEXT_X + TEXT_XOFF) * location.xScale, location.cache.y + (TEXT_Y + TEXT_YOFF) * location.yScale, location.xScale, location.yScale);
        } else {
            wool::Font::wideSpacedFont.drawFloorStringScale(string, location.cache.x + TEXT_X * location.xScale, location.cache.y + TEXT_Y * location.yScale, location.xScale, location.yScale);
        }
    }




    void DefaultBrightTheme::drawButton(const rascUI::Location & location) {
        COLOUR_INT3(BORDER_COLOUR);
        Coords coords(location);
        coords.drawOcto(CORNER_SIZE);
        coords.subBorder(1);

        // Top of button colour
        switch(location.getState()) {
            case rascUI::State::normal:     COLOUR_INT(235, 235, 235);  break;
            case rascUI::State::mouseOver:  COLOUR_INT(255, 255, 255);  break;
            case rascUI::State::mousePress: COLOUR_INT(170, 170, 170);  break;
            case rascUI::State::selected:   COLOUR_INT(215, 215, 215);  break;
            case rascUI::State::inactive:   COLOUR_INT(235, 235, 235);  break;
            default: break;
        }

        coords.drawOctoTop(CORNER_SIZE);

        //bottom of button colour
        switch(location.getState()) {
            case rascUI::State::normal:     COLOUR_INT(170, 170, 170);  break;
            case rascUI::State::mouseOver:  COLOUR_INT(215, 215, 215);  break;
            case rascUI::State::mousePress: COLOUR_INT(235, 235, 235);  break;
            case rascUI::State::selected:   COLOUR_INT(255, 255, 255);  break;
            case rascUI::State::inactive:   COLOUR_INT(235, 235, 235);  break;
            default: break;
        }

        coords.drawOctoBottom(CORNER_SIZE);
    }




    void DefaultBrightTheme::drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) {

        COLOUR_INT3(BORDER_COLOUR);
        Coords coords(location);
        coords.drawOcto(CORNER_SIZE);
        coords.subBorder(1);
        switch(location.getState()) {
            case rascUI::State::normal:     COLOUR_INT(255, 255, 255); break;
            case rascUI::State::mouseOver:  COLOUR_INT(190, 190, 190); break;
            case rascUI::State::mousePress: COLOUR_INT(230, 230, 230); break;
            case rascUI::State::selected:   COLOUR_INT(210, 210, 210); break;
            default: break;
        }
        coords.drawOcto(CORNER_SIZE);

        // For choosing text colour based on the state of button
        if (location.getState() == rascUI::State::inactive) {
            wool_setColourRGB(local_colourInactiveText);
        } else {
            wool_setColourRGB(local_colourMainText);
        }

        // Draw the main text in the text entry box.
        if (location.getState() == rascUI::State::mousePress || location.getState() == rascUI::State::selected) {
            wool::Font::wideSpacedFont.drawFloorStringScale(string, location.cache.x + (TEXT_X + TEXT_XOFF) * location.xScale, location.cache.y + (TEXT_Y + TEXT_YOFF) * location.yScale, location.xScale, location.yScale);
        } else {
            wool::Font::wideSpacedFont.drawFloorStringScale(string, location.cache.x + TEXT_X * location.xScale, location.cache.y + TEXT_Y * location.yScale, location.xScale, location.yScale);
        }

        // Draw the flashing cursor, (if we are supposed to).
        if (drawCursor) {
            GLfloat cursorWidth = 2.0f * location.xScale;

            GLfloat cX1 = coords.x1 + cursorPos * location.xScale * 10.0f + CURSOR_POS * location.xScale,
                    cX2 = cX1 + cursorWidth;

            coords.y1 += cursorWidth;
            coords.y2 -= cursorWidth;

            glVertex2f(cX1, coords.y1);
            glVertex2f(cX2, coords.y1);
            glVertex2f(cX2, coords.y2);
            glVertex2f(cX1, coords.y2);
        }
    }


    void DefaultBrightTheme::drawScrollBarBackground(const rascUI::Location & location) {
        drawFrontPanel(location);
    }

    void DefaultBrightTheme::drawScrollContentsBackground(const rascUI::Location & location) {
        COLOUR_INT(237, 236, 235)
        Coords coords(location);
        coords.drawAll();
    }

    void DefaultBrightTheme::drawScrollPuck(const rascUI::Location & location, bool vertical) {
        drawButton(location);
    }

    void DefaultBrightTheme::drawScrollUpButton(const rascUI::Location & location, bool vertical) {
        // Draw the button background.
        drawButton(location);

        // Set the colour to white.
        wool_setColourRGBA(wool::RGBA::BLACK);

        // Generate coordinates.
        GET_ORIGIN
        GET_6_GRID(2, 10, 18, 8, 16)

        // Draw the arrow shape.
        glVertex2f(x2, y2);
        glVertex2f(x3, y2);
        glVertex2f(x2, y1);
        glVertex2f(x1, y2);
    }

    void DefaultBrightTheme::drawScrollDownButton(const rascUI::Location & location, bool vertical) {
        // Draw the button background.
        drawButton(location);

        // Set the colour to white.
        wool_setColourRGBA(wool::RGBA::BLACK);

        // Generate coordinates.
        GET_ORIGIN
        GET_6_GRID(2, 10, 18, 8, 16)

        // Draw the arrow shape.
        glVertex2f(x2, y1);
        glVertex2f(x1, y1);
        glVertex2f(x2, y2);
        glVertex2f(x3, y1);
    }

    void DefaultBrightTheme::drawFadePanel(const rascUI::Location & location) {
        glColor4f(0.0f, 0.0f, 0.0f, 0.6f);
        Coords coords(location);
        coords.drawAll();
    }

    void DefaultBrightTheme::drawCheckboxButton(const rascUI::Location & location) {
    }
    void DefaultBrightTheme::drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) {
    }
    void DefaultBrightTheme::drawTooltipBackground(const rascUI::Location & location) {
    }
    void DefaultBrightTheme::drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) {
    }
    
    void DefaultBrightTheme::drawWindowDecorationBackPanel(const rascUI::Location & location) {
        drawBackPanel(location);
    }
    
    void DefaultBrightTheme::drawWindowDecorationFrontPanel(const rascUI::Location & location) {
        drawFrontPanel(location);
    }
    
    void DefaultBrightTheme::drawWindowDecorationText(const rascUI::Location & location, const std::string & string) {
        drawText(location, string);
    }
    
    void DefaultBrightTheme::drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) {
        drawButton(location);
    }

    void DefaultBrightTheme::customSetDrawColour(void * colour) {
        wool_setColourRGB(*((wool::RGB *)colour));
    }
    void * DefaultBrightTheme::customGetTextColour(const rascUI::Location & location) {
        // For choosing text colour based on the state of button
        if (location.getState() == rascUI::State::inactive) {
            return customColourInactiveText;
        } else {
            return customColourMainText;
        }
    }
    void DefaultBrightTheme::customDrawTextMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {
        if (location.getState() == rascUI::State::mousePress || location.getState() == rascUI::State::selected) {
            wool::Font::wideSpacedFont.drawFloorStringScale(string, location.cache.x + (offsetX + TEXT_X + TEXT_XOFF) * location.xScale, location.cache.y + (offsetY + TEXT_Y + TEXT_YOFF) * location.yScale, location.xScale * scaleX, location.yScale * scaleY);
        } else {
            wool::Font::wideSpacedFont.drawFloorStringScale(string, location.cache.x + (offsetX + TEXT_X) * location.xScale, location.cache.y + (offsetY + TEXT_Y) * location.yScale, location.xScale * scaleX, location.yScale * scaleY);
        }
    }
    void DefaultBrightTheme::customDrawTextNoMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {
        wool::Font::wideSpacedFont.drawFloorStringScale(string, location.cache.x + (offsetX + TEXT_X) * location.xScale, location.cache.y + (offsetY + TEXT_Y) * location.yScale, location.xScale * scaleX, location.yScale * scaleY);
    }
    void DefaultBrightTheme::customDrawTextTitle(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {
        if (location.getState() == rascUI::State::mousePress || location.getState() == rascUI::State::selected) {
            wool::Font::font.drawFloorStringScale(string, location.cache.x + (offsetX + TEXT_X + TEXT_XOFF) * location.xScale, location.cache.y + (offsetY + TEXT_Y + TEXT_YOFF) * location.yScale, location.xScale * scaleX, location.yScale * scaleY);
        } else {
            wool::Font::font.drawFloorStringScale(string, location.cache.x + (offsetX + TEXT_X) * location.xScale, location.cache.y + (offsetY + TEXT_Y) * location.yScale, location.xScale * scaleX, location.yScale * scaleY);
        }
    }

    void DefaultBrightTheme::getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) {
        if (state == rascUI::State::mousePress || state == rascUI::State::selected) {
            offX = TEXT_X + TEXT_XOFF;
            offY = TEXT_Y + TEXT_YOFF;
        } else {
            offX = TEXT_X;
            offY = TEXT_Y;
        }
    }
}

// --------------------------------------------------------------------

const char * getApiVersion(void) {
    return "theme-rascUI-0.7";
}

rascUI::Theme * createTheme(void) {
    return new rasc::DefaultBrightTheme();
}

void deleteTheme(rascUI::Theme * theme) {
    delete theme;
}

#endif
