/*
 * ModernBrightTheme.cpp
 *
 *  Created on: 28 Aug 2020
 *      Author: wilson
 */

// Force enable the theme if requested.
#ifdef FORCE_ENABLE_MODERNOPENGL_MODERN_BRIGHT_THEME
    #ifndef ENABLE_RASCUI_THEME
        #define ENABLE_RASCUI_THEME
    #endif
#endif

// Only compile if this theme is enabled.
#ifdef ENABLE_RASCUI_THEME

#include <GL/gl3w.h> // Include first

#include "ModernBrightTheme.h"

#include <rascUI/util/Location.h>
#include <wool/font/Font.h>
#include <wool/misc/RGBA.h>
#include <awe/util/Util.h>
#include <algorithm>
#include <cstddef>
#include <string>
#include <cmath>
#include <array>


/**
 * Gets the origin position of the location, in two variables:
 * xOrigin and yOrigin.
 * This then adds the text x offset and y offset onto it.
 */
#define GET_ORIGIN                                                                                              \
    GLfloat xOrigin = location.cache.x, yOrigin = location.cache.y;                                             \
    if (location.getState() == rascUI::State::mousePress || location.getState() == rascUI::State::selected) {   \
        xOrigin += TEXT_XOFF;                                                                                   \
        yOrigin += TEXT_YOFF;                                                                                   \
    }

/**
 * From the defined variables xOrigin and yOrigin, this generates a 6 coordinate grid.
 */
#define GET_6_GRID(x1Pos, x2Pos, x3Pos, y1Pos, y2Pos)   \
    GLfloat x1 = xOrigin + (x1Pos) * location.xScale,   \
            x2 = xOrigin + (x2Pos) * location.xScale,   \
            x3 = xOrigin + (x3Pos) * location.xScale,   \
            y1 = yOrigin + (y1Pos) * location.yScale,   \
            y2 = yOrigin + (y2Pos) * location.yScale;

/**
 * The x and y position that the text is drawn relative to the top left of the component.
 */
#define TEXT_X      5.0f
#define TEXT_Y      7.0f

/**
 * How much the text is shifted by when the user presses down on the mouse.
 */
#define TEXT_XOFF   2.0f
#define TEXT_YOFF   1.0f

/**
 * Horizontal offset for flashing text cursor in textinput UI bits. 
 */
#define CURSOR_POS  4.0f

/**
 * Size for corners (in scale adjusted pixels).
 */
#define CORNER_SIZE 3.0f



namespace modernBright {
    
    // ------------------ Colours -----------------------
    
    namespace colours {
        constexpr static std::array<GLubyte, 4> mainText      {0,   0,   0,   255},
                                                secondaryText {128, 128, 128, 255},
                                                inactiveText  {120, 120, 120, 255},
                                                backplaneMain {192, 192, 192, 255},
                                                backplaneLight{178, 178, 178, 255},
                                                backplaneDark {205, 205, 205, 255},
                                                border        {70,  70,  70,  255},
                                                backPanel     {237, 236, 235, 255};
    }


    // --------------- Coords class ---------------------

    Coords::Coords(const rascUI::Location & location, awe::QuadBuffer & buffer) :
        location(&location),
        buffer(&buffer),
        x1(location.cache.x),
        y1(location.cache.y),
        x2(x1 + location.cache.width),
        y2(y1 + location.cache.height) {
    }
    void Coords::drawTop(void) {
        buffer->vertex2f(x1, y1);
        buffer->vertex2f(x2, y1);
    }
    void Coords::drawBottom(void) {
        buffer->vertex2f(x2, y2);
        buffer->vertex2f(x1, y2);
    }
    void Coords::drawAll(void) {
        buffer->vertex2f(x1, y1); buffer->vertex2f(x2, y1);
        buffer->vertex2f(x2, y2); buffer->vertex2f(x1, y2);
    }

    void Coords::drawOctoTop(GLfloat corner) {
        GLfloat sc = corner * location->xScale,
              y1sc = y1 + sc;
        buffer->vertex2f(x1 + sc, y1); buffer->vertex2f(x2 - sc, y1); buffer->vertex2f(x2, y1sc); buffer->vertex2f(x1, y1sc);
        buffer->vertex2f(x1, y1sc);    buffer->vertex2f(x2, y1sc);
    }

    void Coords::drawOctoBottom(GLfloat corner) {
        GLfloat sc = corner * location->xScale,
              y2sc = y2 - sc;
        buffer->vertex2f(x2, y2sc); buffer->vertex2f(x1, y2sc);
        buffer->vertex2f(x1, y2sc); buffer->vertex2f(x2, y2sc); buffer->vertex2f(x2 - sc, y2); buffer->vertex2f(x1 + sc, y2);
    }
    
    void Coords::drawOctoLeft(GLfloat corner) {
        GLfloat sc = corner * location->xScale,
              x1sc = x1 + sc;
        buffer->vertex2f(x1, y1 + sc); buffer->vertex2f(x1, y2 - sc); buffer->vertex2f(x1sc, y2); buffer->vertex2f(x1sc, y1);
        buffer->vertex2f(x1sc, y1);    buffer->vertex2f(x1sc, y2);
    }

    void Coords::drawOctoRight(GLfloat corner) {
        GLfloat sc = corner * location->xScale,
              x2sc = x2 - sc;
        buffer->vertex2f(x2sc, y2); buffer->vertex2f(x2sc, y1);
        buffer->vertex2f(x2sc, y1); buffer->vertex2f(x2sc, y2); buffer->vertex2f(x2, y2 - sc); buffer->vertex2f(x2, y1 + sc);
    }
    
    void Coords::drawOctoTopPart(GLfloat corner, GLfloat proportion) {
        GLfloat sc = corner * location->xScale,
              y1sc = y1 + sc,
               end = (x2 - x1 - sc*2.0f) * proportion + x1 + sc;
        buffer->vertex2f(x1 + sc, y1); buffer->vertex2f(end, y1); buffer->vertex2f(end, y1sc); buffer->vertex2f(x1, y1sc);
        buffer->vertex2f(x1, y1sc);    buffer->vertex2f(end, y1sc);
    }

    void Coords::drawOctoBottomPart(GLfloat corner, GLfloat proportion) {
        GLfloat sc = corner * location->xScale,
              y2sc = y2 - sc,
               end = (x2 - x1 - sc*2.0f) * proportion + x1 + sc;
        buffer->vertex2f(end, y2sc); buffer->vertex2f(x1, y2sc);
        buffer->vertex2f(x1, y2sc); buffer->vertex2f(end, y2sc); buffer->vertex2f(end, y2); buffer->vertex2f(x1 + sc, y2);
    }
    
    void Coords::drawOctoLeftPart(GLfloat corner, GLfloat proportion) {
        GLfloat sc = corner * location->xScale,
              x1sc = x1 + sc,
               end = (y2 - y1 - sc*2.0f) * proportion + y1 + sc;
        buffer->vertex2f(x1, y1 + sc); buffer->vertex2f(x1, end); buffer->vertex2f(x1sc, end); buffer->vertex2f(x1sc, y1);
        buffer->vertex2f(x1sc, y1);    buffer->vertex2f(x1sc, end);
    }

    void Coords::drawOctoRightPart(GLfloat corner, GLfloat proportion) {
        GLfloat sc = corner * location->xScale,
              x2sc = x2 - sc,
               end = (y2 - y1 - sc*2.0f) * proportion + y1 + sc;
        buffer->vertex2f(x2sc, end); buffer->vertex2f(x2sc, y1);
        buffer->vertex2f(x2sc, y1); buffer->vertex2f(x2sc, end); buffer->vertex2f(x2, end); buffer->vertex2f(x2, y1 + sc);
    }

    void Coords::drawOcto(GLfloat corner) {
        GLfloat sc = corner * location->xScale,
              y1sc = y1 + sc,
              y2sc = y2 - sc;
        buffer->vertex2f(x1 + sc, y1); buffer->vertex2f(x2 - sc, y1); buffer->vertex2f(x2, y1sc);    buffer->vertex2f(x1, y1sc);
        buffer->vertex2f(x1, y1sc);    buffer->vertex2f(x2, y1sc);    buffer->vertex2f(x2, y2sc);    buffer->vertex2f(x1, y2sc);
        buffer->vertex2f(x1, y2sc);    buffer->vertex2f(x2, y2sc);    buffer->vertex2f(x2 - sc, y2); buffer->vertex2f(x1 + sc, y2);
    }

    void Coords::subBorder(GLfloat border) {
        border *= location->xScale;
        x1 += border; y1 += border;
        x2 -= border; y2 -= border;
    }

    // --------------------------------------------------

    ModernBrightTheme::ModernBrightTheme(void) :
        rascUI::Theme(),
        shaderProgram(awe::Util::compileShaderProgram("Quad buffer shader",
            #include <awe/buffers/quadBufferShader/vertex.glsl-out>
            , NULL,
            #include <awe/buffers/quadBufferShader/fragment.glsl-out>
        )),
        buffer(shaderProgram),
        redrawBufferCalled(false) {

        customColourMainText       = (void *)&colours::mainText;
        customColourSecondaryText  = (void *)&colours::secondaryText;
        customColourInactiveText   = (void *)&colours::inactiveText;
        customColourBackplaneMain  = (void *)&colours::backplaneMain;
        customColourBackplaneLight = (void *)&colours::backplaneLight;
        customColourBackplaneDark  = (void *)&colours::backplaneDark;

        customTextCharWidth = wool::Font::wideSpacedFont.getCharWidth();
        customTitleTextCharWidth = wool::Font::font.getCharWidth();

        normalComponentWidth = 19.0f;
        normalComponentHeight = 24.0f;
        uiBorder = 4.0f;
        
        windowDecorationNormalComponentWidth = normalComponentWidth;
        windowDecorationNormalComponentHeight = normalComponentHeight;
        windowDecorationUiBorder = uiBorder;
    }

    ModernBrightTheme::~ModernBrightTheme(void) {
        glDeleteShader(shaderProgram);
    }
    
    void ModernBrightTheme::drawText(const GLfloat * font, const std::string & str, GLfloat x, GLfloat y, GLfloat xScale, GLfloat yScale) {
        // Avoid inconsistent rounding by flooring coordinate first.
        x = std::floor(x);
        y = std::floor(y);
        
        // Extract data from font, and move the font pointer to the start of
        // coordinate data.
        int minChar       = (int)font[0],
            floatsPerChar = (int)font[4];
        GLfloat charWidth = font[5] * xScale;
        font += 7;
        
        for (unsigned char character : str) {
            const GLfloat * charPointer = font + (floatsPerChar * (character - minChar));
            // Get the number of shapes stored in this character. This is the first
            // value in each character. Multiply by 8 to get the number of float values in
            // this character.
            int coords = ((int) *(charPointer++)) * 8;
            const GLfloat * charEndPointer = charPointer + coords;
            while(charPointer != charEndPointer) {
                GLfloat xPos = *(charPointer++) * xScale + x,
                        yPos = *(charPointer++) * yScale + y;
                buffer.vertex2f(xPos, yPos);
            }
            x += charWidth;
        }
    }

    void ModernBrightTheme::init(void * userData) {
    }
    void ModernBrightTheme::beforeDraw(void * userData) {
    }
    void ModernBrightTheme::afterDraw(const rascUI::Rectangle & drawnArea) {
        if (redrawBufferCalled) {
            redrawBufferCalled = false;
        } else {
            buffer.finalise();
            glUseProgram(shaderProgram);
            buffer.draw();
            glUseProgram(0);
        }
    }
    void ModernBrightTheme::redrawFromBuffer(void) {
        redrawBufferCalled = true;
        glUseProgram(shaderProgram);
        buffer.draw();
        glUseProgram(0);
    }
    void * ModernBrightTheme::getDrawBuffer(void) {
        return &buffer;
    }
    void ModernBrightTheme::destroy(void) {
    }

    void ModernBrightTheme::onChangeViewArea(const rascUI::Rectangle & view) {
        buffer.onChangeViewArea(view.width, view.height);
    }

    void ModernBrightTheme::drawBackPanel(const rascUI::Location & location) {
        buffer.setColour4a(colours::backPanel);
        Coords coords(location, buffer);
        coords.drawAll();
    }

    void ModernBrightTheme::drawFrontPanel(const rascUI::Location & location) {
        buffer.setColour4a(colours::border);
        Coords coords(location, buffer);
        coords.drawOcto(CORNER_SIZE);
        coords.subBorder(1);
        if (location.getState() == rascUI::State::inactive) {
            buffer.setColour3b(235, 235, 235);
        } else {
            buffer.setColour3b(222, 221, 220);
        }
        coords.drawOctoTop(CORNER_SIZE);
        if (location.getState() == rascUI::State::inactive) {
            buffer.setColour3b(235, 235, 235);
        } else {
            buffer.setColour3b(202, 201, 200);
        }
        coords.drawOctoBottom(CORNER_SIZE);
    }



    void ModernBrightTheme::drawText(const rascUI::Location & location, const std::string & string) {

        // For choosing text colour based on the state of button
        if (location.getState() == rascUI::State::inactive) {
            buffer.setColour4a(colours::inactiveText);
        } else {
            buffer.setColour4a(colours::mainText);
        }

        if (location.getState() == rascUI::State::mousePress || location.getState() == rascUI::State::selected) {
            drawText(wool::Font::wideSpacedFont.getData(), string, location.cache.x + (TEXT_X + TEXT_XOFF) * location.xScale, location.cache.y + (TEXT_Y + TEXT_YOFF) * location.yScale, location.xScale, location.yScale);
        } else {
            drawText(wool::Font::wideSpacedFont.getData(), string, location.cache.x + TEXT_X * location.xScale, location.cache.y + TEXT_Y * location.yScale, location.xScale, location.yScale);
        }
    }




    void ModernBrightTheme::drawButton(const rascUI::Location & location) {
        buffer.setColour4a(colours::border);
        Coords coords(location, buffer);
        coords.drawOcto(CORNER_SIZE);
        coords.subBorder(1);

        // Top of button colour
        switch(location.getState()) {
            case rascUI::State::normal:     buffer.setColour3b(235, 235, 235);  break;
            case rascUI::State::mouseOver:  buffer.setColour3b(255, 255, 255);  break;
            case rascUI::State::mousePress: buffer.setColour3b(170, 170, 170);  break;
            case rascUI::State::selected:   buffer.setColour3b(215, 215, 215);  break;
            case rascUI::State::inactive:   buffer.setColour3b(235, 235, 235);  break;
            default: break;
        }

        coords.drawOctoTop(CORNER_SIZE);

        //bottom of button colour
        switch(location.getState()) {
            case rascUI::State::normal:     buffer.setColour3b(170, 170, 170);  break;
            case rascUI::State::mouseOver:  buffer.setColour3b(215, 215, 215);  break;
            case rascUI::State::mousePress: buffer.setColour3b(235, 235, 235);  break;
            case rascUI::State::selected:   buffer.setColour3b(255, 255, 255);  break;
            case rascUI::State::inactive:   buffer.setColour3b(235, 235, 235);  break;
            default: break;
        }

        coords.drawOctoBottom(CORNER_SIZE);
    }




    void ModernBrightTheme::drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) {

        buffer.setColour4a(colours::border);
        Coords coords(location, buffer);
        coords.drawOcto(CORNER_SIZE);
        coords.subBorder(1);
        switch(location.getState()) {
            case rascUI::State::normal:     buffer.setColour3b(255, 255, 255); break;
            case rascUI::State::mouseOver:  buffer.setColour3b(190, 190, 190); break;
            case rascUI::State::mousePress: buffer.setColour3b(230, 230, 230); break;
            case rascUI::State::selected:   buffer.setColour3b(210, 210, 210); break;
            default: break;
        }
        coords.drawOcto(CORNER_SIZE);

        // For choosing text colour based on the state of button
        if (location.getState() == rascUI::State::inactive) {
            buffer.setColour4a(colours::inactiveText);
        } else {
            buffer.setColour4a(colours::mainText);
        }

        // Draw the main text in the text entry box.
        if (location.getState() == rascUI::State::mousePress || location.getState() == rascUI::State::selected) {
            drawText(wool::Font::wideSpacedFont.getData(), string, location.cache.x + (TEXT_X + TEXT_XOFF) * location.xScale, location.cache.y + (TEXT_Y + TEXT_YOFF) * location.yScale, location.xScale, location.yScale);
        } else {
            drawText(wool::Font::wideSpacedFont.getData(), string, location.cache.x + TEXT_X * location.xScale, location.cache.y + TEXT_Y * location.yScale, location.xScale, location.yScale);
        }

        // Draw the flashing cursor, (if we are supposed to).
        if (drawCursor) {
            GLfloat cursorWidth = 2.0f * location.xScale;

            GLfloat cX1 = coords.x1 + cursorPos * location.xScale * 10.0f + CURSOR_POS * location.xScale,
                    cX2 = cX1 + cursorWidth;

            coords.y1 += cursorWidth;
            coords.y2 -= cursorWidth;

            buffer.vertex2f(cX1, coords.y1);
            buffer.vertex2f(cX2, coords.y1);
            buffer.vertex2f(cX2, coords.y2);
            buffer.vertex2f(cX1, coords.y2);
        }
    }


    void ModernBrightTheme::drawScrollBarBackground(const rascUI::Location & location) {
        drawFrontPanel(location);
    }

    void ModernBrightTheme::drawScrollContentsBackground(const rascUI::Location & location) {
        buffer.setColour3b(237, 236, 235);
        Coords coords(location, buffer);
        coords.drawAll();
    }

    void ModernBrightTheme::drawScrollPuck(const rascUI::Location & location, bool vertical) {
        drawButton(location);
    }

    void ModernBrightTheme::drawScrollUpButton(const rascUI::Location & location, bool vertical) {
        // Draw the button background.
        drawButton(location);

        // Set the colour to black.
        buffer.setColour3b(0, 0, 0);

        // Generate coordinates.
        GET_ORIGIN
        GET_6_GRID(2, 10, 18, 8, 16)

        // Draw the arrow shape.
        buffer.vertex2f(x2, y2);
        buffer.vertex2f(x3, y2);
        buffer.vertex2f(x2, y1);
        buffer.vertex2f(x1, y2);
    }

    void ModernBrightTheme::drawScrollDownButton(const rascUI::Location & location, bool vertical) {
        // Draw the button background.
        drawButton(location);

        // Set the colour to black.
        buffer.setColour3b(0, 0, 0);

        // Generate coordinates.
        GET_ORIGIN
        GET_6_GRID(2, 10, 18, 8, 16)

        // Draw the arrow shape.
        buffer.vertex2f(x2, y1);
        buffer.vertex2f(x1, y1);
        buffer.vertex2f(x2, y2);
        buffer.vertex2f(x3, y1);
    }

    void ModernBrightTheme::drawFadePanel(const rascUI::Location & location) {
        buffer.setColour4b(0, 0, 0, 153);
        Coords coords(location, buffer);
        coords.drawAll();
    }

    void ModernBrightTheme::drawCheckboxButton(const rascUI::Location & location) {
    }
    void ModernBrightTheme::drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) {
        buffer.setColour4a(colours::border);
        Coords coords(location, buffer);
        coords.drawOcto(CORNER_SIZE);
        coords.subBorder(1);
        
        std::array<GLubyte, 4> primBack, secBack, primMain, secMain;
        if (location.getState() == rascUI::State::inactive) {
            primBack = {235, 235, 235, 255};
            secBack  = {235, 235, 235, 255};
            primMain = {211, 218, 227, 255};
            secMain  = {187, 217, 255, 255};
        } else {
            primBack = {202, 201, 200, 255};
            secBack  = {222, 221, 220, 255};
            primMain = {125, 169, 227, 255};
            secMain  = {84,  138, 204, 255};
        }
        
        // Swap background/main colours and invert progress, for inverted state.
        if (inverted) {
            std::swap(primBack, primMain);
            std::swap(secBack,  secMain);
            progress = 1.0f - progress;
        }
        
        if (vertical) {
            if (progress >= 1.0f) { // Display only main colour for full progress.
                buffer.setColour4a(primMain);
                coords.drawOctoLeft(CORNER_SIZE);
                buffer.setColour4a(secMain);
                coords.drawOctoRight(CORNER_SIZE);
            } else {
                buffer.setColour4a(primBack);
                coords.drawOctoLeft(CORNER_SIZE);
                buffer.setColour4a(secBack);
                coords.drawOctoRight(CORNER_SIZE);
                if (progress > 0.0f) { // Only display main colour over the top if progress is not zero.
                    buffer.setColour4a(primMain);
                    coords.drawOctoLeftPart(CORNER_SIZE, progress);
                    buffer.setColour4a(secMain);
                    coords.drawOctoRightPart(CORNER_SIZE, progress);
                }
            }
            
        } else {
            if (progress >= 1.0f) { // Display only main colour for full progress.
                buffer.setColour4a(primMain);
                coords.drawOctoTop(CORNER_SIZE);
                buffer.setColour4a(secMain);
                coords.drawOctoBottom(CORNER_SIZE);
            } else {
                buffer.setColour4a(primBack);
                coords.drawOctoTop(CORNER_SIZE);
                buffer.setColour4a(secBack);
                coords.drawOctoBottom(CORNER_SIZE);
                if (progress > 0.0f) { // Only display main colour over the top if progress is not zero.
                    buffer.setColour4a(primMain);
                    coords.drawOctoTopPart(CORNER_SIZE, progress);
                    buffer.setColour4a(secMain);
                    coords.drawOctoBottomPart(CORNER_SIZE, progress);
                }
            }
        }
    }
    void ModernBrightTheme::drawTooltipBackground(const rascUI::Location & location) {
    }
    void ModernBrightTheme::drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) {
    }
    
    void ModernBrightTheme::drawWindowDecorationBackPanel(const rascUI::Location & location) {
        drawBackPanel(location);
    }
    
    void ModernBrightTheme::drawWindowDecorationFrontPanel(const rascUI::Location & location) {
        drawFrontPanel(location);
    }
    
    void ModernBrightTheme::drawWindowDecorationText(const rascUI::Location & location, const std::string & string) {
        drawText(location, string);
    }
    
    void ModernBrightTheme::drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) {
        drawButton(location);
    }

    void ModernBrightTheme::customSetDrawColour(void * colour) {
        buffer.setColour4a(*((const std::array<GLubyte, 4> *)colour));
    }
    void * ModernBrightTheme::customGetTextColour(const rascUI::Location & location) {
        // For choosing text colour based on the state of button
        if (location.getState() == rascUI::State::inactive) {
            return customColourInactiveText;
        } else {
            return customColourMainText;
        }
    }
    void ModernBrightTheme::customDrawTextMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {
        if (location.getState() == rascUI::State::mousePress || location.getState() == rascUI::State::selected) {
            drawText(wool::Font::wideSpacedFont.getData(), string, location.cache.x + (offsetX + TEXT_X + TEXT_XOFF) * location.xScale, location.cache.y + (offsetY + TEXT_Y + TEXT_YOFF) * location.yScale, location.xScale * scaleX, location.yScale * scaleY);
        } else {
            drawText(wool::Font::wideSpacedFont.getData(), string, location.cache.x + (offsetX + TEXT_X) * location.xScale, location.cache.y + (offsetY + TEXT_Y) * location.yScale, location.xScale * scaleX, location.yScale * scaleY);
        }
    }
    void ModernBrightTheme::customDrawTextNoMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {
        drawText(wool::Font::wideSpacedFont.getData(), string, location.cache.x + (offsetX + TEXT_X) * location.xScale, location.cache.y + (offsetY + TEXT_Y) * location.yScale, location.xScale * scaleX, location.yScale * scaleY);
    }
    void ModernBrightTheme::customDrawTextTitle(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) {
        if (location.getState() == rascUI::State::mousePress || location.getState() == rascUI::State::selected) {
            drawText(wool::Font::font.getData(), string, location.cache.x + (offsetX + TEXT_X + TEXT_XOFF) * location.xScale, location.cache.y + (offsetY + TEXT_Y + TEXT_YOFF) * location.yScale, location.xScale * scaleX, location.yScale * scaleY);
        } else {
            drawText(wool::Font::font.getData(), string, location.cache.x + (offsetX + TEXT_X) * location.xScale, location.cache.y + (offsetY + TEXT_Y) * location.yScale, location.xScale * scaleX, location.yScale * scaleY);
        }
    }

    void ModernBrightTheme::getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) {
        if (state == rascUI::State::mousePress || state == rascUI::State::selected) {
            offX = TEXT_X + TEXT_XOFF;
            offY = TEXT_Y + TEXT_YOFF;
        } else {
            offX = TEXT_X;
            offY = TEXT_Y;
        }
    }
}

// --------------------------------------------------------------------

const char * getApiVersion(void) {
    return "theme-rascUI-0.7";
}

rascUI::Theme * createTheme(void) {
    return new modernBright::ModernBrightTheme();
}

void deleteTheme(rascUI::Theme * theme) {
    delete theme;
}

#endif
