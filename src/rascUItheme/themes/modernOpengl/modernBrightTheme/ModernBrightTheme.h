/*
 * ModernBrightTheme.h
 *
 *  Created on: 28 Aug 2020
 *      Author: wilson
 */

#ifndef RASCUITHEME_THEMES_MODERNOPENGL_MODERNBRIGHTTHEME_MODERNBRIGHTTHEME_H_
#define RASCUITHEME_THEMES_MODERNOPENGL_MODERNBRIGHTTHEME_MODERNBRIGHTTHEME_H_

#include <awe/buffers/QuadBuffer.h>
#include <rascUI/base/Theme.h>
#include <GL/gl.h>
#include <string>


namespace rascUI {
    class Location;
}


namespace modernBright {


    class Coords {
    private:
        const rascUI::Location * location;
        awe::QuadBuffer * buffer;
    public:
        GLfloat x1, y1, x2, y2;
        Coords(const rascUI::Location & location, awe::QuadBuffer & buffer);
        void drawTop(void);
        void drawBottom(void);
        void drawAll(void);
        void drawOctoTop(GLfloat corner);
        void drawOctoBottom(GLfloat corner);
        void drawOctoLeft(GLfloat corner);
        void drawOctoRight(GLfloat corner);
        void drawOctoTopPart(GLfloat corner, GLfloat proportion);
        void drawOctoBottomPart(GLfloat corner, GLfloat proportion);
        void drawOctoLeftPart(GLfloat corner, GLfloat proportion);
        void drawOctoRightPart(GLfloat corner, GLfloat proportion);
        
        void drawOcto(GLfloat corner);
        void subBorder(GLfloat border);
    };


    class ModernBrightTheme : public rascUI::Theme {
    private:
        GLuint shaderProgram;
        awe::QuadBuffer buffer;
        bool redrawBufferCalled;
        
    public:
        ModernBrightTheme(void);
        virtual ~ModernBrightTheme(void);

    private:
        void drawText(const GLfloat * data, const std::string & str, GLfloat x, GLfloat y, GLfloat xScale, GLfloat yScale);

    protected:
        virtual void init(void * userData) override;
    public:
        virtual void beforeDraw(void * userData) override;
        virtual void afterDraw(const rascUI::Rectangle & drawnArea) override;
        virtual void redrawFromBuffer(void) override;
        virtual void * getDrawBuffer(void) override;
    protected:
        virtual void destroy(void) override;
    public:
        virtual void onChangeViewArea(const rascUI::Rectangle & view) override;
        virtual void drawBackPanel(const rascUI::Location & location) override;
        virtual void drawFrontPanel(const rascUI::Location & location) override;
        virtual void drawText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawButton(const rascUI::Location & location) override;
        virtual void drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) override;
        virtual void drawScrollBarBackground(const rascUI::Location & location) override;
        virtual void drawScrollContentsBackground(const rascUI::Location & location) override;
        virtual void drawScrollPuck(const rascUI::Location & location, bool vertical) override;
        virtual void drawScrollUpButton(const rascUI::Location & location, bool vertical) override;
        virtual void drawScrollDownButton(const rascUI::Location & location, bool vertical) override;
        virtual void drawFadePanel(const rascUI::Location & location) override;
        virtual void drawCheckboxButton(const rascUI::Location & location) override;
        virtual void drawProgressBar(const rascUI::Location & location, GLfloat progress, bool vertical, bool inverted) override;
        virtual void drawTooltipBackground(const rascUI::Location & location) override;
        virtual void drawSlider(const rascUI::Location & location, GLfloat position, bool vertical, bool inverted) override;
        virtual void drawWindowDecorationBackPanel(const rascUI::Location & location) override;
        virtual void drawWindowDecorationFrontPanel(const rascUI::Location & location) override;
        virtual void drawWindowDecorationText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawWindowDecorationButton(const rascUI::Location & location, rascUI::WindowDecorationButtonType type) override;

        virtual void customSetDrawColour(void * colour) override;
        virtual void * customGetTextColour(const rascUI::Location & location) override;
        virtual void customDrawTextMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) override;
        virtual void customDrawTextNoMouse(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) override;
        virtual void customDrawTextTitle(const rascUI::Location & location, GLfloat offsetX, GLfloat offsetY, GLfloat scaleX, GLfloat scaleY, const std::string & string) override;
        virtual void getTextBaseOffsets(rascUI::State state, GLfloat & offX, GLfloat & offY) override;
    };

}

// --------------------------------------------------------------------

#ifdef _WIN32
    #define APIFUNC extern "C" __declspec(dllexport)
#else
    #define APIFUNC extern "C"
#endif

APIFUNC const char * getApiVersion(void);

APIFUNC rascUI::Theme * createTheme(void);

APIFUNC void deleteTheme(rascUI::Theme * theme);

#endif
