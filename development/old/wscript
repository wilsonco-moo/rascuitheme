#! /usr/bin/env python3

# Waf-based C++ dependency managing build script
# Copyright (C) 30 June 2019  Daniel Wilson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

top = "."     # The project location. Keep this as . unless this wscript file cannot be in the top level project folder.
out = "build" # The location of the build folder
libs = "libs" # The location of the libraries folder

def _setup(conf):
    
    _defineCompileOptions(
        build = ["-O0", "-g1", "-Wall", "-pedantic"],
        export = ["-Os", "-Wall"]
    )
    
    _defineLinkOptions(
        build = [],
        export = []
    )
    
    _defineRemoteLibrary(
        libraryName    = "rascUI",
        repoName       = "rascui",
        repo           = "git@gitlab.com:wilsonco-moo/rascui.git",
        remoteLocation = "src/rascUI",
        compileOptions = [],
        libs           = ["m"]
    )
    
    _defineRemoteLibrary(
        libraryName    = "rascUIxcb",
        repoName       = "rascuixcb",
        repo           = "git@gitlab.com:wilsonco-moo/rascuixcb.git",
        remoteLocation = "src/rascUIxcb",
        compileOptions = [],
        libs           = ["xcb", "xcb-keysyms", "xcb-util", "m", "rascUI"]
    )
    
    _defineLocalLibrary(
        libraryName    = "rascUItheme",
        localLocation  = "rascUItheme",
        compileOptions = [],
        libs           = ["dl"]
    )
    
    _defineMainProgram(
        name           = "uiTestProgram",
        sourceLocation = "src",
        compileOptions = [],
        libs           = ["xcb", "xcb-keysyms", "xcb-util", "m", "dl", "rascUI", "rascUIxcb", "rascUItheme"]
    )




class Library:
    def __init__(self, isRemote = False, libraryName = None, repoName = None, repo = None, remoteLocation = None, compileOptions = [], libs = [], linkFlags = []):
        self.isRemote = isRemote
        self.libraryName = libraryName
        self.repoName = repoName
        self.repo = repo
        self.remoteLocation = remoteLocation
        self.compileOptions = compileOptions
        self.libs = libs
        self.linkFlags = linkFlags

mainProgram = None
libraries = []

globalCompileOptions = []
globalLinkOptions = []
globalExportCompileOptions = []
globalExportLinkOptions = []

def _defineMainProgram(name = None, sourceLocation = None, compileOptions = [], libs = [], linkFlags = []):
    global mainProgram
    mainProgram = Library(libraryName = name, remoteLocation = sourceLocation, compileOptions = compileOptions, libs = libs, linkFlags = linkFlags)

def _defineRemoteLibrary(libraryName = None, repoName = None, repo = None, remoteLocation = None, compileOptions = [], libs = [], linkFlags = []):
    libraries.append(Library(isRemote = True, libraryName = libraryName, repoName = repoName, repo = repo, remoteLocation = remoteLocation, compileOptions = compileOptions, libs = libs, linkFlags = linkFlags))
    
def _defineLocalLibrary(libraryName = None, localLocation = None, compileOptions = [], libs = [], linkFlags = []):
    libraries.append(Library(libraryName = libraryName, remoteLocation = localLocation, compileOptions = compileOptions, libs = libs, linkFlags = linkFlags))

def _defineCompileOptions(build = None, export = None):
    global globalCompileOptions, globalExportCompileOptions
    globalCompileOptions = build
    globalExportCompileOptions = export

def _defineLinkOptions(build = None, export = None):
    global globalLinkOptions, globalExportLinkOptions
    globalLinkOptions = build
    globalExportLinkOptions = export

def options(opt):
    opt.load('compiler_c++')

def build(cxt):
    if mainProgram == None:
        _setup(cxt)
    _internalBuild(cxt, globalCompileOptions, globalLinkOptions)

def export(cxt):
    if mainProgram == None:
        _setup(cxt)
    _internalBuild(cxt, globalExportCompileOptions, globalExportLinkOptions)

# Ensure that export gets run using a build context.
from waflib.Context import Context
from waflib.Build import BuildContext
class CustomExportContext(BuildContext):
    cmd = 'export'
    fun = 'export'

def _internalBuild(cxt, compileOptions, linkOptions):
    srcDir = cxt.path.find_node(mainProgram.remoteLocation)
    # Generate a set of all the source files in repository.
    allSourceFiles = srcDir.ant_glob(["**/*.c", "**/*.cpp"])
    srcSet = set()
    for node in allSourceFiles:
        srcSet.add(node.path_from(srcDir))
    
    for library in libraries:
        # Get the library's directory, which is the same as the name for remote libraries, and is the location for local libraries.
        libraryDir = None
        if library.isRemote:
            libraryDir = srcDir.find_node(library.libraryName)
        else:
            libraryDir = srcDir.find_node(library.remoteLocation)
        # Remove all of the source files in this library from the set of all source files.
        libraryFiles = libraryDir.ant_glob(["**/*.c", "**/*.cpp"])
        for node in libraryFiles:
            srcSet.remove(node.path_from(srcDir))
        # Compile this as a shared library.
        cxt.shlib(
            source       = libraryFiles,
            target       = library.libraryName,
            includes     = [srcDir],
            lib          = library.libs,
            libpath      = [cxt.path.get_bld().abspath()],
            cflags       = mainProgram.compileOptions + compileOptions,
            cxxflags     = mainProgram.compileOptions + compileOptions,
            linkflags    = library.linkFlags + linkOptions
        )
        
        # Make sure that we wait for one library to compile, then do the next one.
        # Otherwise if there is a dependency, waf is too stupid to wait for the previous one
        # to *actually* finish, so you get a file truncated issue.
        cxt.add_group()
    
    # Convert the remaining source files to a list of nodes.
    mainProgramSources = []
    for path in srcSet:
        mainProgramSources.append(srcDir.find_node(path))
    # .. and compile the main program.
    cxt.program(
        source       = mainProgramSources,
        target       = mainProgram.libraryName,
        includes     = [srcDir],
        lib          = mainProgram.libs,
        libpath      = [cxt.path.get_bld().abspath()],
        cflags       = mainProgram.compileOptions + compileOptions,
        cxxflags     = mainProgram.compileOptions + compileOptions,
        linkflags    = mainProgram.linkFlags + linkOptions
    )

def configure(cxt):
    from subprocess import call
    import sys, os
    from stat import S_ISLNK
    
    cxt.load('compiler_c++')
    if mainProgram == None:
        _setup(cxt)
    
    # Check that the user actually defined the main program, and that the source directory they specified exists.
    if mainProgram == None:
        sys.stderr.write("ERROR: Main program not set. Please use _defineMainProgram function in _setup.\n")
        sys.exit(1)
    srcDir = cxt.path.find_node(mainProgram.remoteLocation)
    if srcDir == None:
        sys.stderr.write("ERROR: Cannot find source directory "+cxt.path.make_node(mainProgram.remoteLocation).abspath()+".\n")
        sys.exit(1)
    elif not srcDir.isdir():
        sys.stderr.write("ERROR: Something which is not a directory exists instead of the source directory "+srcDir.abspath()+".\n")
        sys.exit(1)
    
    # Create the libraries directory if it does not already exist. Complain if there is something which is not a directory there.
    libNode = cxt.path.find_node(libs)
    if libNode == None:
        print("Creating libraries directory...")
        libNode = cxt.path.make_node(libs)
        libNode.mkdir()
    elif not libNode.isdir():
        sys.stderr.write("ERROR: Something which is not a directory exists instead of the libraries directory "+libNode.abspath()+".\n")
        sys.exit(1)
    
    # Clone all the repositories into the libraries directory. For any which already exist, do a git pull.
    print("Cloning/pulling repositories...")
    for library in libraries:
        if library.isRemote:
            repoDir = libNode.find_node(library.repoName)
            if repoDir == None:
                print("Cloning "+library.repo+" ...")
                result = call(["git", "clone", library.repo], cwd = libNode.abspath())
                repoDir = libNode.find_node(library.repoName)
                if result != 0 or repoDir == None:
                    sys.stderr.write("ERROR: Failed to clone git repository "+library.repo+"\n")
                    sys.exit(1)
            else:
                print("Checking "+library.libraryName+" is up to date...")
                if call(["git", "pull"], cwd = repoDir.abspath()) != 0:
                    sys.stderr.write("ERROR: Failed to pull git repository "+library.repo+"\n")
                    sys.exit(1)
    
    # Create symlinks from the source tree to wherever the source in each library is located, (specified by the user).
    # If any symlinks already exist, check that they point to the correct place.
    print("Creating symlinks...")
    for library in libraries:
        if library.isRemote:
            symSource = srcDir.find_node(library.libraryName)
            symTarget = libNode.find_node(library.repoName).find_node(library.remoteLocation)
            if symTarget == None:
                sys.stderr.write("ERROR: Cannot find remote location "+library.remoteLocation+" within repository "+library.repo+", which should be at the filepath "+libNode.find_node(library.repoName).make_node(library.remoteLocation).abspath()+".\n")
                sys.exit(1)
            if symSource == None:
                symSource = srcDir.make_node(library.libraryName)
                if call(["ln", "-s", symTarget.abspath(), symSource.abspath()], cwd = repoDir.abspath()) != 0:
                    sys.stderr.write("ERROR: Cannot create symlink to "+library.remoteLocation+" within repository "+library.repo+", which should be at the filepath "+libNode.find_node(library.repoName).make_node(library.remoteLocation).abspath()+".\n")
                    sys.exit(1)
            else:
                mode = os.lstat(symSource.abspath()).st_mode
                if S_ISLNK(mode):
                    targetFromSymlink = os.path.realpath(os.path.join(os.path.dirname(symSource.abspath()), os.readlink(symSource.abspath())))
                    if targetFromSymlink != os.path.realpath(symTarget.abspath()):
                        sys.stderr.write("ERROR: Cannot symlink library "+library.libraryName+" into source tree: the following symlink already exists there: ["+symTarget.abspath()+"] -> ["+targetFromSymlink+"].\n")
                        sys.exit(1)
                else:
                    sys.stderr.write("ERROR: Cannot symlink library "+library.libraryName+" into source tree, something else already exists at "+symSource.abspath()+".\n")
                    sys.exit(1)
    
    # Create .gitignore files which ignore all the stuff we have added.
    print("Creating .gitignore files...")
    rootGitIgnore = cxt.path.make_node(".gitignore")
    try:
        with open(rootGitIgnore.abspath(), "w") as f:
            f.write(".gitignore\n"+out+"\n"+libs+"\n.waf*\n.lock-waf*\n")
    except IOError:
        sys.stderr.write("ERROR: Cannot write to .gitignore file "+rootGitIgnore.abspath()+".\n")
        sys.exit(1)
    srcGitIgnore = srcDir.make_node(".gitignore")
    try:
        with open(srcGitIgnore.abspath(), "w") as f:
            f.write(".gitignore\n")
            for library in libraries:
                if library.isRemote:
                    f.write(library.libraryName+"\n")
    except IOError:
        sys.stderr.write("ERROR: Cannot write to .gitignore file "+srcGitIgnore.abspath()+".\n")
        sys.exit(1)

def _customDistclean(cxt):
    from subprocess import call
    if mainProgram == None:
        _setup(cxt)
    
    # Delete the libraries directory, if it exists.
    print("Ensuring libraries directory is deleted...")
    libNode = cxt.path.find_node(libs)
    if libNode != None:
        libNode.delete()
    
    # Delete the build directory, if it exists.
    print("Ensuring build directory is deleted...")
    buildNode = cxt.path.find_node(out)
    if buildNode != None:
        buildNode.delete()
    
    # Delete the symlinks we created in the source tree.
    print("Removing any existing symlinks...")
    srcDir = cxt.path.find_node(mainProgram.remoteLocation)
    for library in libraries:
        if library.isRemote:
            symSource = srcDir.make_node(library.libraryName)
            call(["rm", "-f", symSource.abspath()], cwd = srcDir.abspath())
        
    # Delete the .gitignore files we created.
    print("Removing any existing .gitignore files...")
    rootGitIgnore = cxt.path.find_node(".gitignore")
    if rootGitIgnore != None:
        rootGitIgnore.delete()
    srcGitIgnore = srcDir.make_node(".gitignore")
    if srcGitIgnore != None:
        srcGitIgnore.delete()

# Run _customDistclean each time distclean is done, AS WELL AS the default distclean implementation.
class CustomDistCleanContext(Context):
    cmd = 'distclean'
    def execute(self):
        super(CustomDistCleanContext, self).execute()
        _customDistclean(self)
