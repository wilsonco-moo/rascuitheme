#include <GL/gl.h>
#include <list>
#include <functional>
#include <cstddef>
#include <string>
#include <xcb/xcb.h>
#include <xcb/xproto.h>
#include <cstdlib>
#include <iostream>
/*
 * Rectangle.h
 *
 *  Created on: 1 Oct 2018
 *      Author: wilson
 */

#ifndef RASCUI_UTIL_RECTANGLE_H_
#define RASCUI_UTIL_RECTANGLE_H_


namespace rascUI {

    /**
     * This class represents a simple rectangle, with 4 GLfloat values:
     * x, y, width and height.
     */
    class Rectangle {
    public:
        GLfloat x, y, width, height;

        Rectangle(GLfloat x = 0.0f, GLfloat y = 0.0f, GLfloat width = 0.0f, GLfloat height = 0.0f);
        virtual ~Rectangle(void);

        /**
         * Equality checking with rectangles checks that the x, y, width and height values are identical.
         */
        inline bool operator == (const Rectangle & other) const {
            return x == other.x && y == other.y && width == other.width && height == other.height;
        }
        inline bool operator != (const Rectangle & other) const {
            return x != other.x || y != other.y || width != other.width || height != other.height;
        }

        /**
         * When rectangles are added together, the resulting rectangle is the smallest bounding box which
         * fits both rectangles inside. If either rectangle has a width or height of zero, the other
         * rectangle is returned. If both rectangles have a width and height of zero, a default constructed
         * rectangle is returned.
         */
        Rectangle operator + (const Rectangle & other) const;
        inline Rectangle & operator += (const Rectangle & other) {
            *this = *this + other;
            return *this;
        }

        /**
         * When rectangles are multiplied together, the resulting rectangle is the overlapping region between
         * the two rectangles - (the intersection). If the rectangles do not overlap, a default constructed
         * rectangle is returned.
         * NOTE: Do not use this as a way to check overlap, as it is slower than the overlaps method.
         */
        Rectangle operator * (const Rectangle & other) const;
        inline Rectangle & operator *= (const Rectangle & other) {
            *this = *this * other;
            return *this;
        }

        /**
         * Returns true if this rectangle has a non-zero area - i.e: Our width and height are both
         * greater than zero.
         */
        bool isNonZero(void) const;

        /**
         * Returns true if the specified point is contained within this rectangle.
         */
        bool contains(GLfloat x, GLfloat y) const;

        /**
         * Returns true if the specified rectangle overlaps (collides) with this rectangle.
         * If either rectangle has a width or height of zero, false is automatically returned.
         */
        bool overlaps(const Rectangle & other) const;

        /**
         * These methods return the centre point of the rectangle.
         */
        inline GLfloat centreX(void) const { return x + width * 0.5f; }
        inline GLfloat centreY(void) const { return y + height * 0.5f; }

        /**
         * These methods return a point which is not within the rectangle, assuming the rectangle
         * has positive width and positive height.
         * I.e:
         * rectangle.contains(rectangle.outsideX(), rectangle.outsideY())
         * Is always false, for a rectangle with positive width and height.
         */
        inline GLfloat outsideX(void) const { return x + width + 1.0f; }
        inline GLfloat outsideY(void) const { return y + height + 1.0f; }
    };
}
#endif
/*
 * State.h
 *
 *  Created on: 15 Nov 2018
 *      Author: wilson
 */

#ifndef RASCUI_UTIL_STATE_H_
#define RASCUI_UTIL_STATE_H_


namespace rascUI {

    /**
     * This enum represents the state of a component. The state of a component serves two purposes:
     *  > Controls how the drawing process of the component will take place. It is up to the Theme to decide what (if any)
     *    differences there are between how the component is to be drawn depending upon which state is in use.
     *  > By using the invisible state, the component will not be drawn at all. If this is a container, the component will also not draw
     *    any of it's child components. This allows the easy hiding of ui elements, with very little overhead.
     */
    enum class State {

        /**
         * This is the normal (default) state. This should cause the Theme implementation to draw the component with no special
         * highlighting etc. This is how the component should be *most* of the time.
         */
        normal,

        /**
         * This is the state that is used when the user mouses over the component. This should cause the Theme implementation to
         * draw the component with some highlighting, to give some tactile feedback that the user has mouse-overed a button (or similar).
         */
        mouseOver,

        /**
         * This is the state that is used between the user pressing down the mouse, and the user releasing the mouse. This should cause
         * the Theme implementation to highlight the component differently to mouseOver, preferably with inverted shading to make it
         * look like the button has been pressed.
         */
        mousePress,

        /**
         * This state is (currently) used primarily for toggle components. This represents a selected state, where the user is not currently mousing
         * over the component. This is not used for ordinary mouse responsive components, rather only for toggle components.
         *
         * When the user mouses over a selected toggle component, the mousePress state is used instead of this.
         *
         * This means that it is reasonable for a Theme implementation to draw the same thing for selected and mousePress, if feedback
         * from mousing over a selected toggle components is not important.
         */
        selected,

        /**
         * TODO: Implement logic behind inactive state.
         *
         * This state is used to deactivate a button. If this state is active, the button WILL STILL RECEIVE MOUSE ENTER, PRESS, RELEASE
         * AND LEAVE EVENTS, but will NOT RECEIVE MOUSE CLICK EVENTS.
         *
         * This should cause the Theme implementation to draw the button more grayed-out, or more transparent, to make obvious to
         * the user that this option is not available.
         */
        inactive,

        /**
         * This state causes a component not to be drawn at all. This is not controlled by the Theme implementation, instead this is
         * controlled by the Container class. If this state is active, the Container won't even call the Component's onDraw method. In the
         * case of a container, this state causes it not to draw any of it's child components.
         */
        invisible,
    };


}

#endif
/*
 * Location.h
 *
 *  Created on: 1 Oct 2018
 *      Author: wilson
 */

#ifndef RASCUI_UTIL_LOCATION_H_
#define RASCUI_UTIL_LOCATION_H_



namespace rascUI {

    class Component;


    /**
     * This class defines all the information required to define the position and state of a rascUI component.
     * It has a rectangle for position, and a rectangle for weight.
     * The cache rectangle is calculated when resizing takes place, from the scale, and the values of the position and weight rectangles.
     * This class also stores an x scale and a y scale, and a state field.
     *
     * This means a Location instance contains ALL the sizing information to draw a component.
     *
     * A note about the storage of states:
     *  > States are used to define what each component should look like when drawn. This is used for example to highlight a button
     *    when the user mouse-overs it, and show it pushed in when the user presses their mouse on it.
     *  > The state is represented by the enum State, from State.h.
     *  > Instead of simply storing a state variable, we store a list of std::function<State(State)> instances. When update
     *    is called, we take an initial value of state, (State::normal), and pass it through each of these functions in series.
     *  > The resulting state is cached in the variable cachedState, and is returned when getState() is called.
     *  > All of this allows any class in the class hierarchy to do one of two things:
     *     - Provide a state modifier that overrides the state modifiers of classes further up the class hierarchy, and can
     *       be overidden by classes further down the hierarchy. This can be done by adding a permanent state modifier in the constructor.
     *     - Temporarily override the state modifiers defined by ALL other classes in the hierarchy, by adding a new state modifier
     *       later on, then removing it when we no longer want to override the state. This is used for active/inactive component behaviour.
     */
    class Location {
    private:
        /**
         * This is our cached state, worked out by the state modifier state system.
         */
        State cachedState;

        /**
         * This is the list in which all the state modifiers are stored.
         */
        std::list<std::function<State(State, Component *)>> stateModifiers;

    public:
        /**
         * Position is the position of the component, which is multiplied by the scale.
         * Weight is the weighted position of the component, as a factor of the parent component.
         * Cache is the actual on-screen position of the component, which is calculated from position and weight, when recalculate is called.
         */
        Rectangle position,
                  weight,
                  cache;

        /**
         * This is a copy of the current x scale and y scale. These are used by the drawing process to define the scale to draw
         * features within the cached location.
         */
        GLfloat xScale, yScale;

    public:

        Location(void);

        Location(GLfloat x, GLfloat y, GLfloat width, GLfloat height);
        Location(GLfloat x, GLfloat y, GLfloat width, GLfloat height, GLfloat xWeight, GLfloat yWeight, GLfloat widthWeight, GLfloat heightWeight);

        Location(Rectangle position);
        Location(Rectangle position, Rectangle weight);

        virtual ~Location(void);

        /**
         * This method is called when resizing. This calculates our cache rectangle from our position and weight,
         * fitted into the specified parent rectangle.
         * This method also updates our x scale and y scale.
         */
        void fitCacheInto(const Rectangle & parent, GLfloat xScale, GLfloat yScale);


        /**
         * This method gets our currently cached state. This should be used for drawing and checking current state.
         */
        inline State getState(void) const {
            return cachedState;
        }

        /**
         * This method updates our cached state from the current list of state modifiers. This should be called each time
         * something has changed that would have resulted in our cached state being now wrong.
         *
         * This SHOULD NOT be called every frame if at all possible, instead this should be called ONLY when something changes.
         * This is because if we have a large class hierarchy with lots of different behaviours, the list of state modifiers may
         * be very long. Also, each state modifier may take a long time to execute.
         *
         * The supplied component MUST BE THE OWNER OF THIS LOCATION.
         *
         * This method is allowed to be called, even if the Component does not have a top level container.
         * If the component *does* have a top level container, and partial repainting is enabled, this will cause
         * an automatic repaint, ONLY IF the Component says that we should, through it's doesStateChangeRequireRepaint method.
         */
        void updateState(Component * component);

        /**
         * Pushes a new state modifier to the back of the state modifier list, returning it's iterator.
         * The iterator can then be used in the method removeStateModifier to remove this state modifier.
         */
        std::list<std::function<State(State, Component *)>>::const_iterator pushStateModifier(std::function<State(State, Component *)> stateModifier);

        /**
         * Removes a state modifier, using an iterator returned from pushStateModifier.
         */
        void removeStateModifier(std::list<std::function<State(State, Component *)>>::const_iterator stateModifierIter);
    };
}









/**
 * DESIGN SPEC FOR STATE MODIFIER STATE SYSTEM
 * -------------------------------------------
 *
 *   > Suppose we did not store a State variable, instead we had a std::list<std::function<State(State)>>.
 *   > Use a std::list so we can have constant time removals using an iterator, and retain order of the state modifiers.
 *   > Then we cache our state, and only update it when a method like updateState() (or similar) is called.
 *   > To work out our state:
 *         void updateState(void) {
 *             state = State::normal;
 *             for (std::function<State(State)>> & stateModifier : stateModifiers) {
 *                 state = stateModifier(state);
 *             }
 *         }
 *   > This way each of the std::function instances would have the opportunity to modify the state.
 *   > Each class in the hierarchy which wants to modify the state would push a std::function, it it's constructor.
 *   > Sudden modifications to state, e.g: Switching to State::inactive, would be pushed to the end of the list,
 *     then removed again when the modification is disabled, (this can be done with an iterator).
 *   > We would need two methods: pushStateModifier(), returning an iterator to the new state modifier, and
 *     removeStateModifier(iterator), which can remove a state modifier based on an iterator returned by the previous
 *     method.
 * BUT: This new state system would not allow components that continually assign their state. How would ToggleComponent work?
 *   > It could cache it's previous value of isToggleComponentSelected() each frame. If it changes, it could request
 *     a recalculation of state.
 * BUT: How would we ensure updateState is called at the right times?
 *   > Make it the component code and user code's responsibility to call it.
 * BUT: How would we ensure that updateState is called at the right times when adding and removing the component?
 *   > Call updateState each time a component has it's top level container set.
 * BUT: Consistency between setting inactive, and other mouse state (e.g: From keyboard integration)?
 *   > That would not matter - the internal mouse state which would still be calculated by ResponsiveComponent would simply be
 *     overridden, and would continue to be there once the component is made active again.
 */








#endif
/*
 * Theme.h
 *
 *  Created on: 1 Oct 2018
 *      Author: wilson
 */

#ifndef RASCUI_BASE_THEME_H_
#define RASCUI_BASE_THEME_H_



namespace rascUI {

    class Location;
    class Rectangle;

    /**
     * An instance of Theme should be stored in each TopLevelContainer instance.
     *
     *  > The Theme instance is used to pick how to draw each of the components in the TopLevelContainer. These methods are
     *    bundled into such a class to allow easy changing between UI styles. A new Theme implementation is all that is required
     *    to completely change the look and feel of the UI.
     *
     *  > Theme has a series of abstract methods, each of which should draw a specific type of component. Each method provides as
     *    parameters two things.
     *    - A Location instance. This should be used to work out the position to draw the specific type of Component.
     *    - Other data relevant to that particular type of component, such as the text to draw in a label.
     *    Note: No data about how to draw the component, or the colours to draw the component is provided. This allows the Theme
     *    implementation complete control over the drawing process, allowing for consistent UI.
     *
     *  > The Location parameter in each of these methods contains a special field: state. This is an enum class defined in State.h.
     *    This should affect how each of the component implementations draws, such as a state of State::mouseOver should cause the
     *    Theme implementation to draw the component more highlighted. It is entirely up to the implementation of the Theme subclass
     *    to decide how to highlight, (or even whether to bother).
     *
     *  > NOTE: The Theme implementation does not have to worry about the state State::invisible. Since State::invisible is checked in
     *    the Component and Container classes, we will never receive any draw requests from a component who's state is invisible.
     *    The invisible state causes the Component class not to even call any draw methods.
     *
     * See enum class State in State.h for more information about what the Theme implementation should do for each state.
     *
     * NOTE: Theme is a header-only interface, and no longer has an attached source file.
     */
    class Theme {
    private:
        // Set to true once init has been called.
        bool initCalled;

    public:

        /**
         * This should be updated each time methods are added to Theme, or methods are modified.
         * Only dynamically loaded themes which match this API version should be used.
         */
        #define THEME_API_VERSION "theme-rascUI-0.1"

        /**
         * These should contain the preferred minimum width and height for components using this theme.
         *
         * The height should be used as the minimum for any component which contains or draws text, such as a button,
         * toggle button, text entry box etc.
         *
         * The width should be used as the minimum comfortable usable width for a button. This is used for example,
         * for buttons in the scroll pane.
         *
         * Both of these values are used by the layout system, if it uses a theme.
         */
        const GLfloat normalComponentWidth, normalComponentHeight;

        /**
         * This should contain the preferred minimum distance to place between two components (such as buttons), which
         * are using this theme. It is perfectly acceptable for this to contain a value of zero, if the theme is designed
         * such that no border is needed.
         *
         * This is used extensively by the layout system, and is probably the most important value which the
         * theme must define.
         */
        const GLfloat uiBorder;

        /**
         * The main constructor of Theme. Here the layout options should be provided.
         */
        Theme(GLfloat normalComponentWidth, GLfloat normalComponentHeight, GLfloat uiBorder) :
            initCalled(false),
            normalComponentWidth(normalComponentWidth),
            normalComponentHeight(normalComponentHeight),
            uiBorder(uiBorder) {
        }
        virtual ~Theme(void) {
        }

        /**
         * This returns true once init has been called.
         * This is convenient for use in the destructor of a Theme.
         */
        inline bool hasInitBeenCalled(void) const {
            return initCalled;
        }

        /**
         * This is called ONCE at the start, BEFORE all calls to onChangeViewArea, beforeDraw
         * and afterDraw. This uses the same userData as beforeDraw.
         *
         * Depending on the Theme implementation, carrying out actions here may not be necessary.
         * As a result it is optional to implement this.
         *
         * Optionally, a void pointer of user data can be sent to the theme. This comes from the
         * protected "beforeDrawAndInitThemeUserData" field of TopLevelContainer. Here, subclasses
         * are able to send user data to the theme implementation.
         *
         * NOTE: The superclass method (Theme::init) MUST BE CALLED ALSO BY ANY CLASS OVERRIDING
         *       THIS METHOD. This is because it sets the initCalled variable, for the convenience
         *       of the user.
         */
        virtual void init(void * userData) {
            initCalled = true;
        }

        /**
         * This should be called each time the view area changes, (at this point a TopLevelContainer
         * should also call a recalculation of all components in the hierarchy).
         *
         * This MUST be called AFTER init, but BEFORE beforeDraw and afterDraw.
         *
         * Depending on the Theme implementation, carrying out actions here may not be necessary.
         * As a result it is optional to implement this.
         */
        virtual void onChangeViewArea(const Rectangle & view) {
        }

        /**
         * This is called each time a sequence of drawing takes place, before anything is drawn,
         * after init and onChangeViewArea.
         *
         * Depending on the Theme implementation, carrying out actions here may not be necessary.
         * As a result it is optional to implement this.
         *
         * Optionally, a void pointer of user data can be sent to the theme. This comes from the
         * protected "beforeDrawAndInitThemeUserData" field of TopLevelContainer. Here, subclasses
         * are able to send user data to the theme implementation.
         */
        virtual void beforeDraw(void * userData) {
        }

        /**
         * This is called each time a sequence of drawing takes place, after everything is drawn.
         * Depending on the Theme implementation, carrying out actions here may not be necessary.
         * As a result it is optional to implement this.
         *
         * Optionally, a void pointer of user data can be sent to the theme. This comes from the
         * protected "afterDrawThemeUserData" field of TopLevelContainer. Here, subclasses
         * are able to send user data to the theme implementation.
         *
         * A Rectangle should be provided to this method, which represents the area on-screen which
         * has changed as a result of this drawing operation.
         * In partial repainting mode, this should be the final bounding box after drawing.
         * Otherwise, this should just be the TopLevelContainer's view rectangle.
         */
        virtual void afterDraw(void * userData, const Rectangle & drawnArea) {
        }


        /**
         * This method should draw a rectangular box, for use as the background to a menu, or similar.
         *
         * A FrontPanel component by default does not respond to the mouse, but the theme could respond to mouse
         * states anyway. That would allow FrontPanels to appear selected.
         */
        virtual void drawBackPanel(const Location & location) = 0;

        /**
         * This should draw a rectangular box similar to what is drawn with drawBackPanel. FrontPanels are components
         * that are put in front of a BackPanel, to show a UI section, such as related buttons, or a non-interactive
         * piece of text. A FrontPanel should appear distinct to a button, and look like it is not clickable.
         *
         * A FrontPanel component by default does not respond to the mouse, but the theme could respond to mouse
         * states anyway. That would allow FrontPanels to appear selected.
         */
        virtual void drawFrontPanel(const Location & location) = 0;

        /**
         * This method should draw a piece of text, within the specified rectangle.
         * Text could also respond to mouse, to allow a text label to be manually highlighted within the UI.
         */
        virtual void drawText(const Location & location, const std::string & string) = 0;

        /**
         * This method should draw some text in front of a rectangle, to form a button. This definitely should respond to
         * the mouse, to provide a suitable amount of feedback from the user's interaction.
         */
        virtual void drawButton(const Location & location) = 0;

        /**
         * This should draw a text entry box, with the specified location, text and cursor position. The text entry box
         * should respond to different states, so it is possible for the user to be able to tell if the text entry box
         * is currently being edited.
         *
         * The cursor MUST only be drawn if drawCursor is true, so:
         *  > Cursor blinking works correctly
         *  > The cursor is not drawn if the user is not currently editing this text entry box.
         */
        virtual void drawTextEntryBox(const Location & location, const std::string & string, size_t cursorPos, bool drawCursor) = 0;

        /**
         * This should draw the background panel for a (vertical) scroll bar. It is not necessary, (or
         * particularly useful), for this to respond to different states in the location. Here, it is
         * completely acceptable for a Theme implementation to draw exactly the same as a back panel.
         *
         * Note: This will always have a width of normalComponentWidth + 2 * uiBorder.
         */
        virtual void drawScrollBarBackground(const Location & location) = 0;

        /**
         * This should draw the background panel for a scroll pane, i.e: What is displayed behind the components
         * which are scrolling. It is not necessary, (or particularly useful), for this to respond to different
         * states in the location. Here, it is completely acceptable for a Theme implementation to draw exactly
         * the same as a back panel.
         */
        virtual void drawScrollContentsBackground(const Location & location) = 0;

        /**
         * This should draw the puck for a (vertical) scroll bar. This must respond to all states, like
         * a button. This method is here for fine customisation, it is acceptable for a theme implementation
         * to simply run drawButton here, to make the scroll puck look exactly like a button.
         *
         * Note: This will always have a width of normalComponentWidth.
         */
        virtual void drawScrollPuck(const Location & location) = 0;

        /**
         * This should draw the "scroll up" button, displayed on scroll bars.
         * It is possible (and preferable) for the theme implementation to simply call drawButton, then draw an
         * extra symbol on top.
         * The scroll up and down buttons must be visually distinct from each other, and must respond to mouse
         * states like a button should.
         *
         * Note: This will always have a width of normalComponentWidth, and a height of normalComponentHeight.
         */
        virtual void drawScrollUpButton(const Location & location) = 0;

        /**
         * This should draw the "scroll down" button, displayed on scroll bars.
         * It is possible (and preferable) for the theme implementation to simply call drawButton, then draw an
         * extra symbol on top.
         * The scroll up and down buttons must be visually distinct from each other, and must respond to mouse
         * states like a button should.
         *
         * Note: This will always have a width of normalComponentWidth, and a height of normalComponentHeight.
         */
        virtual void drawScrollDownButton(const Location & location) = 0;

        /**
         * This should draw a semi-transparent box, for use of fading components behind a popup menu.
         *
         * The panel should be drawn semi-transparent, enough to fade-out components behind it, but not too
         * opaque as to obscure them completely.
         *
         * It is not necessary for the theme to respond to states here.
         */
        virtual void drawFadePanel(const Location & location) = 0;
    };

}

#endif

#ifndef RASCUIXCB_MAIN_WINDOWDATA_H_
#define RASCUIXCB_MAIN_WINDOWDATA_H_


namespace rascUIxcb {

    /**
     * This class stores all the basic xcb info about a window.
     * This should be the only information provided to theme implementations.
     */
    class WindowData {
    public:
        xcb_connection_t * connection;
        xcb_screen_t * screen;
        xcb_window_t window;
        xcb_gcontext_t graphics;
        /**
         * This is set to either offScreenBuffer or window, depending on whether
         * double buffering is enabled or disabled respectively.
         * Subclasses MUST USE THIS for all drawing operations, NOT window.
         */
        xcb_drawable_t drawable;
    };
}

#endif
/*
 * Colour.h
 *
 *  Created on: 19 Jun 2019
 *      Author: wilson
 */

#ifndef RASCUIXCB_UTIL_COLOUR_H_
#define RASCUIXCB_UTIL_COLOUR_H_


namespace rascUIxcb {

    class WindowData;

    /**
     * This class represents a colour allocated within an XCB colour map.
     * The colour in the colour map is freed automatically when this instance is destroyed.
     *
     * NOTE: All colour instances created from a given connection MUST
     * be destroyed BEFORE the Connection instance.
     *
     * NOTE: DO NOT INHERIT FROM THIS CLASS.
     */
    class Colour {
    private:
        enum class State {
            // We are storing our colour value literally: setConnection (or the appropriate
            // constructor) has not been called.
            NO_CONNECTION,
            // setConnection (or the appropriate constructor) has been called, but get has
            // not yet been run, so we are still waiting for a response from the X server.
            WAITING,
            // We have received our response from the X server.
            GOT_COLOUR
        };

        union {
            // Our internal colour. This is used in the NO_CONNECTION state.
            uint16_t channels[3];
            // Our response cookie. This is used in the WAITING state.
            xcb_alloc_color_cookie_t cookie;
            // Our final colour. This is used in the GOT_COLOUR state.
            uint32_t colour;
        };

        // Our current state.
        State state;

        // A WindowData instance. This includes a connection and a screen. We must keep a reference to this
        // so we can remove ourself from the colour map when no longer needed.
        WindowData * connection;

        // We can't allow copying, as then we would free our colour from the colour map
        // twice when we are destroyed.
        Colour(const Colour & other);
        Colour & operator = (const Colour & other);

    public:
        /**
         * Creates a colour without a connection. The setConnection method MUST
         * be run at some point later, before get is called.
         *
         * NOTE: Colour values are 16 bit (0-65535 inclusive), NOT 8 bit.
         */
        Colour(uint16_t red, uint16_t green, uint16_t blue);

        /**
         * Creates a colour with a connection by default. Here it will not be necessary
         * to call setConnection.
         * The connection MUST NOT BE NULL.
         *
         * NOTE: Colour values are 16 bit (0-65535 inclusive), NOT 8 bit.
         */
        Colour(uint16_t red, uint16_t green, uint16_t blue, WindowData * connection);

        ~Colour(void); // Don't make this virtual - it's not needed.

        /**
         * This allows the colour to lookup the correct value in the colour map.
         * The connection MUST NOT BE NULL.
         *
         * This MUST ONLY BE CALLED ONCE.
         */
        void setConnection(WindowData * connection);

        /**
         * setConnection (or the appropriate constructor) MUST have been called before this is run.
         */
        uint32_t get(void);

        /**
         * Sets the foreground and background colours of the graphics context, in our WindowData instance,
         * to this colour.
         */
        void setBackground(void);
        void setForeground(void);
    };

}

#endif
/*
 * Colour.cpp
 *
 *  Created on: 19 Jun 2019
 *      Author: wilson
 */



namespace rascUIxcb {

    Colour::Colour(uint16_t red, uint16_t green, uint16_t blue) :
        channels{red, green, blue},
        state(State::NO_CONNECTION),
        connection(NULL) {
    }

    Colour::Colour(uint16_t red, uint16_t green, uint16_t blue, WindowData * connection) :
        cookie(xcb_alloc_color(connection->connection, connection->screen->default_colormap, red, green, blue)),
        state(State::WAITING),
        connection(connection) {
    }

    Colour::~Colour(void) {
        switch(state) {
        case State::WAITING: {
            // If waiting for a response, get the response, and free the colour from the map.
            xcb_alloc_color_reply_t * reply = xcb_alloc_color_reply(connection->connection, cookie, NULL);
            if (reply != NULL) {
                xcb_free_colors(connection->connection, connection->screen->default_colormap, 0, 1, &reply->pixel);
                free(reply);
            }
            break;
        }
        case State::GOT_COLOUR:
            // If already got the colour, free it from the colour map.
            xcb_free_colors(connection->connection, connection->screen->default_colormap, 0, 1, &colour);
            break;
        default:
            // In the NO_CONNECTION state, (or undefined state?) there is nothing to free.
            break;
        }
    }

    void Colour::setConnection(WindowData * connection) {
        if (state == State::NO_CONNECTION) {
            // Simply set the cookie from xcb_alloc_color and go to WAITING state.
            state = State::WAITING;
            this->connection = connection;
            xcb_alloc_color_cookie_t c = xcb_alloc_color(connection->connection, connection->screen->default_colormap, channels[0], channels[1], channels[2]);
            cookie = c;
        } else {
            std::cerr << "WARNING: rascUIxcb::Colour: setConnection called twice.\n";
        }
    }

    uint32_t Colour::get(void) {
        switch(state) {
        case State::WAITING: {
            // If waiting for the colour, get the response, and return that. Set the state to GOT_COLOUR.
            xcb_alloc_color_reply_t * reply = xcb_alloc_color_reply(connection->connection, cookie, NULL);
            state = State::GOT_COLOUR;
            if (reply == NULL) {
                std::cerr << "rascUIxcb::Colour: Failed to lookup colour in screen's default colour map, will default to colour value 0x00000000 instead.\n";
                colour = 0x00000000;
            } else {
                colour = reply->pixel;
                free(reply);
            }
            return colour;
        }
        case State::GOT_COLOUR:
            // If we already have the colour, just return it.
            return colour;
        default:
            std::cerr << "WARNING: rascUIxcb::Colour: get() method called before setConnection.\n";
            return 0x00000000;
        }
    }

    void Colour::setBackground(void) {
        uint32_t col = get();
        xcb_change_gc(connection->connection, connection->graphics, XCB_GC_BACKGROUND, &col);
    }
    void Colour::setForeground(void) {
        uint32_t col = get();
        xcb_change_gc(connection->connection, connection->graphics, XCB_GC_FOREGROUND, &col);
    }
}
