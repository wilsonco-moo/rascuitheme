
#include <rascUI/base/Theme.h>

namespace rascUI {
    class Rectangle;
    class Location;
}

namespace rascUIxcb {
    class WindowData;
}

namespace exampletheme {
    
    // ------------------ Custom theme implementation ----------------------
    
    class ExampleTheme : public rascUI::Theme {
    private:
        rascUIxcb::WindowData * data;
    public:
        ExampleTheme(void);
        virtual ~ExampleTheme(void);
        
        virtual void init(void * userData) override;
        virtual void onChangeViewArea(const rascUI::Rectangle & view) override;
        virtual void beforeDraw(void * userData) override;
        virtual void afterDraw(void * userData, const rascUI::Rectangle & drawnArea) override;
        virtual void drawBackPanel(const rascUI::Location & location) override;
        virtual void drawFrontPanel(const rascUI::Location & location) override;
        virtual void drawText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawButton(const rascUI::Location & location) override;
        virtual void drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) override;
        virtual void drawScrollBarBackground(const rascUI::Location & location) override;
        virtual void drawScrollContentsBackground(const rascUI::Location & location) override;
        virtual void drawScrollPuck(const rascUI::Location & location) override;
        virtual void drawScrollUpButton(const rascUI::Location & location) override;
        virtual void drawScrollDownButton(const rascUI::Location & location) override;
        virtual void drawFadePanel(const rascUI::Location & location) override;
    };
}

// --------------------------------------------------------------------

#ifdef _WIN32
    #define APIFUNC extern "C" __declspec(dllexport)
#else
    #define APIFUNC extern "C"
#endif

APIFUNC const char * getApiVersion(void);

APIFUNC rascUI::Theme * createTheme(void);

APIFUNC void deleteTheme(rascUI::Theme * theme);
