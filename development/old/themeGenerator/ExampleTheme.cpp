
#include "ExampleTheme.h"

#include <rascUI/util/Location.h>
#include <rascUI/util/Rectangle.h>
#include <rascUIxcb/main/WindowData.h>

namespace exampletheme {
    
    // ------------------ Custom theme implementation ----------------------
    
    ExampleTheme::ExampleTheme(void) :
        rascUI::Theme(1, 2, 3),
        data(NULL) {
    }
    ExampleTheme::~ExampleTheme(void) {
    }
    
    void ExampleTheme::init(void * userData) {
        Theme::init(userData);
        data = (rascUIxcb::WindowData *)userData;
    }
    void ExampleTheme::onChangeViewArea(const rascUI::Rectangle & view) {
    }
    void ExampleTheme::beforeDraw(void * userData) {
    }
    void ExampleTheme::afterDraw(void * userData, const rascUI::Rectangle & drawnArea) {
    }
    void ExampleTheme::drawBackPanel(const rascUI::Location & location) {
    }
    void ExampleTheme::drawFrontPanel(const rascUI::Location & location) {
    }
    void ExampleTheme::drawText(const rascUI::Location & location, const std::string & string) {
    }
    void ExampleTheme::drawButton(const rascUI::Location & location) {
    }
    void ExampleTheme::drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) {
    }
    void ExampleTheme::drawScrollBarBackground(const rascUI::Location & location) {
    }
    void ExampleTheme::drawScrollContentsBackground(const rascUI::Location & location) {
    }
    void ExampleTheme::drawScrollPuck(const rascUI::Location & location) {
    }
    void ExampleTheme::drawScrollUpButton(const rascUI::Location & location) {
    }
    void ExampleTheme::drawScrollDownButton(const rascUI::Location & location) {
    }
    void ExampleTheme::drawFadePanel(const rascUI::Location & location) {
    }
}

// --------------------------------------------------------------------

const char * getApiVersion(void) {
    return THEME_API_VERSION;
}

rascUI::Theme * createTheme(void) {
    return new exampletheme::ExampleTheme();
}

void deleteTheme(rascUI::Theme * theme) {
    delete theme;
}
