/*
 * SimpleXcbTheme.cpp
 *
 *  Created on: 20 Jun 2019
 *      Author: wilson
 */

#include "SimpleXcbTheme.h"

#include <cstddef>
#include <sys/types.h>
#include <xcb/xproto.h>
#include <cstdint>
#include <string>

#include <rascUI/util/Location.h>
#include <rascUIxcb/main/WindowData.h>

#define GET_XCB_RECT(oldRect)        \
    xcb_rectangle_t rect = {         \
        (int16_t)oldRect.x,          \
        (int16_t)oldRect.y,          \
        (uint16_t)oldRect.width,     \
        (uint16_t)oldRect.height     \
    };

#define DRAW_OUTLINED_RECT(rectangle, fillColour)                                                                 \
    (fillColour)->setForeground();                                                                  \
    xcb_poly_fill_rectangle(data->connection, data->drawable, data->graphics, 1, &(rectangle));                   \
    colourOutline.setForeground();                                                                  \
    {                                                                                                             \
        xcb_rectangle_t DRAW_OUTLINED_RECT_outlineRect = {                                                        \
            (rectangle).x,                                                                                        \
            (rectangle).y,                                                                                        \
            (uint16_t)((rectangle).width - 1),                                                                    \
            (uint16_t)((rectangle).height - 1)                                                                    \
        };                                                                                                        \
                                                                                                                  \
        xcb_poly_rectangle(data->connection, data->drawable, data->graphics, 1, &DRAW_OUTLINED_RECT_outlineRect); \
    }

#define TEXT_CHAR_WIDTH 7




namespace simplexcbtheme {

    SimpleXcbTheme::SimpleXcbTheme(void) :
        rascUI::Theme(
            18.0f,  // Normal component width   (universal button width, as of 0.42 only for scrollbar puck)
            24.0f,  // Normal component height  (universal button height value)
            4.0f    // UI border
        ),

        colourOutline         (0x3300, 0x3300, 0x3300),
        colourBack            (0xdd00, 0xdd00, 0xdd00),
        colourFront           (0xbb00, 0xbb00, 0xbb00),
        colourText            (0x0000, 0x0000, 0x0000),
        colourDefaultBg       (0x6300, 0xce00, 0xed00),
        colourTextEntry       (0xee00, 0xee00, 0xee00),
        colourScrollBackground(0x6200, 0x6800, 0x0600),

        colourButtonNormal    (0x9900, 0x9900, 0x9900),
        colourButtonMouseOver (0x7700, 0x7700, 0x9900),
        colourButtonMousePress(0x5900, 0x5900, 0x7700),
        colourButtonSelected  (0x6600, 0x6600, 0x8800),
        colourButtonInactive  (0xff00, 0xff00, 0xff00),

        font(0) {
    }

    SimpleXcbTheme::~SimpleXcbTheme(void) {
        if (hasInitBeenCalled()) {
            xcb_close_font(data->connection, font);
        }
    }

    rascUIxcb::Colour * SimpleXcbTheme::getButtonColour(rascUI::State state) {
        switch(state) {
            case rascUI::State::normal:     return &colourButtonNormal;
            case rascUI::State::mouseOver:  return &colourButtonMouseOver;
            case rascUI::State::mousePress: return &colourButtonMousePress;
            case rascUI::State::selected:   return &colourButtonSelected;
            default:                        return &colourButtonInactive;
        }
    }

    void SimpleXcbTheme::init(void * userData) {
        Theme::init(userData);
        data = (rascUIxcb::WindowData *)userData;

        colourOutline.setConnection(data);
        colourBack.setConnection(data);
        colourFront.setConnection(data);
        colourText.setConnection(data);
        colourDefaultBg.setConnection(data);
        colourTextEntry.setConnection(data);
        colourScrollBackground.setConnection(data);

        colourButtonNormal.setConnection(data);
        colourButtonMouseOver.setConnection(data);
        colourButtonMousePress.setConnection(data);
        colourButtonSelected.setConnection(data);
        colourButtonInactive.setConnection(data);

        font = xcb_generate_id(data->connection);
        std::string fontName = "7x13";
        xcb_open_font(data->connection, font, fontName.length(), fontName.c_str());

        uint32_t mask = XCB_GC_FONT;
        xcb_change_gc(data->connection, data->graphics, mask, &font);
    }

    void SimpleXcbTheme::onChangeViewArea(const rascUI::Rectangle & view) {
        GET_XCB_RECT(view)
        colourDefaultBg.setForeground();
        xcb_poly_fill_rectangle(data->connection, data->drawable, data->graphics, 1, &rect);
    }

    void SimpleXcbTheme::beforeDraw(void * userData) {
    }
    
    void SimpleXcbTheme::afterDraw(void * userData, const rascUI::Rectangle & drawnArea) {
    }

    void SimpleXcbTheme::drawBackPanel(const rascUI::Location & location) {
        GET_XCB_RECT(location.cache)
        DRAW_OUTLINED_RECT(rect, &colourBack)
    }

    void SimpleXcbTheme::drawFrontPanel(const rascUI::Location & location) {
        GET_XCB_RECT(location.cache)
        DRAW_OUTLINED_RECT(rect, &colourFront)
    }

    void SimpleXcbTheme::drawText(const rascUI::Location & location, const std::string & string) {
        uint8_t len = (uint8_t)(string.length() > 255 ? 255 : string.length());
        getButtonColour(location.getState())->setBackground();
        colourText.setForeground();
        int xOff, yOff;
        rascUI::State state = location.getState();
        if (state == rascUI::State::mousePress || state == rascUI::State::selected) {
            xOff = 5; yOff = 17;
        } else {
            xOff = 4; yOff = 16;
        }
        xcb_image_text_8(data->connection, len, data->drawable, data->graphics, location.cache.x + xOff, location.cache.y + yOff, string.c_str());
    }

    void SimpleXcbTheme::drawButton(const rascUI::Location & location) {
        GET_XCB_RECT(location.cache)
        DRAW_OUTLINED_RECT(rect, getButtonColour(location.getState()))
    }

    void SimpleXcbTheme::drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) {
        GET_XCB_RECT(location.cache)
        rascUIxcb::Colour * colour = getButtonColour(location.getState());
        DRAW_OUTLINED_RECT(rect, colour)

        rect.x += 4; rect.y += 4; rect.width -= 8; rect.height -= 8;
        colour = &colourTextEntry;
        DRAW_OUTLINED_RECT(rect, colour)

        colour->setBackground();
        colourText.setForeground();

        uint8_t len = (uint8_t)(string.length() > 255 ? 255 : string.length());
        int xOff, yOff;
        rascUI::State state = location.getState();
        if (state == rascUI::State::mousePress || state == rascUI::State::selected) {
            xOff = 4; yOff = 14;
        } else {
            xOff = 3; yOff = 13;
        }
        xcb_image_text_8(data->connection, len, data->drawable, data->graphics, rect.x + xOff, rect.y + yOff, string.c_str());

        if (drawCursor) {
            rect.x += TEXT_CHAR_WIDTH * cursorPos + 3;
            rect.y += 2; rect.height -= 4;
            rect.width = 2;
            xcb_poly_fill_rectangle(data->connection, data->drawable, data->graphics, 1, &rect);
        }
    }

    void SimpleXcbTheme::drawScrollBarBackground(const rascUI::Location & location) {
        drawBackPanel(location);
    }

    void SimpleXcbTheme::drawScrollContentsBackground(const rascUI::Location & location) {
        GET_XCB_RECT(location.cache)
        DRAW_OUTLINED_RECT(rect, &colourScrollBackground)
    }

    void SimpleXcbTheme::drawScrollPuck(const rascUI::Location & location) {
        drawButton(location);
    }

    void SimpleXcbTheme::drawScrollUpButton(const rascUI::Location & location) {
        drawButton(location);
    }

    void SimpleXcbTheme::drawScrollDownButton(const rascUI::Location & location) {
        drawButton(location);
    }

    void SimpleXcbTheme::drawFadePanel(const rascUI::Location & location) {
    }
}

// --------------------------------------------------------------------

const char * getApiVersion(void) {
    return THEME_API_VERSION;
}

rascUI::Theme * createTheme(void) {
    return new simplexcbtheme::SimpleXcbTheme();
}

void deleteTheme(rascUI::Theme * theme) {
    delete theme;
}
