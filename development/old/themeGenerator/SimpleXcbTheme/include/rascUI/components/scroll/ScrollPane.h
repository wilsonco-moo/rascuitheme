/*
 * ScrollPane.h
 *
 *  Created on: 21 Dec 2018
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_SCROLL_SCROLLPANE_H_
#define RASCUI_COMPONENTS_SCROLL_SCROLLPANE_H_

#include <GL/gl.h>

#include "../../base/Container.h"
#include "../../components/abstract/ResponsiveComponent.h"
#include "SegmentContainer.h"

namespace rascUI {

    class Theme;


    /**
     * ScrollPane is a class which automates the use of a segment container.
     *
     * DO NOT ADD COMPONENTS DIRECTLY TO A ScrollPane. Instead add the components to the contents
     * field of the ScrollPane. Components added directly to the ScrollPane WILL NOT SCROLL.
     *
     * NOTE: The bounded area (for partial repainting) of ScrollPane is larger by segmentHeight at the
     *       top and bottom, than the ScrollPane's location. Any parent container MUST allow for this area
     *       above and below the ScrollPane, or partial repainting will not work correctly.
     */
    class ScrollPane : public Container {
    private:
        // ------------------- ScrollPaneSegmentContainer class -----------------------------

        /**
         * This is a custom subclass of SegmentContainer used by ScrollPane.
         * This allows the keyboard events to be correctly handled by the scroll pane.
         */
        class ScrollPaneSegmentContainer : public SegmentContainer {
        private:
            friend class ScrollPane;
            friend class ScrollPanePuck;
            ScrollPane * scrollPane;
            GLfloat targetSegmentPos;
        public:
            ScrollPaneSegmentContainer(Location location, bool drawBackground, GLfloat segmentHeight, ScrollPane * scrollPane);
            virtual ~ScrollPaneSegmentContainer(void);
        private:
            // This MUST BE called at the start of any call to onDraw or boundedRepaint.
            // This updates the segment position of the scroll pane, to carry out any smooth animation, then sets the appropriate components to recalculate and repaint.
            void updateSegmentPosFromTarget(void);
        protected:
            virtual void onChildKeyboardSelect(Component * component) override;
            virtual void boundedRepaint(Rectangle & boundingBox, std::unordered_set<Component *> & wantsRepaint, std::unordered_set<Container *> & childWantsRepaint) override;
            virtual void onDraw(void) override;
        };

        // ---------------------------- ScrollPanePuck class -------------------------------

        class ScrollPanePuck : public ResponsiveComponent {
        private:
            friend class ScrollPaneSegmentContainer;
            ScrollPane * scrollPane;
            GLfloat margin, minHeight;
            GLfloat initialMouseY, initialSegmentPos;
        public:
            ScrollPanePuck(ScrollPane * scrollPane, Theme * theme);
            virtual ~ScrollPanePuck(void);
        protected:
            virtual void onDraw(void) override;
            virtual void recalculate(const Rectangle & parentSize) override;
            virtual void onMousePress(GLfloat viewX, GLfloat viewY, int button) override;
            virtual void onMouseDrag(GLfloat viewX, GLfloat viewY, int button) override;
            virtual void onMouseRelease(GLfloat viewX, GLfloat viewY, int button) override;
            virtual NavigationAction getRequiredKeyboardActionPress(NavigationAction action) override;
            virtual NavigationAction getRequiredKeyboardActionRelease(NavigationAction action) override;
            virtual NavigationAction onSelectionLockKeyPress(NavigationAction defaultAction, int key, bool special) override;
        };

        // ---------------------------- Scroll background class ----------------------------

        class ScrollBackgroundPanel : public Component {
        public:
            ScrollBackgroundPanel(Location location = Location());
            virtual ~ScrollBackgroundPanel(void);
        protected:
            virtual void onDraw(void) override;
            virtual bool shouldRespondToKeyboard(void) const override;
        };

        // --------------------------- Scroll up button class ------------------------------

        class ScrollUpButton : public ResponsiveComponent {
        private:
            ScrollPane * scrollPane;
        public:
            ScrollUpButton(Location location, ScrollPane * scrollPane);
            virtual ~ScrollUpButton(void);
        protected:
            virtual void onDraw(void) override;
            virtual void onMouseClick(GLfloat viewX, GLfloat viewY, int button) override;
        };

        // --------------------------- Scroll Down button class ----------------------------

        class ScrollDownButton : public ResponsiveComponent {
        private:
            ScrollPane * scrollPane;
        public:
            ScrollDownButton(Location location, ScrollPane * scrollPane);
            virtual ~ScrollDownButton(void);
        protected:
            virtual void onDraw(void) override;
            virtual void onMouseClick(GLfloat viewX, GLfloat viewY, int button) override;
        };

        // ---------------------------------------------------------------------------------

        // All scroll bar components are in this sub-container.
        Container scrollbarContainer;
        // This is shown behind the buttons and the scroll bar.
        ScrollBackgroundPanel backPanel;
        // These are the buttons to scroll up and down.
        ScrollUpButton   scrollUpButton;
        ScrollDownButton scrollDownButton;
        // The scroll puck
        ScrollPanePuck puck;

    public:

        /**
         * The SegmentContainer that users of the ScrollPane should add components to.
         */
        ScrollPaneSegmentContainer contents;

        /**
         * This is the speed at which smooth scrolling motion should happen. This is measured as:
         *  > The proportion by which the scroll pane will approach its destination, per second.
         * So the difference between the actual position, and the target position is multiplied by
         * smoothMoveSpeed each second. (This is done each frame, using the elapsed time and some maths).
         *
         * smoothMoveSpeed = 0
         *   > This will mean that all scrolling is done instantly, with no animation.
         *
         * Small values of smoothMoveSpeed
         *   > This will result in very fast scrolling animations.
         *
         * Large values of smoothMoveSpeed
         *   > This will result in very slow scrolling animations.
         *
         * smoothMoveSpeed = 1
         *   > No scrolling will ever occur? Is this actually useful?
         *
         * By default this is 0.05.
         */
        GLfloat smoothMoveSpeed;

        /**
         * By default, drawBackground is true. If the user wants a ScrollPane which does not draw a scroll
         * contents background, then they must specify false.
         *
         * By default, the segment height is COUNT_ONEBORDER_Y(1),  (i.e: the normal component height plus one border).
         */
        ScrollPane(Theme * theme, Location location = Location(), bool drawBackground = true);
        ScrollPane(Theme * theme, Location location, GLfloat segmentHeight);
        ScrollPane(Theme * theme, Location location, bool drawBackground, GLfloat segmentHeight);
        virtual ~ScrollPane(void);

    protected:
        // This is used for mouse scroll wheel scrolling. It does not matter if this is a press or a release
        // event, since a release event always immediately follows from a scroll wheel button press event.
        virtual void onMouseRelease(GLfloat viewX, GLfloat viewY, int button) override;

        // Here we return an area, which has segmentHeight added to the top and bottom.
        virtual Rectangle getBoundedArea(void) const override;

    private:
        void scrollUp(unsigned int amount);
        void scrollDown(unsigned int amount);
    };

}

#endif
