/*
 * SegmentContainer.h
 *
 *  Created on: 21 Dec 2018
 *      Author: wilson
 */

#ifndef RASCUI_COMPONENTS_SCROLL_SEGMENTCONTAINER_H_
#define RASCUI_COMPONENTS_SCROLL_SEGMENTCONTAINER_H_

#include <GL/gl.h>
#include <stddef.h>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "../../base/Container.h"

namespace rascUI {

    class Rectangle;


    /**
     * The SegmentContainer is a specialised type of container, which allows many components
     * to be arranged vertically, in such a way that only the ones within the visible area are actually drawn.
     *
     * NOTE: Removing components from the middle of a SegmentContainer can be very expensive, as all components
     *       after it would have to be moved backwards in our internal data structures.
     *
     * NOTE: The bounded area (for partial repainting) of SegmentContainer is larger by segmentHeight at the
     *       top and bottom, than the SegmentContainer's location. Any parent container MUST allow for this area
     *       above and below the SegmentContainer, or partial repainting will not work correctly.
     */
    class SegmentContainer : public Container {
    private:
        // The height of segments in this container. By default this is set to 32.
        GLfloat segmentHeight;
        // The current position that the top of the container is, within the segments. By default this is set to zero.
        // This value is measured in multiples of segments.
        GLfloat segmentPos;
        // This vector stores our child components by their segment position.
        std::vector<Component *> componentsBySegmentPos;
        // This map stores the iterator in the previous std::vector, for each component. This allows us to remove
        // components by pointer easily.
        std::unordered_map<Component *, size_t> componentPositions;

        // These values cache the first component to draw, and the number of components to draw, within the current
        // visible range. These are assigned during the recalculate method.
        size_t firstSegmentCache, // Stores the segment id we should start iterating from
               lastSegmentCache;  // Stores the segment id one after the one we should iterate up to.

        // These values cache the range of segments which are fully visible. This is used by the keyboard navigation.
        size_t firstFullyVisibleSegmentCache, // Stores the segment id we should start iterating from
               lastFullyVisibleSegmentCache;  // Stores the segment id one after the one we should iterate up to.

        // This stores whether we should draw a background behind our components.
        bool drawBackground;

    public:
        SegmentContainer(Location location, GLfloat segmentHeight, bool drawBackground);
        virtual ~SegmentContainer(void);

        /**
         * Sets the segment position. This will trigger a recalculation of the currently visible child components,
         * and a repaint of the entire segment container.
         */
        void setSegmentPos(GLfloat segmentPos);

        /**
         * Returns the current segment position.
         */
        inline GLfloat getSegmentPos(void) const {
            return segmentPos;
        }

        /**
         * Returns the segment height that is in use.
         */
        inline GLfloat getSegmentHeight(void) const {
            return segmentHeight;
        }

        /**
         * This returns the maximum segment position which should be used, (before we scroll past the bottom),
         * with the current number of components.
         * This represents the segment position if we placed the bottom of our view at the bottom of the
         * last component. If there are fewer components than the height of the screen, this value will
         * be clipped to zero.
         */
        GLfloat getMaximumSegmentPos(void) const;

        /**
         * This method gets the nearest segmentPos to the current segmentPos, for the specified
         * segment id to be completely in view. This should be used to control scrolling upon keyboard
         * selection.
         */
        GLfloat getNearestSegmentPos(size_t segmentId);

        /**
         * Returns the segment id of the specified component, assuming the component is a child component
         * of this SegmentContainer. If it is not, this method will cause undefined behaviour.
         */
        inline size_t getChildSegmentId(Component * component) const {
            return componentPositions.at(component);
        }

    private:
        // Used internally for recalculation. This also automatically causes a repaint of the entire SegmentContainer.
        void internalRecalculate(void);

    protected:
        // When components are added, we must add them to our internal data structures.
        virtual void onAdd(Component * component) override;
        // When components are removed, we must remove them from our internal data structures.
        virtual void onRemove(Component * component) override;

        // Here we only recalculate the components that are currently visible. This is done efficiently,
        // in time proportional to the number of VISIBLE components, NOT the total number of components.
        // even if we have thousands of components.
        virtual void recalculate(const Rectangle & parentSize) override;

        // Here we only draw the components that are currently visible. This is done efficiently,
        // in time proportional to the number of VISIBLE components, NOT the total number of components.
        virtual void onDraw(void) override;

        // Here we return an area, which has segmentHeight added to the top and bottom.
        virtual Rectangle getBoundedArea(void) const override;

        // Here we modify Container's boundedRepaint method so we only actually draw components which are visible.
        virtual void boundedRepaint(Rectangle & boundingBox, std::unordered_set<Component *> & wantsRepaint, std::unordered_set<Container *> & childWantsRepaint) override;

        // To avoid components which have fallen outside the visible range from receiving mouse events,
        // here we only add those child components which are currently visible.
        virtual void addIfMouseOver(GLfloat viewX, GLfloat viewY, std::unordered_set<Component *> * mouseOver) override;


    public:
        // --------------------------------- Keyboard navigation --------------------------------------

        // Override these methods, so that when navigating using the keyboard, we do not always pick the component
        // at the top, then cause any ScrollPane to scroll to the top.

        // These methods modify the original methods by first searching the visible segment area for components.
        // If that fails, the equivalent superclass method is then called, to search the rest of the area.

        virtual Component * getFirstKeyboardResponsiveComponent(void) const override;
        virtual Component * getFirstKeyboardResponsiveComponentDepth(void) const override;
        virtual Component * getFirstKeyboardResponsiveComponentNonDepth(void) const override;
        virtual Component * getLastKeyboardResponsiveComponent(void) const override;
        virtual Component * getLastKeyboardResponsiveComponentDepth(void) const override;
        virtual Component * getLastKeyboardResponsiveComponentNonDepth(void) const override;
    };

}

#endif
