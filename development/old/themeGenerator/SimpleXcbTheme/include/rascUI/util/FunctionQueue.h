/*
 * FunctionQueue.h
 *
 *  Created on: 13 Dec 2018
 *      Author: wilson
 */

#ifndef RASCUI_UTIL_FUNCTIONQUEUE_H_
#define RASCUI_UTIL_FUNCTIONQUEUE_H_

#include <functional>

namespace rascUI {

    /**
     * This class represents a queue of events (as std::functions), which can be queued, then executed when necessary.
     * This is useful for delaying actions until a time where it is possible to execute them, such as:
     *   > Avoiding modifying the component hierarchy of a top level container while it is being drawn
     *   > Getting around situations which would otherwise cause deadlock.
     * 
     * This class also provides functionality to run events after a specified amount of time, which can optionally
     * repeat every specified time interval.
     * 
     * To add an event who's execution is delayed, a token is required. When a token is deleted, it is guaranteed that
     * no event which was assigned to this token, will every be started again. It is even possible for a timed job,
     * assigned to a specific token, to delete it's own token (or any other token), from the event itself. This way,
     * a timed repeating event is able to cancel itself, meaning that it will never repeat again.
     *
     * NOTE: Each function queue contains a mutex, to make all operations thread-safe.
     *       BUT, the user provided functions, provided to the FunctionQueue, are NOT executed within this mutex.
     */
    class FunctionQueue {
    private:
    
        /**
         * Classes used internally, and the data we hold internally. These are defined in FunctionQueue.cpp, as to
         * avoid #including many many standard headers,
         * (<list>, <mutex>, <set>, <unordered_set>, <chrono>, <iostream>, ... etc), from this simple header file.
         */
        class InternalData; class Token; class Job;
        InternalData * data;

        /**
         * Function queues cannot be copied: We hold raw resources.
         */
        FunctionQueue(const FunctionQueue & other);
        FunctionQueue & operator = (const FunctionQueue & other);

    public:
        FunctionQueue(void);
        virtual ~FunctionQueue(void);

        /**
         * Adds a function to the queue. This will be executed next time update is called.
         * No timing is associated with this function, and it will never repeat.
         * In any given update cycle, waiting normal (untimed) functions are always executed before
         * any timed functions.
         *
         * NOTE: Any untimed functions, (or timed functions with a timeUntilStart of 0), added from within a timed or untimed
         *       function, will always be executed in the same update cycle, (unless it's respective token is deleted).
         */
        void addFunction(std::function<void(void)> func);

        /**
         * Adds a timed function to the queue. To do this, a token must be used.
         * The time until the function starts, (in milliseconds), must be defined.
         * An interval can also be specified. If an interval greater than zero is specified, the job will repeat every <interval> milliseconds.
         *
         * TIMING ACCURACY IS NOT GUARANTEED, depending on the outside implementation.
         * BUT, timing consistency is guaranteed. For example, if time is set to 1000ms (1 second):
         *  > It is NOT GUARANTEED that this will be run precisely on the mark of each second.
         *  > BUT, it IS guaranteed that in an N second window, the function WILL be run precisely
         *    N times. On average, it WILL be called exactly once per second.
         *
         * NOTE: If functions with the same time until start are added, the order in which they will
         *       be executed is UNDEFINED.
         * NOTE: A timed (possibly repeating) function is able to delete it's own token, from within the
         *       execution of the event, to cancel itself.
         * NOTE: In a given update cycle, any waiting untimed functions are always executed before any waiting timed functions.
         * NOTE: Any untimed functions, (or timed functions with a timeUntilStart of 0), added from within a timed or untimed
         *       function, will always be executed in the same update cycle, (unless it's respective token is deleted).
         */
        inline void addTimedFunction(void * token, unsigned long long timeUntilStart, std::function<void(void)> func) {
            addTimedRepeatingFunction(token, timeUntilStart, 0, func);
        }
        void addTimedRepeatingFunction(void * token, unsigned long long timeUntilStart, unsigned long long interval, std::function<void(void)> func);

        /**
         * Runs all functions waiting in the queue, including timed functions, as appropriate.
         */
        void update(void);

        /**
         * Creates a token, to allow the caller to use timed functions.
         */
        void * createToken(void);

        /**
         * Deletes the token, and cancels any waiting timed functions related to this token.
         * After this call, no timed functions related to this token will ever be run again.
         * This MUST ONLY EVER BE CALLED ONCE, for a particular token generated by createToken,
         * or undefined behaviour will result.
         *
         * The user does not actually have to keep a pointer to the FunctionQueue, since the token
         * contains all the required information internally. As a result, deleting a token requires
         * only a static method.
         *
         * NOTE: It is perfectly fine for a timed job to delete it's own token from w
         *
         * This should be accessed with FunctionQueue::deleteToken(token).
         */
        static void deleteToken(void * token);

        /**
         * If we have any waiting timed functions, hasWaiting should be set to true. Otherwise, it's value won't be changed.
         * If we have any timed functions, due to start within less than minTimeUntil, then minTimeUntil will be set to
         * the time until they start.
         */
        void hasWaitingTimedFunctions(unsigned long long timeNow, unsigned long long * minTimeUntil, bool * hasWaiting) const;

        /**
         * If not on Windows, a file descriptor (preferably a pipe) can be provided to the function queue.
         * Each time a new function (untimed or timed) is added to the queue, a single byte will be written to this.
         * This allows the function queue to interrupt a call to pselect or similar, each time a new event is added
         * to the queue. This is used in rascUIxcb, so an external thread can interrupt the waiting of the main UI loop.
         *
         * NOTE: This file descriptor (pipe) MUST BE NON-BLOCKING.
         */
        #ifndef _WIN32
            void setUpdateFileDescriptor(int fileDescriptor);
        #endif

        // ----------------------- Function queue updater ----------------

        /**
         * This class will update the given function queue, when this instance is destroyed.
         * This is useful for example, if you want a function queue to be updated as the last thing in a method,
         * or after a mutex has been unlocked.
         *
         * NULL can be provided as the Function Queue, which will mean nothing will happen.
         */
        class FunctionQueueUpdater {
        private:
            FunctionQueue * queue;
        public:
            FunctionQueueUpdater(FunctionQueue * queue);
            virtual ~FunctionQueueUpdater(void);
        };
    };
}

#endif
