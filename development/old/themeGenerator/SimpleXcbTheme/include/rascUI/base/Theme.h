/*
 * Theme.h
 *
 *  Created on: 1 Oct 2018
 *      Author: wilson
 */

#ifndef RASCUI_BASE_THEME_H_
#define RASCUI_BASE_THEME_H_

#include <GL/gl.h>
#include <cstddef>
#include <string>

namespace rascUI {

    class Location;
    class Rectangle;

    /**
     * An instance of Theme should be stored in each TopLevelContainer instance.
     *
     *  > The Theme instance is used to pick how to draw each of the components in the TopLevelContainer. These methods are
     *    bundled into such a class to allow easy changing between UI styles. A new Theme implementation is all that is required
     *    to completely change the look and feel of the UI.
     *
     *  > Theme has a series of abstract methods, each of which should draw a specific type of component. Each method provides as
     *    parameters two things.
     *    - A Location instance. This should be used to work out the position to draw the specific type of Component.
     *    - Other data relevant to that particular type of component, such as the text to draw in a label.
     *    Note: No data about how to draw the component, or the colours to draw the component is provided. This allows the Theme
     *    implementation complete control over the drawing process, allowing for consistent UI.
     *
     *  > The Location parameter in each of these methods contains a special field: state. This is an enum class defined in State.h.
     *    This should affect how each of the component implementations draws, such as a state of State::mouseOver should cause the
     *    Theme implementation to draw the component more highlighted. It is entirely up to the implementation of the Theme subclass
     *    to decide how to highlight, (or even whether to bother).
     *
     *  > NOTE: The Theme implementation does not have to worry about the state State::invisible. Since State::invisible is checked in
     *    the Component and Container classes, we will never receive any draw requests from a component who's state is invisible.
     *    The invisible state causes the Component class not to even call any draw methods.
     *
     * See enum class State in State.h for more information about what the Theme implementation should do for each state.
     *
     * NOTE: Theme is a header-only interface, and no longer has an attached source file. This is so that
     *       dynamically loaded Theme implementations do not actually have to link to rascUI.
     */
    class Theme {
    private:
        // Set to true once init has been called.
        bool initCalled;

    public:

        /**
         * This should be updated each time methods are added to Theme, or methods are modified.
         * Only dynamically loaded themes which match this API version should be used.
         */
        #define THEME_API_VERSION "theme-rascUI-0.1"

        /**
         * These should contain the preferred minimum width and height for components using this theme.
         *
         * The height should be used as the minimum for any component which contains or draws text, such as a button,
         * toggle button, text entry box etc.
         *
         * The width should be used as the minimum comfortable usable width for a button. This is used for example,
         * for buttons in the scroll pane.
         *
         * Both of these values are used by the layout system, if it uses a theme.
         */
        const GLfloat normalComponentWidth, normalComponentHeight;

        /**
         * This should contain the preferred minimum distance to place between two components (such as buttons), which
         * are using this theme. It is perfectly acceptable for this to contain a value of zero, if the theme is designed
         * such that no border is needed.
         *
         * This is used extensively by the layout system, and is probably the most important value which the
         * theme must define.
         */
        const GLfloat uiBorder;

        /**
         * The main constructor of Theme. Here the layout options should be provided.
         */
        Theme(GLfloat normalComponentWidth, GLfloat normalComponentHeight, GLfloat uiBorder) :
            initCalled(false),
            normalComponentWidth(normalComponentWidth),
            normalComponentHeight(normalComponentHeight),
            uiBorder(uiBorder) {
        }
        virtual ~Theme(void) {
        }

        /**
         * This returns true once init has been called.
         * This is convenient for use in the destructor of a Theme.
         */
        inline bool hasInitBeenCalled(void) const {
            return initCalled;
        }

        /**
         * This is called ONCE at the start, BEFORE all calls to onChangeViewArea, beforeDraw
         * and afterDraw. This uses the same userData as beforeDraw.
         *
         * Depending on the Theme implementation, carrying out actions here may not be necessary.
         * As a result it is optional to implement this.
         *
         * Optionally, a void pointer of user data can be sent to the theme. This comes from the
         * protected "beforeDrawAndInitThemeUserData" field of TopLevelContainer. Here, subclasses
         * are able to send user data to the theme implementation.
         *
         * NOTE: The superclass method (Theme::init) MUST BE CALLED ALSO BY ANY CLASS OVERRIDING
         *       THIS METHOD. This is because it sets the initCalled variable, for the convenience
         *       of the user.
         */
        virtual void init(void * userData) {
            initCalled = true;
        }

        /**
         * This should be called each time the view area changes, (at this point a TopLevelContainer
         * should also call a recalculation of all components in the hierarchy).
         *
         * This MUST be called AFTER init, but BEFORE beforeDraw and afterDraw.
         *
         * Depending on the Theme implementation, carrying out actions here may not be necessary.
         * As a result it is optional to implement this.
         */
        virtual void onChangeViewArea(const Rectangle & view) {
        }

        /**
         * This is called each time a sequence of drawing takes place, before anything is drawn,
         * after init and onChangeViewArea.
         *
         * Depending on the Theme implementation, carrying out actions here may not be necessary.
         * As a result it is optional to implement this.
         *
         * Optionally, a void pointer of user data can be sent to the theme. This comes from the
         * protected "beforeDrawAndInitThemeUserData" field of TopLevelContainer. Here, subclasses
         * are able to send user data to the theme implementation.
         */
        virtual void beforeDraw(void * userData) {
        }

        /**
         * This is called each time a sequence of drawing takes place, after everything is drawn.
         * Depending on the Theme implementation, carrying out actions here may not be necessary.
         * As a result it is optional to implement this.
         *
         * Optionally, a void pointer of user data can be sent to the theme. This comes from the
         * protected "afterDrawThemeUserData" field of TopLevelContainer. Here, subclasses
         * are able to send user data to the theme implementation.
         *
         * A Rectangle should be provided to this method, which represents the area on-screen which
         * has changed as a result of this drawing operation.
         * In partial repainting mode, this should be the final bounding box after drawing.
         * Otherwise, this should just be the TopLevelContainer's view rectangle.
         */
        virtual void afterDraw(void * userData, const Rectangle & drawnArea) {
        }


        /**
         * This method should draw a rectangular box, for use as the background to a menu, or similar.
         *
         * A FrontPanel component by default does not respond to the mouse, but the theme could respond to mouse
         * states anyway. That would allow FrontPanels to appear selected.
         */
        virtual void drawBackPanel(const Location & location) = 0;

        /**
         * This should draw a rectangular box similar to what is drawn with drawBackPanel. FrontPanels are components
         * that are put in front of a BackPanel, to show a UI section, such as related buttons, or a non-interactive
         * piece of text. A FrontPanel should appear distinct to a button, and look like it is not clickable.
         *
         * A FrontPanel component by default does not respond to the mouse, but the theme could respond to mouse
         * states anyway. That would allow FrontPanels to appear selected.
         */
        virtual void drawFrontPanel(const Location & location) = 0;

        /**
         * This method should draw a piece of text, within the specified rectangle.
         * Text could also respond to mouse, to allow a text label to be manually highlighted within the UI.
         */
        virtual void drawText(const Location & location, const std::string & string) = 0;

        /**
         * This method should draw some text in front of a rectangle, to form a button. This definitely should respond to
         * the mouse, to provide a suitable amount of feedback from the user's interaction.
         */
        virtual void drawButton(const Location & location) = 0;

        /**
         * This should draw a text entry box, with the specified location, text and cursor position. The text entry box
         * should respond to different states, so it is possible for the user to be able to tell if the text entry box
         * is currently being edited.
         *
         * The cursor MUST only be drawn if drawCursor is true, so:
         *  > Cursor blinking works correctly
         *  > The cursor is not drawn if the user is not currently editing this text entry box.
         */
        virtual void drawTextEntryBox(const Location & location, const std::string & string, size_t cursorPos, bool drawCursor) = 0;

        /**
         * This should draw the background panel for a (vertical) scroll bar. It is not necessary, (or
         * particularly useful), for this to respond to different states in the location. Here, it is
         * completely acceptable for a Theme implementation to draw exactly the same as a back panel.
         *
         * Note: This will always have a width of normalComponentWidth + 2 * uiBorder.
         */
        virtual void drawScrollBarBackground(const Location & location) = 0;

        /**
         * This should draw the background panel for a scroll pane, i.e: What is displayed behind the components
         * which are scrolling. It is not necessary, (or particularly useful), for this to respond to different
         * states in the location. Here, it is completely acceptable for a Theme implementation to draw exactly
         * the same as a back panel.
         */
        virtual void drawScrollContentsBackground(const Location & location) = 0;

        /**
         * This should draw the puck for a (vertical) scroll bar. This must respond to all states, like
         * a button. This method is here for fine customisation, it is acceptable for a theme implementation
         * to simply run drawButton here, to make the scroll puck look exactly like a button.
         *
         * Note: This will always have a width of normalComponentWidth.
         */
        virtual void drawScrollPuck(const Location & location) = 0;

        /**
         * This should draw the "scroll up" button, displayed on scroll bars.
         * It is possible (and preferable) for the theme implementation to simply call drawButton, then draw an
         * extra symbol on top.
         * The scroll up and down buttons must be visually distinct from each other, and must respond to mouse
         * states like a button should.
         *
         * Note: This will always have a width of normalComponentWidth, and a height of normalComponentHeight.
         */
        virtual void drawScrollUpButton(const Location & location) = 0;

        /**
         * This should draw the "scroll down" button, displayed on scroll bars.
         * It is possible (and preferable) for the theme implementation to simply call drawButton, then draw an
         * extra symbol on top.
         * The scroll up and down buttons must be visually distinct from each other, and must respond to mouse
         * states like a button should.
         *
         * Note: This will always have a width of normalComponentWidth, and a height of normalComponentHeight.
         */
        virtual void drawScrollDownButton(const Location & location) = 0;

        /**
         * This should draw a semi-transparent box, for use of fading components behind a popup menu.
         *
         * The panel should be drawn semi-transparent, enough to fade-out components behind it, but not too
         * opaque as to obscure them completely.
         *
         * It is not necessary for the theme to respond to states here.
         */
        virtual void drawFadePanel(const Location & location) = 0;
    };

}

#endif
