/*
 * ThemeGroup.h
 *
 *  Created on: 1 Jul 2019
 *      Author: wilson
 */

#ifndef SRC_RASCUI_BASE_THEMEGROUP_H_
#define SRC_RASCUI_BASE_THEMEGROUP_H_

#include <vector>
#include "Theme.h"

namespace rascUI {

    /**
     * A ThemeGroup allows multiple theme implementations to be grouped together.
     * For any method called in the ThemeGroup, the same respective method
     * is called for all of the ThemeGroup's child Theme instances.
     *
     * The methods are called in order that the child theme instances were added to the ThemeGroup,
     * WITH THE EXCEPTION OF THE afterDraw METHOD, WHERE the methods are called
     * in REVERSE order that the instances were added to the group.
     *
     * This allows, for example, a base Theme implementation and a secondary Theme implementation
     * to be linked together. The first one could do common actions like performing double
     * buffering, and the second could just be a pure Theme implementation. This would allow
     * the second one to be swapped out, without duplicating the code in the base theme.
     */
    class ThemeGroup : public rascUI::Theme {
    private:
        std::vector<Theme *> themes;
    public:

        /**
         * There are two available constructors. One allows the user to manually set normalComponentWidth,
         * normalComponentHeight and uiBorder. The other will set these values from a provided template
         * theme. NOTE that this template theme will NOT be automatically added to our internal list of themes.
         *
         * These normalComponentWidth, normalComponentHeight and uiBorder ARE USED for the ThemeGroup,
         * NOT the respective values of any of our child instances.
         */
        ThemeGroup(GLfloat normalComponentWidth, GLfloat normalComponentHeight, GLfloat uiBorder);
        ThemeGroup(Theme * templateTheme);
        virtual ~ThemeGroup(void);

        /**
         * Adds a child theme to this group.
         *  The methods are called in order that the child theme instances were added to the ThemeGroup,
         * WITH THE EXCEPTION OF THE afterDraw METHOD, WHERE the methods are called
         * in REVERSE order that the instances were added to the group.
         */
        void addTheme(Theme * theme);


        virtual void init(void * userData) override;
        virtual void onChangeViewArea(const Rectangle & view) override;
        virtual void beforeDraw(void * userData) override;
        virtual void afterDraw(void * userData, const Rectangle & drawnArea) override;
        virtual void drawBackPanel(const Location & location) override;
        virtual void drawFrontPanel(const Location & location) override;
        virtual void drawText(const Location & location, const std::string & string) override;
        virtual void drawButton(const Location & location) override;
        virtual void drawTextEntryBox(const Location & location, const std::string & string, size_t cursorPos, bool drawCursor) override;
        virtual void drawScrollBarBackground(const Location & location) override;
        virtual void drawScrollContentsBackground(const Location & location) override;
        virtual void drawScrollPuck(const Location & location) override;
        virtual void drawScrollUpButton(const Location & location) override;
        virtual void drawScrollDownButton(const Location & location) override;
        virtual void drawFadePanel(const Location & location) override;
    };
}

#endif
