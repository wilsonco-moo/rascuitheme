/*
 * SimpleXcbTheme.h
 *
 *  Created on: 20 Jun 2019
 *      Author: wilson
 */

#ifndef RASCUIXCB_THEME_SIMPLEXCBTHEME_H_
#define RASCUIXCB_THEME_SIMPLEXCBTHEME_H_

#include <rascUI/util/State.h>
#include "../util/Colour.h"
#include "XcbThemeBase.h"

namespace rascUIxcb {

    /**
     * This is a simple, (but complete) implementation of XcbThemeBase.
     */
    class SimpleXcbTheme : public XcbThemeBase {
    private:
        // Colours
        Colour colourOutline, colourBack, colourFront, colourText, colourDefaultBg, colourTextEntry, colourScrollBackground,
               colourButtonNormal, colourButtonMouseOver, colourButtonMousePress, colourButtonSelected, colourButtonInactive;

        // Fonts
        xcb_font_t font;

    public:
        SimpleXcbTheme(bool enableDoubleBuffer);
        virtual ~SimpleXcbTheme(void);

    private:
        Colour * getButtonColour(rascUI::State state);
    public:

        virtual void init(void * userData) override;
        virtual void onChangeViewArea(const rascUI::Rectangle & view) override;
        virtual void drawBackPanel(const rascUI::Location & location) override;
        virtual void drawFrontPanel(const rascUI::Location & location) override;
        virtual void drawText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawButton(const rascUI::Location & location) override;
        virtual void drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) override;
        virtual void drawScrollBarBackground(const rascUI::Location & location) override;
        virtual void drawScrollContentsBackground(const rascUI::Location & location) override;
        virtual void drawScrollPuck(const rascUI::Location & location) override;
        virtual void drawScrollUpButton(const rascUI::Location & location) override;
        virtual void drawScrollDownButton(const rascUI::Location & location) override;
        virtual void drawFadePanel(const rascUI::Location & location) override;
    };

}

#endif
