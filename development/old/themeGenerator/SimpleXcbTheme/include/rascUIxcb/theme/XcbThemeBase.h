/*
 * XcbThemeBase.h
 *
 *  Created on: 7 Jun 2019
 *      Author: wilson
 */

#ifndef RASCUIXCB_THEME_XCBTHEMEBASE_H_
#define RASCUIXCB_THEME_XCBTHEMEBASE_H_

#include <cstddef>
#include <xcb/xcb.h>

#include <rascUI/base/Theme.h>
#include <rascUI/util/Rectangle.h>

namespace rascUIxcb {

    class Window;
    class Connection;
    class WindowData;

    /**
     * This is a base class for Themes in rascUIxcb. It automates all the required operations, like
     * extracting info about the connection, window and graphics from the user data,
     * and enabling (optional) double buffering.
     *
     * This does not (really?) work as a theme on its own. This is intended to either be used
     * as a base class for a theme, or be combined with another theme using a ThemeGroup.
     *
     * NOTE: XcbThemeBase instances MUST be destroyed before their respective Connection instances.
     */
    class XcbThemeBase : public rascUI::Theme {
    private:
        // Configuration
        bool doubleBufferEnabled,
             doubleBufferAllocated;    // Stores true if we have allocated a double buffer yet.
        rascUI::Rectangle view;        // This is stored so we can clip the draw area given by after draw.
        xcb_pixmap_t offScreenBuffer;  // This exists only if double buffering is enabled, otherwise will be zero.

    protected:

        // This is generated from the user data provided in the init method.
        WindowData * data;

    private:
        // Don't allow copying: We hold X resources, (namely the off screen buffer).
        XcbThemeBase(const XcbThemeBase & other);
        XcbThemeBase & operator = (const XcbThemeBase & other);

    public:
        /**
         * Here, the user can specify whether double buffering should be enabled.
         * This is set once for each instance of XcbThemeBase, and afterwards
         * cannot be changed.
         */
        XcbThemeBase(bool doubleBufferEnabled, GLfloat normalComponentWidth = 0.0f, GLfloat normalComponentHeight = 0.0f, GLfloat uiBorder = 0.0f);
        virtual ~XcbThemeBase(void);

    public:
        /**
         * This returns whether double buffering is enabled. This is set in the constructor.
         */
        inline bool isDoubleBufferEnabled(void) const { return doubleBufferEnabled; }

        // Methods we override to provide the required operations like double buffering.
        virtual void init(void * userData) override;
        virtual void onChangeViewArea(const rascUI::Rectangle & view) override;
        virtual void afterDraw(void * userData, const rascUI::Rectangle & drawnArea) override;

        // Methods we override and do nothing, as to allow use in a ThemeGroup.
        virtual void beforeDraw(void * userData) override;
        virtual void drawBackPanel(const rascUI::Location & location) override;
        virtual void drawFrontPanel(const rascUI::Location & location) override;
        virtual void drawText(const rascUI::Location & location, const std::string & string) override;
        virtual void drawButton(const rascUI::Location & location) override;
        virtual void drawTextEntryBox(const rascUI::Location & location, const std::string & string, size_t cursorPos, bool drawCursor) override;
        virtual void drawScrollBarBackground(const rascUI::Location & location) override;
        virtual void drawScrollContentsBackground(const rascUI::Location & location) override;
        virtual void drawScrollPuck(const rascUI::Location & location) override;
        virtual void drawScrollUpButton(const rascUI::Location & location) override;
        virtual void drawScrollDownButton(const rascUI::Location & location) override;
        virtual void drawFadePanel(const rascUI::Location & location) override;
    };
}

#endif
