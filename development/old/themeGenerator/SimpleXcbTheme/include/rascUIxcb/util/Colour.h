/*
 * Colour.h
 *
 *  Created on: 19 Jun 2019
 *      Author: wilson
 */

#ifndef RASCUIXCB_UTIL_COLOUR_H_
#define RASCUIXCB_UTIL_COLOUR_H_

#include <xcb/xcb.h>
#include <cstdint>

namespace rascUIxcb {

    class WindowData;

    /**
     * This class represents a colour allocated within an XCB colour map.
     * The colour in the colour map is freed automatically when this instance is destroyed.
     *
     * NOTE: All colour instances created from a given connection MUST
     * be destroyed BEFORE the Connection instance.
     *
     * NOTE: DO NOT INHERIT FROM THIS CLASS.
     */
    class Colour {
    private:
        enum class State {
            // We are storing our colour value literally: setConnection (or the appropriate
            // constructor) has not been called.
            NO_CONNECTION,
            // setConnection (or the appropriate constructor) has been called, but get has
            // not yet been run, so we are still waiting for a response from the X server.
            WAITING,
            // We have received our response from the X server.
            GOT_COLOUR
        };

        union {
            // Our internal colour. This is used in the NO_CONNECTION state.
            uint16_t channels[3];
            // Our response cookie. This is used in the WAITING state.
            xcb_alloc_color_cookie_t cookie;
            // Our final colour. This is used in the GOT_COLOUR state.
            uint32_t colour;
        };

        // Our current state.
        State state;

        // A WindowData instance. This includes a connection and a screen. We must keep a reference to this
        // so we can remove ourself from the colour map when no longer needed.
        WindowData * connection;

        // We can't allow copying, as then we would free our colour from the colour map
        // twice when we are destroyed.
        Colour(const Colour & other);
        Colour & operator = (const Colour & other);

    public:
        /**
         * Creates a colour without a connection. The setConnection method MUST
         * be run at some point later, before get is called.
         *
         * NOTE: Colour values are 16 bit (0-65535 inclusive), NOT 8 bit.
         */
        Colour(uint16_t red, uint16_t green, uint16_t blue);

        /**
         * Creates a colour with a connection by default. Here it will not be necessary
         * to call setConnection.
         * The connection MUST NOT BE NULL.
         *
         * NOTE: Colour values are 16 bit (0-65535 inclusive), NOT 8 bit.
         */
        Colour(uint16_t red, uint16_t green, uint16_t blue, WindowData * connection);

        ~Colour(void); // Don't make this virtual - it's not needed.

        /**
         * This allows the colour to lookup the correct value in the colour map.
         * The connection MUST NOT BE NULL.
         *
         * This MUST ONLY BE CALLED ONCE.
         */
        void setConnection(WindowData * connection);

        /**
         * setConnection (or the appropriate constructor) MUST have been called before this is run.
         */
        uint32_t get(void);

        /**
         * Sets the foreground and background colours of the graphics context, in our WindowData instance,
         * to this colour.
         */
        void setBackground(void);
        void setForeground(void);
    };

}

#endif
