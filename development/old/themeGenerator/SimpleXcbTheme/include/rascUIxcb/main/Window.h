/*
 * Window.h
 *
 *  Created on: 10 Jun 2019
 *      Author: wilson
 */

#ifndef RASCUIXCB_MAIN_WINDOW_H_
#define RASCUIXCB_MAIN_WINDOW_H_

#include <GL/gl.h>
#include <xcb/xcb.h>

#include <rascUI/base/TopLevelContainer.h>
#include "WindowData.h"

namespace rascUI {
    class Theme;
}


namespace rascUIxcb {

    class WindowConfig;
    class Connection;

    /**
     * This class encapsulates an XCB window.
     * The window is created when this class is created, but not automatically mapped.
     * To make the window visible, setMapped must be used.
     * The window is automatically destroyed when the Window instance is destroyed.
     *
     * With this class, currently only one window at a time is supported.
     */
    class Window : public rascUI::TopLevelContainer {
    private:
        rascUI::Rectangle windowDrawArea;
        bool mapped;

    public:

        Connection * connection;

        xcb_window_t window;
        xcb_gcontext_t graphics;

        // This is provided to Theme implementations.
        WindowData data;

        /**
         * The WindowConfig instance defines all the initial properties of the window.
         * It is not needed afterwards, so can be safely destroyed.
         *
         * Since Window is a subclass of rascUI::TopLevelContainer, we require a theme.
         * This Theme should ideally be a subclass of XcbThemeBase.
         *
         * X and Y scale pointers can optionally also be supplied.
         */
        Window(Connection * connection,
               const WindowConfig & config,
               rascUI::Theme * theme,
               const GLfloat * xScalePtr = NULL,
               const GLfloat * yScalePtr = NULL);

        virtual ~Window(void);

        /**
         * This returns the current draw area of the window.
         * This represents the current width and height of the window.
         */
        inline const rascUI::Rectangle & getWindowDrawArea(void) const {
            return windowDrawArea;
        }

        /**
         * Sets whether the current window should be mapped or not.
         * In essence, calling setMapped(true) makes the window visible,
         * and setMapped(false) makes the window invisible.
         */
        void setMapped(bool mapped);

        /**
         * Returns whether the window is currently mapped or not.
         */
        inline bool isMapped(void) const {
            return mapped;
        }

        /**
         * Enters the window's main loop. This will keep receiving events, until the user closes the window,
         * at which point we will break the main loop, and this function will return.
         */
        void mainLoop(void);
    };
}

#endif
