
#ifndef RASCUIXCB_MAIN_WINDOWDATA_H_
#define RASCUIXCB_MAIN_WINDOWDATA_H_

#include <xcb/xcb.h>

namespace rascUIxcb {

    /**
     * This class stores all the basic xcb info about a window.
     * This should be the only information provided to theme implementations.
     */
    class WindowData {
    public:
        xcb_connection_t * connection;
        xcb_screen_t * screen;
        xcb_window_t window;
        xcb_gcontext_t graphics;
        /**
         * This is set to either offScreenBuffer or window, depending on whether
         * double buffering is enabled or disabled respectively.
         * Subclasses MUST USE THIS for all drawing operations, NOT window.
         */
        xcb_drawable_t drawable;
    };
}

#endif
