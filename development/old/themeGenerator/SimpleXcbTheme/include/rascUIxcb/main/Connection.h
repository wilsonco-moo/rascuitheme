/*
 * Connection.h
 *
 *  Created on: 10 Jun 2019
 *      Author: wilson
 */

#ifndef RASCUIXCB_DATA_CONNECTION_H_
#define RASCUIXCB_DATA_CONNECTION_H_

#include <xcb/xcb.h>
#include <xcb/xcb_keysyms.h>
#include <cstddef>

namespace rascUIxcb {

    /**
     * This class encapsulates a connection, using XCB, to an X server.
     * The connection is opened when a Connection instance is created,
     * and automatically closed when the Connection instance is destroyed.
     */
    class Connection {

    private:
        // This stores the next event in the queue, so the user can peek at the next event.
        xcb_generic_event_t * nextEvent;

        // Don't allow copying: We hold X resources.
        Connection(const Connection & other);
        Connection & operator = (const Connection & other);
    public:

        xcb_connection_t * connection;
        xcb_screen_t * screen;
        xcb_key_symbols_t * keySymbols;

        /**
         * See xcb_connect documentation for details of displayname and screenp.
         * If left as the default values, the default X display will be used.
         */
        Connection(const char * displayname = NULL, int * screenp = NULL);
        virtual ~Connection(void);

    private:
        // Used internally within getPipedEvent. Returns true if this should result in exiting the getPipedEvent method.
        bool pollForCheckAndReturnEvent(xcb_generic_event_t ** event, bool * createdEvent, bool * error, int inputPipe);

        // Polls for the next event, while maintaining nextEvent. This should be used in preference to
        // xcb_poll_for_event in getPipedEvent.
        xcb_generic_event_t * pollForEventWithNext(void);

    public:
        /**
         * Returns true if the connection was successful, false otherwise.
         */
        bool wasSuccessful(void) const;

        /**
         * This runs xcb_flush on the connection.
         * This ensures that the server receives all pending requests.
         * This should be called in preference to sync, (since this is significantly faster),
         * unless waiting for the server to finish drawing is explicitly required.
         */
        void flush(void) const;

        /**
         * This runs xcb_aux_sync on the connection.
         * This ensures that the server receives all pending requests,
         * then waits for the server to finish processing them.
         */
        void sync(void) const;

        // ----------------------------- Utility methods ---------------------------------

        /**
         * Converts a keycode to a keysym.
         *
         * Adapted from, lines 916 - 958
         *  - Original: https://cep.xray.aps.anl.gov/software/qt4-x11-4.8.6-browser/d1/da0/qxcbkeyboard_8cpp_source.html
         *  - Archive:  https://web.archive.org/web/20180326040020/https://cep.xray.aps.anl.gov/software/qt4-x11-4.8.6-browser/d1/da0/qxcbkeyboard_8cpp_source.html
         */
        xcb_keysym_t keyCodeToKeySym(xcb_keycode_t keyCode, uint16_t state) const;

        /**
         * Waits until either an event happens on this connection,
         * an error happens on this connection (or the pipe),
         * or some data shows up on the pipe input.
         * For the timeout, specify either a number of milliseconds, or NULL for no (forever) timeout.
         *
         * The pipe input should either be a non-blocking pipe, or be set to -1, at which point it
         * will be ignored, and we will only wait for events.
         *
         *  > If an event is available, it is read and put in the event pointer,
         *    createdEvent is set to true, and all pipe data is read. Error may still get set
         *    to true here, if the pipe has been closed, or a further connection error has happened.
         *  > If an error happens, and no event can be generated, error is set to true, and event
         *    will likely be set to NULL. createdEvent will not be changed. The pipe will not be emptied,
         *    as it is expected that the user should close the connection and pipe now.
         *  > If data shows up in the pipe, it is all read, and we return.
         *  > If we are interrupted with some signal, we also empty the pipe and return.
         *  > If the call times out, we also empty the pipe and return.
         *
         * NOTE: It is the caller's responsibility to free the event returned by this method.
         */
        void getPipedEvent(xcb_generic_event_t ** event, bool * createdEvent, bool * error, int inputPipe, unsigned long long * timeout);

        /**
         * Allows access to the next waiting event, i.e: The one which will be returned by the next iteration
         * of getPipedEvent, if such an event exists. If there are no pending events after the one just returned
         * by getPipedEvent, this returns NULL.
         *
         * NOTE: The caller must NOT free the event returned by this method, as it will be later returned by
         *       getPipedEvent.
         */
        inline xcb_generic_event_t * peekNextEvent(void) const {
            return nextEvent;
        }
    };

}

#endif
