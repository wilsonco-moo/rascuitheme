/*
 * WindowConfig.h
 *
 *  Created on: 10 Jun 2019
 *      Author: wilson
 */

#ifndef RASCUIXCB_MAIN_WINDOWCONFIG_H_
#define RASCUIXCB_MAIN_WINDOWCONFIG_H_

#include <cstdint>
#include <string>

namespace rascUIxcb {

    /**
     * This class defines the initial setup of a Window instance.
     * Once the window has been created, the WindowConfig is no longer needed,
     * and can be safely destroyed.
     */
    class WindowConfig {
    public:
        std::string title;
        int16_t screenX, screenY;
        uint16_t screenWidth, screenHeight;
        uint16_t borderWidth;

        WindowConfig(const std::string & title = "",
                     int16_t screenX = 0,
                     int16_t screenY = 0,
                     uint16_t screenWidth = 340,
                     uint16_t screenHeight = 150,
                     uint16_t borderWidth = 10);

        virtual ~WindowConfig(void);
    };

}

#endif
