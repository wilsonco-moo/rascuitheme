
#include <sys/types.h>
#include <xcb/xcb.h>
#include <xcb/xcb_keysyms.h>
#include <xcb/xproto.h>
#include <cctype>
#include <chrono>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <string>
#include <thread>
#include <vector>

#include <rascUI/base/TopLevelContainer.h>
#include <rascUI/components/abstract/TextObject.h>
#include <rascUI/components/generic/BackPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/scroll/ScrollPane.h>
#include <rascUI/components/textEntry/BasicTextEntryBox.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/util/Layout.h>
#include <rascUI/util/Location.h>
#include <rascUI/util/Rectangle.h>
#include <rascUIxcb/main/Connection.h>
#include <rascUIxcb/main/Window.h>
#include <rascUIxcb/main/WindowConfig.h>
#include <rascUIxcb/theme/SimpleXcbTheme.h>
#include <rascUIxcb/util/Colour.h>
#include <rascUI/base/ThemeGroup.h>
#include <rascUItheme/DynamicThemeLoader.h>

/**
 * Dependencies:
 *
 * libxcb1             For most stuff
 * libxcb-keysyms1     For key code translation stuff
 * x11proto-core-dev   For key codes
 * libxcb-util0-dev    For auxillary functions (xcb_aux stuff, like xcb_aux_sync).
 *
 *
 *
 * libxcb1-dev
 * libxcb-keysyms1-dev
 *
 * Link against:  xcb, xcb-keysyms, xcb-util
 */



class CustomContainer : public rascUI::Container {
public:
    CustomContainer(rascUI::Location location) :
        rascUI::Container(location) {
    }
    virtual ~CustomContainer(void) {
    }

protected:
    virtual void onDraw(void) override {
        //std::cout << "DRAW RIGHT CONTAINER.\n";
        rascUI::Container::onDraw();
    }
    virtual void boundedRepaint(rascUI::Rectangle & boundingBox, std::unordered_set<Component *> & wantsRepaint, std::unordered_set<Container *> & childWantsRepaint) override {
        /*if (boundingBox.overlaps(getBoundedArea())) {
            std::cout << "[" << boundingBox.x << ", " << boundingBox.y << ", " << boundingBox.width << ", " << boundingBox.height << "] ";
            std::cout << "COLLIDES\n";
        }*/
        rascUI::Container::boundedRepaint(boundingBox, wantsRepaint, childWantsRepaint);
    }
};

#define WINDOW (*((rascUIxcb::Window *)(&windowBuff)))

int main(void) {

    rascUIxcb::Connection connection;

    if (!connection.wasSuccessful()) {
        std::cerr << "Failed to connect to X display.\n";
        return EXIT_FAILURE;
    }


    rascUIxcb::XcbThemeBase themeBase(true);
    rascUItheme::DynamicThemeLoader themeLoader("themeGenerator/SimpleXcbTheme/SimpleXcbTheme.so");
    if (!themeLoader.loadedSuccessfully()) {
        std::cerr << "Failed to load theme.\n";
        exit(EXIT_FAILURE);
    }
    rascUI::ThemeGroup themeGroup(themeLoader.getTheme());
    themeGroup.addTheme(&themeBase);
    themeGroup.addTheme(themeLoader.getTheme());

    char windowBuff[sizeof(rascUIxcb::Window)];
    {
        rascUIxcb::WindowConfig windowConfig;
        windowConfig.screenWidth = 400;
        windowConfig.screenHeight = 400;
        new (windowBuff) rascUIxcb::Window(&connection, windowConfig, &themeGroup);
    }

    #define LAYOUT_THEME &themeGroup

    #define TOPH 140
    #define LEFT_SCROLL_BUTTON_COUNT 200
    #define RIGHT_SCROLL_BUTTON_COUNT 35


    #define MAX_SEGMENT_HEIGHT (COUNT_OUTBORDER_Y(1) + UI_BORDER)

    #define H_CONTAINER_LOC(id)  \
        BORDERTABLE_X(id, 2,     \
         PIN_T(TOPH,             \
          GEN_BORDER             \
        ))

    #define V_CONTAINER_LOC(id)  \
        BORDERTABLE_Y(id, 2,     \
         GEN_BORDER              \
        )

    #define BOTTOM_SECTION_LOC      \
        SUB_BORDERMARGIN_T(TOPH,    \
         GEN_BORDER                 \
        )

    #define SCROLLPANE_LOC(id)                    \
        BORDERTABLE_X(id, 2,                      \
         SUB_MARGIN_Y(MAX_SEGMENT_HEIGHT,         \
          BOTTOM_SECTION_LOC                      \
        ))


    rascUI::Container leftContainer(rascUI::Location(H_CONTAINER_LOC(0)));
        rascUI::BackPanel leftBackPanel;
        rascUI::Container leftTopContainer(rascUI::Location(V_CONTAINER_LOC(0)));
            rascUI::FrontPanel leftTopFrontPanel;
            rascUI::Button leftTopTopButton(rascUI::Location(V_CONTAINER_LOC(0)), "Time: 0.0 seconds");
            rascUI::Button leftTopBottomButton(rascUI::Location(V_CONTAINER_LOC(1)), "Left top bottom");
        rascUI::Container leftBottomContainer(rascUI::Location(V_CONTAINER_LOC(1)));
            rascUI::FrontPanel leftBottomFrontPanel;
            rascUI::ToggleButtonSeries series(true);
            rascUI::ToggleButton leftBottomTopButton(&series, rascUI::Location(V_CONTAINER_LOC(0)), "Left bottom top");
            rascUI::ToggleButton leftBottomBottomButton(&series, rascUI::Location(V_CONTAINER_LOC(1)), "Left bottom bottom");
    CustomContainer rightContainer(rascUI::Location(H_CONTAINER_LOC(1)));
        rascUI::BackPanel rightBackPanel;
        rascUI::Container rightTopContainer(rascUI::Location(V_CONTAINER_LOC(0)));
            rascUI::FrontPanel rightTopFrontPanel;
            rascUI::Button rightTopTopButton(rascUI::Location(V_CONTAINER_LOC(0)), "Right top top");
            rascUI::Button rightTopBottomButton(rascUI::Location(V_CONTAINER_LOC(1)), "Right top bottom");
        rascUI::Container rightBottomContainer(rascUI::Location(V_CONTAINER_LOC(1)));
            rascUI::FrontPanel rightBottomFrontPanel;
            rascUI::Button rightBottomTopButton(rascUI::Location(V_CONTAINER_LOC(0)), "Right bottom top");
            rascUI::BasicTextEntryBox rightBottomBottomButton(rascUI::Location(V_CONTAINER_LOC(1)), "Text entry!!");

    rascUI::ScrollPane leftScrollPane(&themeGroup, rascUI::Location(SCROLLPANE_LOC(0)));
    leftScrollPane.smoothMoveSpeed = 0;

    std::vector<rascUI::Button> leftScrollButtons;
    leftScrollButtons.reserve(LEFT_SCROLL_BUTTON_COUNT);
    for (int i = 0; i < LEFT_SCROLL_BUTTON_COUNT; i++) {
        leftScrollButtons.emplace_back(rascUI::Location(SUB_BORDER_B(GEN_FILL)), std::string("Button number: ") + std::to_string(i));
        leftScrollPane.contents.add(&(*leftScrollButtons.rbegin()));
    }

    rascUI::ScrollPane rightScrollPane(&themeGroup, rascUI::Location(SCROLLPANE_LOC(1)), COUNT_OUTBORDER_Y(1) + UI_BORDER);
    rightScrollPane.smoothMoveSpeed = 0;

    std::vector<rascUI::Container> rightScrollContainers;
    std::vector<rascUI::FrontPanel> rightScrollFrontPanels;
    std::vector<rascUI::BasicTextEntryBox> rightScrollTextEntrys;
    std::vector<rascUI::Button> rightScrollButtons;
    rightScrollContainers.reserve(LEFT_SCROLL_BUTTON_COUNT);
    rightScrollFrontPanels.reserve(LEFT_SCROLL_BUTTON_COUNT);
    rightScrollTextEntrys.reserve(LEFT_SCROLL_BUTTON_COUNT * 2);
    rightScrollButtons.reserve(LEFT_SCROLL_BUTTON_COUNT);
    for (int i = 0; i < RIGHT_SCROLL_BUTTON_COUNT; i++) {
        rightScrollContainers.emplace_back(rascUI::Location(SUB_BORDER_B(GEN_FILL)));
        rascUI::Container * cont = &(*rightScrollContainers.rbegin());
        rightScrollPane.contents.add(cont);
        rightScrollFrontPanels.emplace_back();
        cont->add(&(*rightScrollFrontPanels.rbegin()));
        rightScrollTextEntrys.emplace_back(rascUI::Location(BORDERTABLE_X(0, 3, GEN_BORDER)), std::string("TL ")+std::to_string(i));
        cont->add(&(*rightScrollTextEntrys.rbegin()));
        rightScrollButtons.emplace_back(rascUI::Location(BORDERTABLE_X(1, 3, GEN_BORDER)), std::string("B ")+std::to_string(i));
        cont->add(&(*rightScrollButtons.rbegin()));
        rightScrollTextEntrys.emplace_back(rascUI::Location(BORDERTABLE_X(2, 3, GEN_BORDER)), std::string("TR ")+std::to_string(i));
        cont->add(&(*rightScrollTextEntrys.rbegin()));
    }


    rascUI::BackPanel aboveScrollPanePanel(rascUI::Location(PIN_T(MAX_SEGMENT_HEIGHT, BOTTOM_SECTION_LOC)));
    rascUI::BackPanel belowScrollPanePanel(rascUI::Location(PIN_B(MAX_SEGMENT_HEIGHT, BOTTOM_SECTION_LOC)));

    series.setSelectedButton(&leftBottomTopButton);

    WINDOW.add(&leftScrollPane);
    WINDOW.add(&rightScrollPane);
        leftContainer.add(&leftBackPanel);
            leftTopContainer.add(&leftTopFrontPanel);
            leftTopContainer.add(&leftTopTopButton);
            leftTopContainer.add(&leftTopBottomButton);
        leftContainer.add(&leftTopContainer);
            leftBottomContainer.add(&leftBottomFrontPanel);
            leftBottomContainer.add(&leftBottomTopButton);
            leftBottomContainer.add(&leftBottomBottomButton);
        leftContainer.add(&leftBottomContainer);
    WINDOW.add(&leftContainer);
        rightContainer.add(&rightBackPanel);
            rightTopContainer.add(&rightTopFrontPanel);
            rightTopContainer.add(&rightTopTopButton);
            rightTopContainer.add(&rightTopBottomButton);
        rightContainer.add(&rightTopContainer);
            rightBottomContainer.add(&rightBottomFrontPanel);
            rightBottomContainer.add(&rightBottomTopButton);
            rightBottomContainer.add(&rightBottomBottomButton);
        rightContainer.add(&rightBottomContainer);
    WINDOW.add(&rightContainer);
    WINDOW.add(&aboveScrollPanePanel);
    WINDOW.add(&belowScrollPanePanel);

    WINDOW.setMapped(true);

    void * token = WINDOW.beforeEvent.createToken();
    unsigned long long seconds = 0; char partialSeconds = 0;
    WINDOW.beforeEvent.addTimedRepeatingFunction(token, 2146, 100, [&leftTopTopButton, &seconds, &partialSeconds](void) {
        partialSeconds++;
        if (partialSeconds >= 10) {
            partialSeconds = 0;
            seconds++;
        }
        leftTopTopButton.setText(std::string("Time: ") + std::to_string(seconds) + "." + std::to_string((int)partialSeconds) + " seconds");
        leftTopTopButton.repaint();
    });

    WINDOW.mainLoop();

    rascUI::FunctionQueue::deleteToken(token);

    WINDOW.~Window();

    return 0;
}
